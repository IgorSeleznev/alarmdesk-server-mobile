import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version "2.5.6"
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
    kotlin("kapt") version "1.5.31"
    kotlin("jvm") version "1.5.31"
    kotlin("plugin.spring") version "1.5.31"
}

group = "com.seleznyoviyu.alarmdesk"
version = "1.0.0"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
    mavenLocal()
    mavenCentral()
    jcenter()
}

val junitJupiterVersion = "5.4.2"

tasks.bootRun {
    mainClass.set("com.seleznyoviyu.alarmdesk.server.mobile.AlarmdeskServerMobileApplication")
    args("--spring.profiles.active=test")
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-jdbc")
    implementation("org.springframework.boot:spring-boot-starter-security")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.flywaydb:flyway-core")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    runtimeOnly("org.postgresql:postgresql")


    implementation("javax.xml.bind:jaxb-api:2.1")
    implementation("io.jsonwebtoken:jjwt:0.9.1")
    implementation("org.apache.commons:commons-lang3:3.12.0")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.mapstruct:mapstruct:1.4.2.Final")
    kapt("org.mapstruct:mapstruct-processor:1.4.2.Final")
    implementation("com.google.guava:guava:30.1.1-jre")
    implementation("org.seleznyoviyu.utils:seleznyoviyu-utils-common:1.0.0")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")
    //implementation("org.seleznyoviyu.utils:seleznyoviyu-utils-security:1.0.0")
    //implementation("org.seleznyoviyu.utils:seleznyoviyu-utils-security-spring-webmvc-interceptor:1.0.0")

    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.springframework.security:spring-security-test")
    testImplementation("io.rest-assured:rest-assured:4.4.0")
    testImplementation("org.assertj:assertj-core:3.20.2")
    testImplementation("com.google.code.gson:gson:2.8.7")
    testImplementation ("org.junit.jupiter:junit-jupiter-api:$junitJupiterVersion")
    testImplementation ("org.junit.jupiter:junit-jupiter-params:$junitJupiterVersion")
    testRuntimeOnly ("org.junit.jupiter:junit-jupiter-engine:$junitJupiterVersion")
    testImplementation ("org.testcontainers:testcontainers:1.15.3")
    testImplementation ("org.testcontainers:junit-jupiter:1.15.3")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "11"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}
