INSERT INTO user_account (id, name, login, password_hash, role, region_id, external_id)
VALUES (999999, 'Тестовый пользователь', 'test', '$2a$10$V60bCCclKsHLfm3MIVru2.FSDoGURHl00.N0qAWvDIE1VboMqeQli', 'DEVELOPER', 0, 'c36788c8-92ca-4101-a6c3-508675a3d577')
ON CONFLICT DO NOTHING;

INSERT INTO public.region (id, title)
VALUES (999999, 'Регион для тестирования')
ON CONFLICT DO NOTHING;