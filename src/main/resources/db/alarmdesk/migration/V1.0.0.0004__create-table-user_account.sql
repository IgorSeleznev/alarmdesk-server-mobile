CREATE SEQUENCE user_account_seq INCREMENT 1 START 1 MINVALUE 1;

CREATE TABLE public.user_account
(
    id            BIGINT NOT NULL,
    name          VARCHAR(100),
    login         VARCHAR(64),
    password_hash VARCHAR(1024),
    role          VARCHAR(32),
    region_id     BIGINT NOT NULL,
    external_id   UUID DEFAULT NULL,
    CONSTRAINT user_account_pk PRIMARY KEY (id),
    CONSTRAINT user_account_region_id_fk FOREIGN KEY (region_id) REFERENCES public.region (id) ON DELETE CASCADE
);

CREATE INDEX user_account_login_password_hash_idx ON public.user_account (login, password_hash);

CREATE UNIQUE INDEX user_account_login_unique_idx ON public.user_account (login ASC);

CREATE UNIQUE INDEX user_account_name_unique_idx ON public.user_account (name ASC);

CREATE INDEX user_account_region_id_idx ON public.user_account (region_id);