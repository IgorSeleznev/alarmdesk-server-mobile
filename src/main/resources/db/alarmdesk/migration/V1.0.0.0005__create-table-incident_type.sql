CREATE SEQUENCE incident_type_seq INCREMENT 1 START 1 MINVALUE 1;

CREATE TABLE public.incident_type
(
    id                        BIGINT      NOT NULL,
    parent_incident_type_id   BIGINT               DEFAULT NULL,
    title                     VARCHAR(256),
    default_incident_priority VARCHAR(10) NOT NULL,
    parameters                JSONB       NOT NULL,
    is_equipment_describe     BOOLEAN     NOT NULL DEFAULT false,
    incident_resolve_sla      INT         NOT NULL DEFAULT 0,
    incident_reaction_sla     INT         NOT NULL DEFAULT 0,
    CONSTRAINT incident_type_pk PRIMARY KEY (id)
);

CREATE INDEX incident_type_parent_incident_type_id_idx ON public.incident_type (parent_incident_type_id);