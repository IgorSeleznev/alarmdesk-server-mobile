CREATE SEQUENCE location_seq INCREMENT 1 START 1 MINVALUE 1;

CREATE TABLE public.location (
    id BIGINT NOT NULL,
    title VARCHAR(256) NOT NULL,
    customer_id BIGINT NOT NULL,
    region_id BIGINT NOT NULL,
    address VARCHAR(1024) DEFAULT NULL,
    ukm_location BIGINT DEFAULT NULL,
    CONSTRAINT location_pk PRIMARY KEY (id),
    CONSTRAINT location_region_id_fk FOREIGN KEY (region_id) REFERENCES public.region (id) ON DELETE CASCADE,
    CONSTRAINT location_customer_id_fk FOREIGN KEY (customer_id) REFERENCES public.customer (id) ON DELETE CASCADE
);