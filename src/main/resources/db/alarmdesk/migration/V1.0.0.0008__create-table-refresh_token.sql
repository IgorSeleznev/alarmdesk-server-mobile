CREATE SEQUENCE refresh_token_seq INCREMENT 1 START 1 MINVALUE 1;

CREATE TABLE public.refresh_token
(
    id              BIGINT    NOT NULL,
    token           UUID      NOT NULL,
    create_datetime TIMESTAMP NOT NULL,
    lifetime        BIGINT    NOT NULL,
    user_account_id BIGINT    NOT NULL,
    CONSTRAINT refresh_token_pk PRIMARY KEY (id),
    CONSTRAINT refresh_token_user_account_id_fk FOREIGN KEY (user_account_id) REFERENCES public.user_account (id) ON DELETE CASCADE
);

CREATE INDEX refresh_token_token_idx ON public.refresh_token (token ASC);
