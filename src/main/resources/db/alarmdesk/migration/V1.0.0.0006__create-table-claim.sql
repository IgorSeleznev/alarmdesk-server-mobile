CREATE SEQUENCE claim_seq INCREMENT 1 START 1 MINVALUE 1;

CREATE TABLE public.claim (
    id BIGINT NOT NULL,
    location_id BIGINT NOT NULL,
    status VARCHAR(16) NOT NULL DEFAULT 'CREATED',
    engineer_id BIGINT NOT NULL,
    author_id BIGINT NOT NULL,
    comment TEXT DEFAULT NULL,
    incident_type_id BIGINT NOT NULL,
    incident_priority VARCHAR(10) NOT NULL,
    incident_type_parameter_values JSONB NOT NULL DEFAULT '{}'::JSONB,
    status_update_author_id BIGINT NOT NULL,
    create_datetime TIMESTAMP NOT NULL,
    status_update_datetime TIMESTAMP NOT NULL,
    finish_datetime TIMESTAMP DEFAULT NULL,
    engineer_assign_datetime TIMESTAMP DEFAULT NULL,
    technical_details JSONB NOT NULL DEFAULT '{}'::JSONB,
    status_log JSONB NOT NULL DEFAULT '[]'::JSONB,
    comment_log JSONB NOT NULL DEFAULT '[]'::JSONB,
    CONSTRAINT claim_pk PRIMARY KEY (id),
    CONSTRAINT claim_incident_type_id_fk FOREIGN KEY (incident_type_id) REFERENCES public.incident_type (id) ON DELETE CASCADE
);

CREATE INDEX claim_engineer_id_status_idx ON public.claim (engineer_id ASC, status ASC);

CREATE INDEX claim_engineer_id_idx ON public.claim (engineer_id ASC);

CREATE INDEX claim_status_idx ON public.claim (status ASC);

CREATE INDEX claim_status_location_id_idx ON public.claim (status ASC, location_id ASC);