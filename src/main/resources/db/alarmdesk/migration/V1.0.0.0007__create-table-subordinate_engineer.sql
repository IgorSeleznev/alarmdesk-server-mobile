CREATE SEQUENCE subordinate_engineer_seq INCREMENT 1 START 1 MINVALUE 1;

CREATE TABLE public.subordinate_engineer (
    id BIGINT NOT NULL,
    head_engineer_user_account_id BIGINT NOT NULL,
    engineer_user_account_id BIGINT NOT NULL,
    CONSTRAINT subordinate_engineer_pk PRIMARY KEY (id),
    CONSTRAINT subordinate_engineer_head_engineer_user_account_id_fk FOREIGN KEY (head_engineer_user_account_id) REFERENCES public.user_account (id) ON DELETE CASCADE,
    CONSTRAINT subordinate_engineer_engineer_user_account_id_fk FOREIGN KEY (engineer_user_account_id) REFERENCES public.user_account (id) ON DELETE CASCADE
);