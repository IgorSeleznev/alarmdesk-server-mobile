CREATE SEQUENCE customer_seq INCREMENT 1 START 1 MINVALUE 1;

CREATE TABLE public.customer
(
    id           BIGINT       NOT NULL,
    title        VARCHAR(256) NOT NULL,
    address      VARCHAR(1024) DEFAULT NULL,
    CONSTRAINT customer_pk PRIMARY KEY (id)
);