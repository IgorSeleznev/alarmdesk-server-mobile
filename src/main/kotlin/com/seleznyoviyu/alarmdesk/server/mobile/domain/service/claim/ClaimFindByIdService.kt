package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.claim

import com.seleznyoviyu.alarmdesk.server.mobile.da.client.claim.ClaimEntityFindByIdClient
import com.seleznyoviyu.alarmdesk.server.mobile.domain.mapper.claim.ClaimMapper
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.Claim
import org.springframework.stereotype.Service

@Service
class ClaimFindByIdService(
    private val findClient: ClaimEntityFindByIdClient,
    private val mapper: ClaimMapper
) {
    fun findById(id: Long): Claim {
        return mapper.map(
            findClient.findById(id)
        )
    }
}