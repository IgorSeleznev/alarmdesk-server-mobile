package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.notification

import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.user.UserAccountFindByIdService
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.Claim
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.notification.ClaimAssignedNotification
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user.SimplifiedUserAccount
import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.incident.IncidentTypeTitleListByLeafService
import org.springframework.stereotype.Service

@Service
class ClaimEngineerAssignNotificationBuildService(
    private val userFindService: UserAccountFindByIdService,
    private val titlesService: IncidentTypeTitleListByLeafService,
) {
    fun buildNotification(claim: Claim): ClaimAssignedNotification {
        val engineer = userFindService.findById(claim.engineerId!!)
        val author = userFindService.findById(claim.authorId)
        return ClaimAssignedNotification(
            claimId = claim.id!!,
            engineer = SimplifiedUserAccount(
                id = engineer.id!!,
                name = engineer.name
            ),
            author = SimplifiedUserAccount(
                id = author.id!!,
                name = author.name
            ),
            assignDateTime = claim.engineerAssignDateTime!!,
            incidentTypeId = claim.incidentTypeId,
            incidentTypesTitles = titlesService.listTitleByLeaf(claim.incidentTypeId),
            priority = claim.incidentPriority,
            comment = claim.comment
        )

    }
}