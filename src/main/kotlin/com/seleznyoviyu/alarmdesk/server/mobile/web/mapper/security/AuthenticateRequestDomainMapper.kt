package com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.security

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.security.AuthenticateRequest
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.mobile.authenticate.AuthenticateRequestJson
import org.mapstruct.InjectionStrategy
import org.mapstruct.Mapper

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
interface AuthenticateRequestDomainMapper {
    fun map(source: AuthenticateRequestJson): AuthenticateRequest
}