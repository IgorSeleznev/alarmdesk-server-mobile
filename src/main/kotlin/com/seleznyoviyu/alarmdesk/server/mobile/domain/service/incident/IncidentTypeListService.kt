package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.incident

import com.seleznyoviyu.alarmdesk.server.mobile.da.client.incident.IncidentTypeEntityFindAllByParentIdClient
import com.seleznyoviyu.alarmdesk.server.mobile.da.client.incident.IncidentTypeEntityFindAllWithNoParentIdClient
import com.seleznyoviyu.alarmdesk.server.mobile.domain.mapper.incident.IncidentTypeMapper
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.IncidentType
import org.springframework.stereotype.Service

@Service
class IncidentTypeListService(
    private val findClient: IncidentTypeEntityFindAllByParentIdClient,
    private val findRootClient: IncidentTypeEntityFindAllWithNoParentIdClient,
    private val mapper: IncidentTypeMapper
) {
    fun listByParent(parentId: Long): List<IncidentType> {
        return findClient.findByParentId(parentId).map(mapper::map)
    }

    fun listRoot(): List<IncidentType> {
        return findRootClient.findWithNoParentId().map(mapper::map)
    }
}