package com.seleznyoviyu.alarmdesk.server.mobile.da.client.incident

import com.fasterxml.jackson.databind.ObjectMapper
import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.incident.IncidentTypeEntity
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Component

@Component
class IncidentTypeEntityInsertClient(
    private val jdbcTemplate: NamedParameterJdbcTemplate,
    private val objectMapper: ObjectMapper
) {
    fun insert(incidentType: IncidentTypeEntity): Long {
        jdbcTemplate.update(
            """
INSERT INTO incident_type (
    id,
    title,
    parent_incident_type_id,
    default_incident_priority,
    parameters,
    is_equipment_describe,
    incident_resolve_sla,
    incident_reaction_sla
) VALUES (
    :id,
    :parentId,
    :title,
    :defaultPriority,
    :parameters,
    :isEquipmentDescribe,
    :incidentResolveSla,
    :incidentReactionSla
)
            """.trimIndent(),
            MapSqlParameterSource()
                .addValue("id", incidentType.id)
                .addValue("title", incidentType.title)
                .addValue("parentId", incidentType.parentId)
                .addValue("defaultPriority", incidentType.defaultPriority.toString())
                .addValue(
                    "parameters",
                    objectMapper.writeValueAsString(incidentType.parameters)
                )
                .addValue("is_equipment_describe", incidentType.isEquipmentDescribe)
                .addValue("incidentResolveSla", incidentType.incidentResolveSla)
                .addValue("incidentReactionSla", incidentType.incidentReactionSla)
        )
        return incidentType.id!!
    }
}