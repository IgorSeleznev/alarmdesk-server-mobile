package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.scheduled

import org.springframework.scheduling.TriggerContext
import java.util.*

interface ScheduledService {
    fun execute()
    fun nextExecutionTime(triggerContext: TriggerContext): Date
}