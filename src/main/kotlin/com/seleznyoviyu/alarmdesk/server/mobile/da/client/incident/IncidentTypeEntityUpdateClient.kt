package com.seleznyoviyu.alarmdesk.server.mobile.da.client.incident

import com.fasterxml.jackson.databind.ObjectMapper
import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.incident.IncidentTypeEntity
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Component

@Component
class IncidentTypeEntityUpdateClient(
    private val jdbcTemplate: NamedParameterJdbcTemplate,
    private val objectMapper: ObjectMapper
) {
    fun update(incidentType: IncidentTypeEntity): Long {
        jdbcTemplate.update(
            """
UPDATE incident_type 
SET
    title = :title,
    parent_incident_type_id = :parentId,
    default_incident_priority = :defaultPriority,
    parameters = :parameters,
    is_equipment_describe = :isEquipmentDescribe,
    incident_resolve_sla = :incidentResolveSla,
    incident_reaction_sla = :incidentReactionSla
WHERE id = :id    
            """.trimIndent(),
            MapSqlParameterSource()
                .addValue("id", incidentType.id)
                .addValue("title", incidentType.title)
                .addValue("parentId", incidentType.parentId)
                .addValue("defaultPriority", incidentType.defaultPriority.toString())
                .addValue(
                    "parameters",
                    objectMapper.writeValueAsString(incidentType.parameters)
                )
                .addValue("is_equipment_describe", incidentType.isEquipmentDescribe)
                .addValue("incidentResolveSla", incidentType.incidentResolveSla)
                .addValue("incidentReactionSla", incidentType.incidentReactionSla)
        )
        return incidentType.id!!
    }
}