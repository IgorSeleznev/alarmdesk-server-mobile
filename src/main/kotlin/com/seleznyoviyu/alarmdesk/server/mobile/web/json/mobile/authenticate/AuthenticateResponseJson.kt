package com.seleznyoviyu.alarmdesk.server.mobile.web.json.mobile.authenticate

data class AuthenticateResponseJson(
    val accessToken: String?,
    val refreshToken: String?
)
