package com.seleznyoviyu.alarmdesk.server.mobile.da.client.refreshtoken

import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.refreshtoken.RefreshTokenEntity
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Component
import java.sql.Timestamp
import java.sql.Types

@Component
class RefreshTokenEntityInsertClient(
    private val jdbcTemplate: NamedParameterJdbcTemplate
) {
    fun insert(refreshTokenEntity: RefreshTokenEntity) {
        jdbcTemplate.update(
            """
INSERT INTO refresh_token (
    id,
    token,
    create_datetime,
    lifetime,
    user_account_id
) VALUES (
    :id,
    :token,
    :createDateTime,
    :lifetime,
    :userAccountId
)
           """.trimIndent(),
            MapSqlParameterSource()
                .addValue("id", refreshTokenEntity.id)
                .addValue("token", refreshTokenEntity.token)
                .addValue(
                    "createDateTime",
                    Timestamp.from(
                        refreshTokenEntity.createDateTime
                    ),
                    Types.TIMESTAMP
                )
                .addValue("lifetime", refreshTokenEntity.lifetime)
                .addValue("userAccountId", refreshTokenEntity.userAccountId)
        )
    }
}