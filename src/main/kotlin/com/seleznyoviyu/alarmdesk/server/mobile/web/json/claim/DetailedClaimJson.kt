package com.seleznyoviyu.alarmdesk.server.mobile.web.json.claim

import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.claim.ClaimStatus
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.ClaimCommentLog
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.DetailedClaimIncidentTypeParameterValue
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.IncidentPriorityType
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.SimplifiedClaimIncidentType
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.location.SimplifiedLocation
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user.SimplifiedUserAccount
import java.time.Instant

@JsonNaming(PropertyNamingStrategies.KebabCaseStrategy::class)
data class DetailedClaimJson(
    val id: Long,
    val incident: SimplifiedClaimIncidentType,
    val incidentPriority: IncidentPriorityType,
    val incidentTypeParameterValues: Map<Long, DetailedClaimIncidentTypeParameterValue>,
    val location: SimplifiedLocation,
    val status: ClaimStatus,
    val engineer: SimplifiedUserAccount?,
    val author: SimplifiedUserAccount,
    val statusUpdateAuthor: SimplifiedUserAccount,
    val createDateTime: Instant,
    val statusUpdateDateTime: Instant,
    val finishDateTime: Instant?,
    val comments: List<ClaimCommentLog>,
    val statusLog: List<ClaimCommentLog>,
    val technicalDetails: Map<String, Map<String, String>>?
)
