package com.seleznyoviyu.alarmdesk.server.mobile.common.exception

class ForbiddenException : RuntimeException()