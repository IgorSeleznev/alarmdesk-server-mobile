package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.security

import com.seleznyoviyu.alarmdesk.server.mobile.web.configuration.JwtTokenUtil
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Service
class RefreshTokenCreateService(
    private val refreshTokenSaveService: RefreshTokenSaveService,
    private val jwtTokenUtil: JwtTokenUtil
) {
    @Transactional
    fun createRefreshToken(jwtToken: String): UUID {
        val refreshToken = UUID.randomUUID()
        val userAccountExternalId = jwtTokenUtil.getExternalId(jwtToken)
        refreshTokenSaveService.save(
            refreshToken = refreshToken,
            userAccountExternalId = userAccountExternalId!!
        )
        return refreshToken
    }
}