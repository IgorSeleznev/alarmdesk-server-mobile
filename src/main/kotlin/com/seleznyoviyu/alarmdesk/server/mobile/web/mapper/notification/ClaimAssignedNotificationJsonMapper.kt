package com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.notification

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.notification.ClaimAssignedNotification
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.notification.ClaimAssignedNotificationJson
import org.mapstruct.InjectionStrategy
import org.mapstruct.Mapper

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
interface ClaimAssignedNotificationJsonMapper {
    fun map(source: ClaimAssignedNotification): ClaimAssignedNotificationJson
}