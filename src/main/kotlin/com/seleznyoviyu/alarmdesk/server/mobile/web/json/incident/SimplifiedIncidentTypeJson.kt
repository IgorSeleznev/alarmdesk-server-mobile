package com.seleznyoviyu.alarmdesk.server.mobile.web.json.incident

import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.SimplifiedIncidentTypeParameter

@JsonNaming(PropertyNamingStrategies.KebabCaseStrategy::class)
data class SimplifiedIncidentTypeJson(
    val id: Long,
    val parentId: Long?,
    val title: String,
    val isLast: Boolean,
    val parameters: List<SimplifiedIncidentTypeParameter>
)
