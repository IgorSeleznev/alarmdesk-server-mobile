package com.seleznyoviyu.alarmdesk.server.mobile.da.exception

class NotFoundException : RuntimeException {

    constructor() : super()

    constructor(message: String) : super(message)
}