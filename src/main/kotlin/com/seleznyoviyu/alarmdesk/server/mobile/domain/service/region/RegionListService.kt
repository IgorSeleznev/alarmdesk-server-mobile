package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.region

import com.seleznyoviyu.alarmdesk.server.mobile.da.client.region.RegionEntityListClient
import com.seleznyoviyu.alarmdesk.server.mobile.domain.mapper.region.RegionMapper
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.region.Region
import org.springframework.stereotype.Service

@Service
class RegionListService(
    private val client: RegionEntityListClient,
    private val mapper: RegionMapper
) {
    fun listAll(): List<Region> {
        return client.listAll().map(mapper::map)
    }
}