package com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.security

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.security.AuthenticationResponse
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.mobile.authenticate.AuthenticateResponseJson
import org.mapstruct.InjectionStrategy
import org.mapstruct.Mapper

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
interface AuthenticateResponseJsonMapper {
    fun map(source: AuthenticationResponse): AuthenticateResponseJson
}