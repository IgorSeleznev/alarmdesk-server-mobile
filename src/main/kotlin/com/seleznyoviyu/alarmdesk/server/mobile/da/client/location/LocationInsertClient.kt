package com.seleznyoviyu.alarmdesk.server.mobile.da.client.location

import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.location.LocationEntity
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Component

@Component
class LocationInsertClient(
    private val jdbcTemplate: NamedParameterJdbcTemplate
) {
    fun insert(location: LocationEntity): Long {
        jdbcTemplate.update(
            """
INSERT INTO location (
    id,
    ukm_location,
    title,
    region_id,
    address
) VALUES (
    :id,
    :ukmLocation,
    :title,
    :regionId,
    :address
)
            """.trimIndent(),
            MapSqlParameterSource()
                .addValue("ukmLocation", location.ukmLocation)
                .addValue("title", location.title)
                .addValue("regionId", location.regionId)
                .addValue("address", location.address)
                .addValue("id", location.id)
        )
        return location.id!!
    }
}