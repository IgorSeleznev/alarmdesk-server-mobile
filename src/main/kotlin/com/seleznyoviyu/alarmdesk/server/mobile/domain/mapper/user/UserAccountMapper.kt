package com.seleznyoviyu.alarmdesk.server.mobile.domain.mapper.user

import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.user.UserAccountEntity
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user.UserAccount
import org.mapstruct.InjectionStrategy
import org.mapstruct.Mapper


@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
interface UserAccountMapper {
    fun map(source: UserAccountEntity): UserAccount
}