package com.seleznyoviyu.alarmdesk.server.mobile.da.entity.region

data class RegionEntity(
    val id: Long?,
    val title: String
)
