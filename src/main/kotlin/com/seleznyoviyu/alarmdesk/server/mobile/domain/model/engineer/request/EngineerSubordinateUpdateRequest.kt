package com.seleznyoviyu.alarmdesk.server.mobile.domain.model.engineer.request

data class EngineerSubordinateUpdateRequest(
    val headEngineerId: Long?,
    val subordinateEngineerId: Long
)
