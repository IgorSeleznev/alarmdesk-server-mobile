package com.seleznyoviyu.alarmdesk.server.mobile.web.json.claim.request

import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming

@JsonNaming(PropertyNamingStrategies.KebabCaseStrategy::class)
data class ClaimCommentUpdateRequestJson(
    val claimId: Long,
    val comment: String
)