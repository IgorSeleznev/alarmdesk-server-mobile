package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.user

import com.seleznyoviyu.alarmdesk.server.mobile.da.client.common.NextIdClient
import com.seleznyoviyu.alarmdesk.server.mobile.da.client.user.UserAccountEntityInsertClient
import com.seleznyoviyu.alarmdesk.server.mobile.da.client.user.UserAccountEntityUpdateClient
import com.seleznyoviyu.alarmdesk.server.mobile.domain.mapper.user.UserAccountEntityMapper
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user.UserAccount
import org.springframework.stereotype.Component
import java.util.*

@Component
class UserAccountSaver(
    private val updateClient: UserAccountEntityUpdateClient,
    private val insertClient: UserAccountEntityInsertClient,
    private val mapper: UserAccountEntityMapper,
    private val nextIdClient: NextIdClient
) {
    fun save(userAccount: UserAccount): Long {
        if (userAccount.id == null) {
            return insertClient.insert(
                mapper.map(userAccount).copy(
                    id = nextIdClient.nextId("user_account_seq"),
                    externalId = UUID.randomUUID()
                )
            )
        }
        return updateClient.update(
            mapper.map(userAccount)
        )
    }
}