package com.seleznyoviyu.alarmdesk.server.mobile.web.controller.claim.status

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.claim.ClaimStatus
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.BUSINESS_ADMIN
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.DEVELOPER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.OPERATOR
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.request.ClaimStatusUpdateBriefRequest
import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.claim.ClaimStatusUpdateManager
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.claim.ClaimStatusUpdateBriefRequestJson
import com.seleznyoviyu.alarmdesk.server.mobile.web.resolver.HttpSessionAuthenticatedUserResolver
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.annotation.security.RolesAllowed

@RestController
@RequestMapping("/claim")
class ClaimStatusToCancelController(
    private val updateManager: ClaimStatusUpdateManager,
    private val userResolver: HttpSessionAuthenticatedUserResolver
) {

    @RolesAllowed(
        "ROLE_$DEVELOPER",
        "ROLE_$OPERATOR",
        "ROLE_$BUSINESS_ADMIN"
    )
    @PutMapping("/claim/cancel")
    fun cancelClaim(@RequestBody request: ClaimStatusUpdateBriefRequestJson) {
        updateManager.updateStatus(
            ClaimStatusUpdateBriefRequest(
                claimId = request.claimId,
                comment = request.comment,
                authorId = userResolver.resolve().id!!,
                newStatus = ClaimStatus.CANCELED
            )
        )
    }
}