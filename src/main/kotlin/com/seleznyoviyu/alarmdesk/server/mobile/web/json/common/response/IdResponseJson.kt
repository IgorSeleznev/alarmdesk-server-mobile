package com.seleznyoviyu.alarmdesk.server.mobile.web.json.common.response

data class IdResponseJson(
    val id: Long
)