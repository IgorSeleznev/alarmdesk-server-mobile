package com.seleznyoviyu.alarmdesk.server.mobile.web.controller.incident

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.BUSINESS_ADMIN
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.DEVELOPER
import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.incident.IncidentTypeSaveService
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.common.IdResponseJson
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.incident.IncidentTypeJson
import com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.incident.IncidentTypeDomainMapper
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.annotation.security.RolesAllowed

@RestController
@RequestMapping("/incident-type")
class IncidentTypeSaveController(
    private val service: IncidentTypeSaveService,
    private val mapper: IncidentTypeDomainMapper
) {
    @RolesAllowed(
        "ROLE_$DEVELOPER",
        "ROLE_$BUSINESS_ADMIN"
    )
    @PutMapping("/save")
    fun save(@RequestBody request: IncidentTypeJson): IdResponseJson {
        return IdResponseJson(
            id = service.save(
                mapper.map(request)
            )
        )
    }
}