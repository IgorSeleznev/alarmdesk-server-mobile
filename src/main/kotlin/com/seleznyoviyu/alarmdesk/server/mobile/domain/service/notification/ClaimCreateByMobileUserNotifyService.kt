package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.notification

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.notification.ClaimCreatedNotification
import org.springframework.stereotype.Service

@Service
class ClaimCreateByMobileUserNotifyService(
    private val poolManager: NotificationPoolManager,
) {
    fun notify(
        usersIdList: List<Long>,
        notification: ClaimCreatedNotification
    ) {
        usersIdList.forEach { userAccountId ->
            poolManager.put(
                userAccountId,
                notification
            )
        }
    }
}