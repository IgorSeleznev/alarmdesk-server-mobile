package com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user

data class UserAccountSaveRequest(
    val id: Long?,
    val name: String,
    val login: String,
    val password: String,
    val role: String,
    val regionId: Long
)
