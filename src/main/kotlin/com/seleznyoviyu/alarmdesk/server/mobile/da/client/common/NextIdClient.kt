package com.seleznyoviyu.alarmdesk.server.mobile.da.client.common

import com.seleznyoviyu.alarmdesk.server.mobile.da.exception.NotFoundException
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Component
import java.sql.ResultSet

@Component
class NextIdClient(
    private val jdbcTemplate: NamedParameterJdbcTemplate
) {
    fun nextId(sequenceName: String): Long {
        val result: List<Long> = jdbcTemplate.query(
            """
select nextval('$sequenceName')
            """.trimIndent()
        ) { resultSet: ResultSet, _: Int ->
            resultSet.getLong(1)
        }

        if (result.isNotEmpty()) {
            return result[0]
        }
        throw NotFoundException("Отсутствует результат при попытке получить следующее значение sequence")
    }
}