package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.user

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user.UserAccountSaveRequest
import org.springframework.stereotype.Service

@Service
class UserAccountSaveService(
    private val saver: UserAccountSaver,
    private val userAccountBuilder: UserAccountSaveBuilder
) {
    fun saveUserAccount(request: UserAccountSaveRequest): Long {
        return saver.save(
            userAccountBuilder.build(request)
        )
    }
}