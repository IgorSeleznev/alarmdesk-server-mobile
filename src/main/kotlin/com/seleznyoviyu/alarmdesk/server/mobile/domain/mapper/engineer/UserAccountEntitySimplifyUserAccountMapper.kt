package com.seleznyoviyu.alarmdesk.server.mobile.domain.mapper.engineer

import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.user.UserAccountEntity
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user.SimplifiedUserAccount
import org.mapstruct.InjectionStrategy
import org.mapstruct.Mapper


@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
interface UserAccountEntitySimplifyUserAccountMapper {
    fun map(source: UserAccountEntity): SimplifiedUserAccount
}