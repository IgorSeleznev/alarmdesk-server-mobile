package com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user

data class SimplifiedUserAccount(
    val id: Long,
    val name: String
)
