package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.claim

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.request.ClaimStatusUpdateBriefRequest
import org.springframework.stereotype.Service

@Service
class ClaimStatusUpdateManager(
    private val validator: ClaimStatusUpdateValidator,
    private val updateService: ClaimStatusUpdateService,
    private val requestBuilder: ClaimStatusUpdateRequestBuilder
) {
    fun updateStatus(request: ClaimStatusUpdateBriefRequest) {
        validator.validate(request)
        updateService.updateStatus(
            requestBuilder.buildRequest(request)
        )
    }
}