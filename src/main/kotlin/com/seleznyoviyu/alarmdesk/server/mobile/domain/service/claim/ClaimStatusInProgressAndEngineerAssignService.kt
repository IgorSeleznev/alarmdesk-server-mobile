package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.claim

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.request.ClaimStatusUpdateBriefRequest
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class ClaimStatusInProgressAndEngineerAssignService(
    private val statusUpdateService: ClaimStatusUpdateManager,
    private val engineerAssignService: ClaimEngineerAssignService
) {
    @Transactional
    fun claimInProgressAndEngineerAssign(request: ClaimStatusUpdateBriefRequest) {
        statusUpdateService.updateStatus(request)
        engineerAssignService.engineerAssign(
            request.claimId,
            request.authorId
        )
    }
}