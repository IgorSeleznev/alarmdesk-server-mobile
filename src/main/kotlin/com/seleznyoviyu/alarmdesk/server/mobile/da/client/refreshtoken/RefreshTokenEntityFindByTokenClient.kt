package com.seleznyoviyu.alarmdesk.server.mobile.da.client.refreshtoken

import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.refreshtoken.RefreshTokenEntity
import com.seleznyoviyu.alarmdesk.server.mobile.da.exception.NotFoundException
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Component
import java.time.Instant
import java.util.*

@Component
class RefreshTokenEntityFindByTokenClient(
    private val jdbcTemplate: NamedParameterJdbcTemplate
) {
    fun findByRefreshToken(refreshToken: String): RefreshTokenEntity {
        val result = jdbcTemplate.query(
            """
SELECT id, token, create_datetime, lifetime, user_account_id
FROM refresh_token
WHERE token = :token                
            """.trimIndent(),
            MapSqlParameterSource().addValue("token", refreshToken)
        ) { resultSet, _ ->
            RefreshTokenEntity(
                id = resultSet.getLong(1),
                token = resultSet.getObject(2, UUID::class.java),
                createDateTime = resultSet.getObject(3, Instant::class.java),
                lifetime = resultSet.getLong(4),
                userAccountId = resultSet.getLong(5)
            )
        }

        if (result.isNotEmpty()) {
            return result[0]
        }
        throw NotFoundException("Не найдены данные для refresh token $refreshToken")
    }
}