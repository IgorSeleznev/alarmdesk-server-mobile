package com.seleznyoviyu.alarmdesk.server.mobile.common.mapper

interface ConvertMapper<TSource, TTarget> {

    fun map(source: TSource): TTarget
}