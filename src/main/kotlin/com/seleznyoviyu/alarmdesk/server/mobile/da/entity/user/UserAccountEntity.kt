package com.seleznyoviyu.alarmdesk.server.mobile.da.entity.user

import java.util.*

data class UserAccountEntity(
    val id: Long?,
    val name: String,
    val login: String,
    val passwordHash: String,
    val role: String,
    val regionId: Long,
    val externalId: UUID?
)