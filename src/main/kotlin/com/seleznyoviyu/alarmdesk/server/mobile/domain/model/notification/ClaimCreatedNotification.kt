package com.seleznyoviyu.alarmdesk.server.mobile.domain.model.notification

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.ClaimIncidentTypeParameterValue
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user.SimplifiedUserAccount
import java.time.Instant

data class ClaimCreatedNotification(
    val claimId: Long,
    val author: SimplifiedUserAccount,
    val createdDateTime: Instant,
    val incidentTitles: List<String>,
    val incidentTypeId: Long,
    val incidentTypeParameterValues: Map<Long, ClaimIncidentTypeParameterValue>,
    val comment: String?
) : Notification
