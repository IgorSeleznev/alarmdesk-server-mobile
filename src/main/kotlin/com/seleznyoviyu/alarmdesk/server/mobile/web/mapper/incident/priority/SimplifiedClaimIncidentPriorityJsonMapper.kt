package com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.incident.priority

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.SimplifiedClaimIncidentPriority
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.claim.SimplifiedClaimIncidentPriorityJson
import org.mapstruct.InjectionStrategy
import org.mapstruct.Mapper

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
interface SimplifiedClaimIncidentPriorityJsonMapper {
    fun map(source: SimplifiedClaimIncidentPriority): SimplifiedClaimIncidentPriorityJson
}