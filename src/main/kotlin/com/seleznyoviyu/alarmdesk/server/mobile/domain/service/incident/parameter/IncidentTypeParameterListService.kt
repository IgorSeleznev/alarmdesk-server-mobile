package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.incident.parameter

import com.seleznyoviyu.alarmdesk.server.mobile.da.client.incident.IncidentTypeEntityFindByIdClient
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.IncidentTypeParameter
import org.springframework.stereotype.Service

@Service
class IncidentTypeParameterListService(
    private val client: IncidentTypeEntityFindByIdClient
) {
    fun parameterList(incidentTypeId: Long): List<IncidentTypeParameter> {
        return client.findById(incidentTypeId).parameters
    }
}