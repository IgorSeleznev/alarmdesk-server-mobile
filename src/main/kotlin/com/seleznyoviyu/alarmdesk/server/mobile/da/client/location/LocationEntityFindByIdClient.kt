package com.seleznyoviyu.alarmdesk.server.mobile.da.client.location

import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.location.LocationEntity
import com.seleznyoviyu.alarmdesk.server.mobile.da.exception.NotFoundException
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Component
import java.sql.ResultSet

@Component
class LocationEntityFindByIdClient(
    private val jdbcTemplate: NamedParameterJdbcTemplate
) {
    fun findById(id: Long): LocationEntity {
        val result: List<LocationEntity> = jdbcTemplate.query(
            """
select
    id,
    title,
    region_id,
    address,
    ukm_location
from location
where id = :id
            """.trimIndent(),
            MapSqlParameterSource().addValue("id", id)
        ) { resultSet: ResultSet, _: Int ->
            LocationEntity(
                id = resultSet.getLong(1),
                title = resultSet.getString(2),
                regionId = resultSet.getLong(3),
                address = resultSet.getString(4),
                ukmLocation = resultSet.getLong(5)
            )
        }

        if (result.isNotEmpty()) {
            return result[0]
        }
        throw NotFoundException("Не удалось найти локацию по id = $id")
    }
}