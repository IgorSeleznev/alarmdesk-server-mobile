package com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.request

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.claim.ClaimStatus

data class ClaimStatusUpdateBriefRequest(
    val claimId: Long,
    val comment: String,
    val authorId: Long,
    val newStatus: ClaimStatus
)
