package com.seleznyoviyu.alarmdesk.server.mobile.da.client.user

import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.user.UserAccountEntity
import com.seleznyoviyu.alarmdesk.server.mobile.da.rowmapper.user.UserAccountEntityRowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Component
import java.util.*

@Component
class UserAccountEntityFindByIdClient(
    private val jdbcTemplate: NamedParameterJdbcTemplate,
    private val rowMapper: UserAccountEntityRowMapper
) {
    fun findById(id: Long): Optional<UserAccountEntity> {
        val result: List<UserAccountEntity> = jdbcTemplate.query(
            """
select
    id,
    name,
    login,
    password_hash,
    role,
    region_id,
    external_id
from user_account
where id = :id
            """.trimIndent(),
            MapSqlParameterSource().addValue("id", id),
            rowMapper
        )

        if (result.isNotEmpty()) {
            return Optional.of(result[0])
        }
        return Optional.empty()
    }
}