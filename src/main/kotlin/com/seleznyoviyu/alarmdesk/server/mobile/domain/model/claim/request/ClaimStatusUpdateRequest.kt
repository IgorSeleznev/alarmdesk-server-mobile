package com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.request

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.Claim
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.UserAccountBrief

data class ClaimStatusUpdateRequest(
    val requestBrief: ClaimStatusUpdateBriefRequest,
    val author: UserAccountBrief,
    val sourceClaim: Claim
)
