package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.security

import com.seleznyoviyu.alarmdesk.server.mobile.web.configuration.JwtTokenUtil
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.security.AuthenticateRequest
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.security.UserAccountSecurityDetails
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Service

@Service
class JwtTokenBuildService(
    private val authenticationManager: AuthenticationManager,
    private val jwtTokenUtil: JwtTokenUtil,
) {
    fun buildJwtToken(request: AuthenticateRequest): String {
        val authenticate: Authentication = authenticationManager
            .authenticate(UsernamePasswordAuthenticationToken(request.login, request.password))
        val user = authenticate.principal as UserAccountSecurityDetails
        return jwtTokenUtil.generateAccessToken(user)!!
    }
}