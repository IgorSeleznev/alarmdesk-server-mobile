package com.seleznyoviyu.alarmdesk.server.mobile.domain.configuration

import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.scheduled.ScheduledService
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.TaskScheduler
import org.springframework.scheduling.annotation.SchedulingConfigurer
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler
import org.springframework.scheduling.config.ScheduledTaskRegistrar

@Configuration
class SchedulingConfiguration(
    private val scheduledServices:List<ScheduledService>
) : SchedulingConfigurer {

    companion object {
        const val SCHEDULER_NAME = "AlarmDeskTaskScheduler"
    }

    @Bean
    fun taskScheduler(): TaskScheduler {
        val scheduler = ThreadPoolTaskScheduler();
        scheduler.setThreadNamePrefix(SCHEDULER_NAME);
        scheduler.initialize();
        return scheduler;
    }

    override fun configureTasks(scheduledTaskRegistrar: ScheduledTaskRegistrar) {
        for (scheduledService in scheduledServices) {
            scheduledTaskRegistrar.addTriggerTask(
                scheduledService::execute,
                scheduledService::nextExecutionTime
            )
        }
    }
}