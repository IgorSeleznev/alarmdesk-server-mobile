package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.claim.detailed.constructor

import com.seleznyoviyu.alarmdesk.server.mobile.da.client.user.UserAccountEntityFindByIdClient
import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.claim.detailed.constructor.ClaimIncidentTypeDetailedClaimConstructor.Companion.INCIDENT_TYPE_CLAIM_DETAILED_CONSTRUCTOR_BEAN_NAME
import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.claim.detailed.constructor.ClaimUsersDetailedClaimConstructor.Companion.USERS_CLAIM_DETAILED_CONSTRUCTOR_BEAN_NAME
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Component
import java.util.*

@Qualifier(USERS_CLAIM_DETAILED_CONSTRUCTOR_BEAN_NAME)
@Component
class ClaimUsersDetailedClaimConstructor(
    private val userFindClient: UserAccountEntityFindByIdClient,
    @Qualifier(INCIDENT_TYPE_CLAIM_DETAILED_CONSTRUCTOR_BEAN_NAME) private val constructor: DetailedClaimConstructor
) : DetailedClaimConstructor {

    companion object {
        const val USERS_CLAIM_DETAILED_CONSTRUCTOR_BEAN_NAME = "ClaimUsersDetailedClaimConstructor"
    }

    override fun construct(builder: DetailedClaimBuilder): DetailedClaimBuilder {
        builder.author = userFindClient.findById(builder.claimEntity.authorId).get()
        builder.engineer = when (builder.claimEntity.engineerId) {
            null -> Optional.empty()
            else -> userFindClient.findById(builder.claimEntity.engineerId!!)
        }

        builder.statusUpdateAuthor = userFindClient.findById(builder.claimEntity.statusUpdateAuthorId).get()
        return constructor.construct(builder)
    }
}