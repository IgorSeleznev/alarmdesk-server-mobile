package com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.incident

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.IncidentType
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.incident.IncidentTypeJson
import org.mapstruct.InjectionStrategy
import org.mapstruct.Mapper

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
interface IncidentTypeJsonMapper {
    fun map(source: IncidentType): IncidentTypeJson
}