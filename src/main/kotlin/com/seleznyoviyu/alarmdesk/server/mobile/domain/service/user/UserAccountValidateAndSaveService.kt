package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.user

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user.UserAccount
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user.UserAccountSaveRequest
import org.springframework.stereotype.Service

@Service
class UserAccountValidateAndCreateService(
    private val validator: UserCreateRolesAllowValidator,
    private val saveService: UserAccountSaveService
) {
    fun saveUserAccount(saveRequest: UserAccountSaveRequest, author: UserAccount): Long {
        validator.validate(author.role, saveRequest.role)
        return saveService.saveUserAccount(saveRequest)
    }
}