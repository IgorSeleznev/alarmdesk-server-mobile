package com.seleznyoviyu.alarmdesk.server.mobile.domain.model.security

import org.springframework.security.core.GrantedAuthority

data class UserAccountSecurityAuthority(
    private val authority: String
) : GrantedAuthority {
    override fun getAuthority(): String {
        return authority
    }
}
