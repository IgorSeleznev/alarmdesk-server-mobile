package com.seleznyoviyu.alarmdesk.server.mobile.da.entity.claim

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.claim.ClaimStatus
import java.time.Instant

data class ClaimCommentLogEntity(
    val datetime: Instant,
    val authorId: AuthorUserAccountEntity,
    val status: ClaimStatus,
    val comment: String?
)
