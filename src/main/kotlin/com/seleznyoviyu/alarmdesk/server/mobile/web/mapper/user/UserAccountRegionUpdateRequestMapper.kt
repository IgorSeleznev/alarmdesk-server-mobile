package com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.user

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user.UserAccountRegionUpdateRequest
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.user.UserAccountRegionUpdateRequestJson
import org.mapstruct.InjectionStrategy
import org.mapstruct.Mapper

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
interface UserAccountRegionUpdateRequestMapper {
    fun map(source: UserAccountRegionUpdateRequestJson): UserAccountRegionUpdateRequest
}