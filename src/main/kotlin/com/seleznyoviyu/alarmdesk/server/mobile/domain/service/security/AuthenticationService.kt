package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.security

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.security.AuthenticateRequest
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.security.AuthenticationResponse
import org.springframework.stereotype.Service

@Service
class AuthenticationService(
    private val jwtTokenBuildService: JwtTokenBuildService,
    private val refreshTokenCreateService: RefreshTokenCreateService
) {
    fun authenticate(request: AuthenticateRequest): AuthenticationResponse {
        val jwtToken = jwtTokenBuildService.buildJwtToken(request)
        return AuthenticationResponse(
            jwtToken,
            refreshTokenCreateService.createRefreshToken(jwtToken).toString()
        )
    }
}