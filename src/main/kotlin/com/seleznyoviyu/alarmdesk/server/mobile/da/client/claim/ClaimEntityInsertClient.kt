package com.seleznyoviyu.alarmdesk.server.mobile.da.client.claim

import com.fasterxml.jackson.databind.ObjectMapper
import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.claim.ClaimEntity
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Component
import java.sql.Timestamp
import java.sql.Types

@Component
class ClaimEntityInsertClient(
    private val objectMapper: ObjectMapper,
    private val jdbcTemplate: NamedParameterJdbcTemplate
) {
    fun insert(claim: ClaimEntity) {
        jdbcTemplate.update(
            """
insert into claim (
    id,
    location_id,
    status,
    author_id,
    engineer_id,
    comment,
    incident_type_id,
    incident_type_parameter_values,
    incident_priority,
    status_update_author_id,
    create_datetime,
    status_update_datetime,
    engineer_assign_datetime,
    finish_datetime,
    technical_details,
    status_log,
    comment_log
) values (
    :id,
    :locationId,
    :status,
    :authorId,
    :engineerId,
    :comment,
    :incidentTypeId,
    :incidentTypeParameterValues,
    :incidentPriority,
    :statusUpdateAuthorId,
    :engineerAssignDateTime,
    :createdDateTime,
    :statusUpdateDateTime,
    :finishedDateTime,
    :technicalDetails,
    :statusLog,
    :commentLog
)    
            """.trimIndent(),
            MapSqlParameterSource()
                .addValue("id", claim.id)
                .addValue("locationId", claim.locationId)
                .addValue("status", claim.status.toString())
                .addValue("authorId", claim.authorId)
                .addValue("engineerId", claim.engineerId)
                .addValue("comment", claim.comment)
                .addValue("incidentTypeId", claim.incidentTypeId)
                .addValue(
                    "incidentTypeParameterValues",
                    objectMapper.writeValueAsString(
                        claim.incidentTypeParameterValues
                    )
                )
                .addValue("incidentPriority", claim.incidentPriority)
                .addValue("statusUpdateAuthorId", claim.statusUpdateAuthorId)
                .addValue(
                    "createdDateTime",
                    Timestamp.from(
                        claim.createDateTime
                    ),
                    Types.TIMESTAMP
                )
                .addValue(
                    "statusUpdateDateTime",
                    Timestamp.from(
                        claim.statusUpdateDateTime,
                    ),
                    Types.TIMESTAMP
                )
                .addValue(
                    "engineerAssignDateTime",
                    Timestamp.from(
                        claim.engineerAssignDateTime,
                    ),
                    Types.TIMESTAMP
                )
                .addValue(
                    "finishedDateTime",
                    Timestamp.from(
                        claim.finishDateTime,
                    ),
                    Types.TIMESTAMP
                )
                .addValue(
                    "technicalDetails",
                    objectMapper.writeValueAsString(
                        claim.technicalDetails
                    )
                )
                .addValue(
                    "statusLog",
                    objectMapper.writeValueAsString(
                        claim.statusLog
                    )
                )
                .addValue(
                    "commentLog",
                    objectMapper.writeValueAsString(
                        claim.commentLog
                    )
                )
        )
    }
}