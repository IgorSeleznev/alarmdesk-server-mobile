package com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.IncidentPriorityType
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.SimplifiedClaimIncidentType
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.location.SimplifiedLocation
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.claim.ClaimStatus
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user.SimplifiedUserAccount
import java.time.Instant

data class DetailedClaim(
    val id: Long?,
    val incident: SimplifiedClaimIncidentType,
    val incidentPriority: IncidentPriorityType,
    val incidentTypeParameterValues: Map<Long, DetailedClaimIncidentTypeParameterValue>,
    val location: SimplifiedLocation,
    val comment: String,
    val status: ClaimStatus,
    val engineer: SimplifiedUserAccount?,
    val author: SimplifiedUserAccount,
    val statusUpdateAuthor: SimplifiedUserAccount,
    val createDateTime: Instant,
    val statusUpdateDateTime: Instant,
    val finishDateTime: Instant?,
    val commentLog: List<ClaimCommentLog>,
    val statusLog: List<ClaimStatusLog>,
    val technicalDetails: Map<String, Map<String, String>>?
)
