package com.seleznyoviyu.alarmdesk.server.mobile

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class AlarmdeskServerMobileApplication

fun main(args: Array<String>) {
    runApplication<AlarmdeskServerMobileApplication>(*args)
}
