package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.security

import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.user.UserAccountEntityFindExternalIdByIdService
import com.seleznyoviyu.alarmdesk.server.mobile.da.client.common.NextIdClient
import com.seleznyoviyu.alarmdesk.server.mobile.da.client.refreshtoken.RefreshTokenEntityInsertClient
import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.refreshtoken.RefreshTokenEntity
import org.springframework.stereotype.Service
import java.time.Instant
import java.util.*

@Service
class RefreshTokenSaveService(
    private val findService: UserAccountEntityFindExternalIdByIdService,
    private val insertClient: RefreshTokenEntityInsertClient,
    private val nextIdClient: NextIdClient
) {
    fun save(refreshToken: UUID, userAccountExternalId: UUID) {
        insertClient.insert(
            RefreshTokenEntity(
                id = nextIdClient.nextId("refresh_token_seq"),
                token = refreshToken,
                createDateTime = Instant.now(),
                lifetime = 365 * 24 * 60 * 60 * 1000,
                userAccountId = findService.findByExternalId(userAccountExternalId).id!!
            )
        )
    }
}