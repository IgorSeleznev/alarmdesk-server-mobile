package com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.request

data class ClaimCreateRequestIncidentTypeParameterValue(
    val value: String?,
    val options: Map<Long, String>?
)