package com.seleznyoviyu.alarmdesk.server.mobile.web.controller.notification

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.BUSINESS_ADMIN
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.DEVELOPER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.ENGINEER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.HEAD_ENGINEER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.MOBILE_USER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.OPERATOR
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.SYSTEM_ADMIN
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.WEB_USER
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.notification.ClaimAssignedNotification
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.notification.ClaimCreatedNotification
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.notification.ClaimStatusUpdateNotification
import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.notification.CurrentUserNextNotificationService
import com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.notification.ClaimAssignedNotificationJsonMapper
import com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.notification.ClaimCreatedNotificationJsonMapper
import com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.notification.ClaimStatusUpdatedNotificationJsonMapper
import com.seleznyoviyu.alarmdesk.server.mobile.web.resolver.HttpSessionAuthenticatedUserResolver
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.annotation.security.RolesAllowed

@RestController
@RequestMapping("/notification")
class CurrentUserNextNotificationController(
    private val service: CurrentUserNextNotificationService,
    private val assignedMapper: ClaimAssignedNotificationJsonMapper,
    private val createdMapper: ClaimCreatedNotificationJsonMapper,
    private val statusUpdatedMapper: ClaimStatusUpdatedNotificationJsonMapper,
    private val userResolver: HttpSessionAuthenticatedUserResolver
) {
    @RolesAllowed(
        "ROLE_$MOBILE_USER",
        "ROLE_$DEVELOPER",
        "ROLE_$OPERATOR",
        "ROLE_$ENGINEER",
        "ROLE_$SYSTEM_ADMIN",
        "ROLE_$BUSINESS_ADMIN",
        "ROLE_$WEB_USER",
        "ROLE_$HEAD_ENGINEER"
    )
    @GetMapping("/next")
    fun nextNotification(): Any {
        val notification = service.nextNotification(userResolver.resolve().id!!)
        if (notification is ClaimCreatedNotification) {
            return createdMapper.map(notification)
        }
        if (notification is ClaimAssignedNotification) {
            return assignedMapper.map(notification)
        }
        if (notification is ClaimStatusUpdateNotification) {
            return statusUpdatedMapper.map(notification)
        }
        throw IllegalStateException("Неизвестный тип оповещения: $notification")
    }
}