package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.notification

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.Claim
import org.springframework.stereotype.Component

@Component
class ClaimEngineerAssignNotifyService(
    private val poolManager: NotificationPoolManager,
    private val notificationBuildService: ClaimEngineerAssignNotificationBuildService
) {
    fun notify(claim: Claim) {
        poolManager.put(
            claim.engineerId!!,
            notificationBuildService.buildNotification(claim)
        )
    }
}