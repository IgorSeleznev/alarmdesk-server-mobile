package com.seleznyoviyu.alarmdesk.server.mobile.web.controller.engineer

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.claim.ClaimStatus
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.DEVELOPER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.HEAD_ENGINEER
import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.engineer.SubordinateEngineerListService
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.user.SimplifyUserAccountJson
import com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.user.SimplifyUserAccountJsonMapper
import com.seleznyoviyu.alarmdesk.server.mobile.web.resolver.HttpSessionAuthenticatedUserResolver
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.annotation.security.RolesAllowed

@RestController
@RequestMapping("/engineer")
class SubordinateEngineerListController(
    private val service: SubordinateEngineerListService,
    private val mapper: SimplifyUserAccountJsonMapper,
    private val userResolver: HttpSessionAuthenticatedUserResolver
) {

    @RolesAllowed(
        "ROLE_$DEVELOPER",
        "ROLE_$HEAD_ENGINEER"
    )
    @GetMapping("/subordinate/list/{status}")
    fun subordinateEngineerList(@PathVariable status: ClaimStatus): List<SimplifyUserAccountJson> {
        return service.list(
            status, userResolver.resolve().id!!
        ).map(mapper::map)
    }

    @RolesAllowed(
        DEVELOPER,
        HEAD_ENGINEER
    )
    @GetMapping("/subordinate/list")
    fun subordinateEngineerList(): List<SimplifyUserAccountJson> {
        return service.list(
            userResolver.resolve().id!!
        ).map(mapper::map)
    }
}