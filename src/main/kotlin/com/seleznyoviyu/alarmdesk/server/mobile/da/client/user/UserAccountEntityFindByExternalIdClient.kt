package com.seleznyoviyu.alarmdesk.server.mobile.da.client.user

import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.user.UserAccountEntity
import com.seleznyoviyu.alarmdesk.server.mobile.da.exception.NotFoundException
import com.seleznyoviyu.alarmdesk.server.mobile.da.rowmapper.user.UserAccountEntityRowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Component
import java.util.*

@Component
class UserAccountEntityFindByExternalIdClient(
    private val jdbcTemplate: NamedParameterJdbcTemplate,
    private val rowMapper: UserAccountEntityRowMapper
) {
    fun findByExternalId(externalId: UUID): UserAccountEntity {
        val result: List<UserAccountEntity> = jdbcTemplate.query(
            """
select
    id,
    name,
    login,
    password_hash,
    role,
    region_id,
    external_id
from user_account
where external_id = :externalId
            """.trimIndent(),
            MapSqlParameterSource().addValue("externalId", externalId),
            rowMapper
        )

        if (result.isNotEmpty()) {
            return result[0]
        }
        throw NotFoundException("Не найден пользователь для external id = $externalId")
    }
}