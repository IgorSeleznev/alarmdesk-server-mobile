package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.engineer

import com.seleznyoviyu.alarmdesk.server.mobile.da.client.engineer.EngineerTotalsClient
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.engineer.response.EngineerTotalsResponse
import org.springframework.stereotype.Service
import java.util.*

@Service
class EngineerTotalsService(
    private val totalsDao: EngineerTotalsClient
) {
    fun engineerTotals(engineerId: Long): EngineerTotalsResponse {
        val totals = totalsDao.engineerTotals(engineerId)
        return EngineerTotalsResponse(
            justCreated = totals.justCreated,
            inProgress = totals.inProgress,
            finished = totals.finished
        )
    }
}