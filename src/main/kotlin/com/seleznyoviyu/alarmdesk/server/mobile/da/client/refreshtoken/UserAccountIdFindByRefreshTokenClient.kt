package com.seleznyoviyu.alarmdesk.server.mobile.da.client.refreshtoken

import com.seleznyoviyu.alarmdesk.server.mobile.da.exception.NotFoundException
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Component
import java.util.*

@Component
class UserAccountIdFindByRefreshTokenClient(
    private val jdbcTemplate: NamedParameterJdbcTemplate
) {
    fun findUserAccountIdByRefreshToken(refreshToken: UUID): Long {
        val result = jdbcTemplate.query(
            """
SELECT user_account_id
FROM refresh_token
WHERE token = :token                
            """.trimIndent(),
            MapSqlParameterSource().addValue("token", refreshToken)
        ) { resultSet, _ ->
            resultSet.getLong(1)
        }

        if (result.isNotEmpty()) {
            return result[0]
        }
        throw NotFoundException("Не найден refresh token $refreshToken")
    }
}