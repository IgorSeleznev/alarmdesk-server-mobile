package com.seleznyoviyu.alarmdesk.server.mobile.da.entity.claim

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.claim.ClaimStatus
import java.time.Instant

data class ClaimEntityStatusUpdateRequest(
    val claimId: Long,
    val newStatus: ClaimStatus,
    val authorId: Long,
    val statusUpdateDateTime: Instant,
    val comment: String
)