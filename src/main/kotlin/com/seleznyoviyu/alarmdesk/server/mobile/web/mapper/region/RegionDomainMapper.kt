package com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.region

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.region.Region
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.region.RegionJson
import org.mapstruct.InjectionStrategy
import org.mapstruct.Mapper

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
interface RegionDomainMapper {
    fun map(source: RegionJson): Region
}