package com.seleznyoviyu.alarmdesk.server.mobile.web.controller.claim

import com.seleznyoviyu.alarmdesk.server.mobile.web.json.claim.ClaimCreateByMobileUserRequestJson
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.request.ClaimCreateByMobileUserRequest
import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.claim.ClaimCreateByMobileService
import com.seleznyoviyu.alarmdesk.server.mobile.web.resolver.HttpSessionAuthenticatedUserResolver
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/claim")
class ClaimCreateByMobileUserController(
    private val service: ClaimCreateByMobileService,
    private val userResolver: HttpSessionAuthenticatedUserResolver,
) {
    /*@RolesAllowed(
        "ROLE_$MOBILE_USER",
        "ROLE_$DEVELOPER"
    )*/
    @PostMapping("/create-by-mobile-user")
    fun createClaim(@RequestBody request: ClaimCreateByMobileUserRequestJson) {
        service.createClaim(
            ClaimCreateByMobileUserRequest(
                incidentTypeId = request.incidentTypeId,
                incidentTypeParameterValues = request.incidentTypeParameterValues,
                comment = request.comment,
                authorId = userResolver.resolve().id!!
            )
        )
    }
}