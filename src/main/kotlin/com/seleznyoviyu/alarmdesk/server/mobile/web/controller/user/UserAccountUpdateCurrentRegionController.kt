package com.seleznyoviyu.alarmdesk.server.mobile.web.controller.user

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.BUSINESS_ADMIN
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.DEVELOPER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.ENGINEER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.HEAD_ENGINEER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.MOBILE_USER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.OPERATOR
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.SYSTEM_ADMIN
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.WEB_USER
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user.UserAccountRegionUpdateRequest
import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.user.UserAccountRegionUpdateService
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.common.IdRequestJson
import com.seleznyoviyu.alarmdesk.server.mobile.web.resolver.HttpSessionAuthenticatedUserResolver
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.annotation.security.RolesAllowed

@RestController
@RequestMapping("/user-account")
class UserAccountUpdateCurrentRegionController(
    private val service: UserAccountRegionUpdateService,
    private val userResolver: HttpSessionAuthenticatedUserResolver
) {
    @RolesAllowed(
        "ROLE_$MOBILE_USER",
        "ROLE_$DEVELOPER",
        "ROLE_$OPERATOR",
        "ROLE_$ENGINEER",
        "ROLE_$SYSTEM_ADMIN",
        "ROLE_$BUSINESS_ADMIN",
        "ROLE_$WEB_USER",
        "ROLE_$HEAD_ENGINEER"
    )
    @PutMapping("/current-update-region")
    fun updateRegion(@RequestBody request: IdRequestJson) {
        service.updateRegion(
            UserAccountRegionUpdateRequest(
                userAccountId = userResolver.resolve().id!!,
                regionId = request.id
            )
        )
    }
}