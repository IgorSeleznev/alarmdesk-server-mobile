package com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.claim

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.SimplifiedClaim
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.claim.SimplifiedClaimJson
import org.mapstruct.InjectionStrategy
import org.mapstruct.Mapper

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
interface SimplifiedClaimJsonMapper {
    fun map(source: SimplifiedClaim): SimplifiedClaimJson
}