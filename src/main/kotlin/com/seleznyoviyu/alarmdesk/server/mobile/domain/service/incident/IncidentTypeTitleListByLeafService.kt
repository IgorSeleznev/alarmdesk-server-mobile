package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.incident

import com.seleznyoviyu.alarmdesk.server.mobile.da.client.incident.IncidentTypeEntityFindByIdClient
import com.seleznyoviyu.alarmdesk.server.mobile.da.client.incident.IncidentTypeEntityFindParentByIdClient
import org.springframework.stereotype.Service
import java.util.*

@Service
class IncidentTypeTitleListByLeafService(
    private val findParentClient: IncidentTypeEntityFindParentByIdClient,
    private val findClient: IncidentTypeEntityFindByIdClient
) {
    fun listTitleByLeaf(leafIncidentTypeId: Long): List<String> {
        var actual = findClient.findById(leafIncidentTypeId)
        if (actual.parentId == null) {
            return listOf(actual.title)
        }
        val titles = LinkedList<String>()
        while (actual.parentId != null) {
            titles.addFirst(actual.title)
            actual = findParentClient.findParentById(actual.parentId!!)
        }
        return titles
    }
}