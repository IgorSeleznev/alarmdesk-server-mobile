package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.claim.detailed.constructor

import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.claim.detailed.constructor.ClaimUsersDetailedClaimConstructor.Companion.USERS_CLAIM_DETAILED_CONSTRUCTOR_BEAN_NAME
import com.seleznyoviyu.alarmdesk.server.mobile.da.client.claim.ClaimEntityFindByIdClient
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Primary
import org.springframework.stereotype.Component

@Primary
@Component
class ClaimEntityDetailedClaimConstructor(
    private val client: ClaimEntityFindByIdClient,
    @Qualifier(USERS_CLAIM_DETAILED_CONSTRUCTOR_BEAN_NAME)
    private val constructor: DetailedClaimConstructor
) : DetailedClaimConstructor {

    override fun construct(builder: DetailedClaimBuilder): DetailedClaimBuilder {
        builder.claimEntity = client.findById(builder.claimId)
        return constructor.construct(builder)
    }
}