package com.seleznyoviyu.alarmdesk.server.mobile.da.entity.user

data class SimplifiedUserAccountEntity(
    val id: Long,
    val name: String
)
