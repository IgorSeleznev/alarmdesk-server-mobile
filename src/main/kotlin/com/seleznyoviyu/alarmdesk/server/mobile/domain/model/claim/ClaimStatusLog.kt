package com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.claim.ClaimStatus
import java.time.Instant

data class ClaimStatusLog(
    val datetime: Instant,
    val author: UserAccountBrief,
    val status: ClaimStatus
)