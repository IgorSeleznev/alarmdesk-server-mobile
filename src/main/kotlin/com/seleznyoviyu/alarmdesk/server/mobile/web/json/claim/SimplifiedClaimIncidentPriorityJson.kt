package com.seleznyoviyu.alarmdesk.server.mobile.web.json.claim

data class SimplifiedClaimIncidentPriorityJson(
    val name: String,
    val title: String
)
