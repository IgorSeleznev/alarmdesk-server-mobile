package com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.claim.ClaimStatus
import java.time.Instant

data class SimplifiedClaim(
    val id: Long,
    val incidentTypeTitle: String,
    val incidentPriorityTitle: String,
    val locationName: String,
    val status: ClaimStatus,
    val authorName: String,
    val engineerName: String,
    val createdDatetime: Instant,
    val comment: String
)