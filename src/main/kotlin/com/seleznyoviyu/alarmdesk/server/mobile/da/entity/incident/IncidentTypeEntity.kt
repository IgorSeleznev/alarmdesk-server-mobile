package com.seleznyoviyu.alarmdesk.server.mobile.da.entity.incident

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.IncidentPriorityType
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.IncidentTypeParameter

data class IncidentTypeEntity(
    val id: Long?,
    val parentId: Long?,
    val title: String,
    val defaultPriority: IncidentPriorityType,
    val parameters: List<IncidentTypeParameter>,
    val isEquipmentDescribe: Boolean,
    val incidentResolveSla: Int,
    val incidentReactionSla: Int,
    val isLast: Boolean
)