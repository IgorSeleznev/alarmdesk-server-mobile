package com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim

data class UserAccountBrief(
    val userAccountId: Long,
    val name: String,
    val login: String
)
