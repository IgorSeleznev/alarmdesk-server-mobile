package com.seleznyoviyu.alarmdesk.server.mobile.domain.model.engineer.response

data class EngineerTotalsResponse(
    val justCreated: Int,
    val inProgress: Int,
    val finished: Int
)
