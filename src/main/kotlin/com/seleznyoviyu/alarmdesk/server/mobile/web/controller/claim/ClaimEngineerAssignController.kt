package com.seleznyoviyu.alarmdesk.server.mobile.web.controller.claim

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.BUSINESS_ADMIN
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.DEVELOPER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.HEAD_ENGINEER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.OPERATOR
import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.claim.ClaimEngineerAssignService
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.claim.ClaimEngineerAssignRequestJson
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.annotation.security.RolesAllowed

@RestController
@RequestMapping("/claim")
class ClaimEngineerAssignController(
    private val service: ClaimEngineerAssignService
) {

    @RolesAllowed(
        "ROLE_$DEVELOPER",
        "ROLE_$OPERATOR",
        "ROLE_$BUSINESS_ADMIN",
        "ROLE_$HEAD_ENGINEER"
    )
    @PutMapping("/engineer-assign")
    fun engineerAssign(request: ClaimEngineerAssignRequestJson) {
        service.engineerAssign(request.claimId, request.engineerId)
    }
}