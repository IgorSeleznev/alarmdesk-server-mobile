package com.seleznyoviyu.alarmdesk.server.mobile.web.controller.user

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.BUSINESS_ADMIN
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.DEVELOPER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.ENGINEER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.HEAD_ENGINEER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.MOBILE_USER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.OPERATOR
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.SYSTEM_ADMIN
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.WEB_USER
import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.security.UserAccountSecurityTokensRefreshService
import com.seleznyoviyu.alarmdesk.server.mobile.web.configuration.JwtTokenUtil
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.mobile.authenticate.AuthenticateResponseJson
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.refreshtoken.RefreshTokenRequestJson
import com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.security.AuthenticateResponseJsonMapper
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.annotation.security.RolesAllowed

@RestController
@RequestMapping("/user-account")
class UserAccountSecurityTokensRefreshController(
    private val refreshService: UserAccountSecurityTokensRefreshService,
    private val responseMapper: AuthenticateResponseJsonMapper
) {
    @RolesAllowed(
        "ROLE_$MOBILE_USER",
        "ROLE_$DEVELOPER",
        "ROLE_$OPERATOR",
        "ROLE_$ENGINEER",
        "ROLE_$SYSTEM_ADMIN",
        "ROLE_$BUSINESS_ADMIN",
        "ROLE_$WEB_USER",
        "ROLE_$HEAD_ENGINEER"
    )
    @PostMapping("/refresh-token")
    fun refreshTokens(@RequestBody request: RefreshTokenRequestJson): ResponseEntity<AuthenticateResponseJson?>? {
        return try {
            val response = refreshService.refreshTokens(request.refreshToken)
            ResponseEntity.ok()
                //.header(HttpHeaders.AUTHORIZATION, jwtToken)
                .header(JwtTokenUtil.ACCESS_TOKEN_HEADER, response.accessToken)
                .header(JwtTokenUtil.REFRESH_TOKEN_HEADER, response.refreshToken)
                .body(
                    responseMapper.map(response)
                )
        } catch (ex: BadCredentialsException) {
            ResponseEntity.status(HttpStatus.UNAUTHORIZED).build<AuthenticateResponseJson>()
        }

    }
}