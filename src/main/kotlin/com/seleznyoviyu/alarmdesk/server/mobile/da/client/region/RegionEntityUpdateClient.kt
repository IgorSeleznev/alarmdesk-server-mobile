package com.seleznyoviyu.alarmdesk.server.mobile.da.client.region

import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.region.RegionEntity
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Component

@Component
class RegionEntityUpdateClient(
    private val jdbcTemplate: NamedParameterJdbcTemplate
) {
    fun update(region: RegionEntity): Long {
        jdbcTemplate.update(
            """
UPDATE region
SET title = :title
WHERE id = :id 
            """.trimIndent(),
            MapSqlParameterSource()
                .addValue("id", region.id)
                .addValue("title", region.title)
        )
        return region.id!!
    }
}