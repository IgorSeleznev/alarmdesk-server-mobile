package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.incident

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.IncidentPriorityType
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.SimplifiedClaimIncidentPriority
import org.springframework.stereotype.Service

@Service
class IncidentPriorityListService {
    fun list(): List<SimplifiedClaimIncidentPriority> {
        return IncidentPriorityType.values().asList().map {
            SimplifiedClaimIncidentPriority(
                it.name,
                it.title
            )
        }
    }
}