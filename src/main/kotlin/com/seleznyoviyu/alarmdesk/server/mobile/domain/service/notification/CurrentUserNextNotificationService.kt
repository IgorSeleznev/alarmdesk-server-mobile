package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.notification

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.notification.Notification
import org.springframework.stereotype.Service

@Service
class CurrentUserNextNotificationService(
    private val poolManager: NotificationPoolManager
) {
    fun nextNotification(userAccountId: Long): Notification {
        return poolManager.nextNotification(userAccountId)
    }
}