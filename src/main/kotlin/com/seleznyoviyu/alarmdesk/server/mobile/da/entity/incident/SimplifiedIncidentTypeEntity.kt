package com.seleznyoviyu.alarmdesk.server.mobile.da.entity.incident

data class SimplifiedIncidentTypeEntity(
    val id: Long,
    val parentId: Long?,
    val title: String,
    val isLast: Boolean
)