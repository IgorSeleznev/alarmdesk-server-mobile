package com.seleznyoviyu.alarmdesk.server.mobile.da.client.engineer

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.claim.ClaimStatus
import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.user.UserAccountEntity
import com.seleznyoviyu.alarmdesk.server.mobile.da.rowmapper.user.UserAccountEntityRowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Component

@Component
class UserAccountSubordinateListByClaimStatusClient(
    private val jdbcTemplate: NamedParameterJdbcTemplate,
    private val rowMapper: UserAccountEntityRowMapper
) {
    fun subordinateListByClamStatus(claimStatus: ClaimStatus, headEngineerId: Long): List<UserAccountEntity> {
        return jdbcTemplate.query(
            """
SELECT 
       id,
       name,
       login,
       password_hash,
       role,
       region_id
FROM user_account ua
     INNER JOIN subordinate_engineer se ON se.engineer_user_account_id = ua.id
     INNER JOIN claim c ON c.engineer_id = ua.id 
WHERE se.head_engineer_user_account_id = :headEngineerId 
  AND c.status = :claimStatus                               
            """.trimIndent(),
            MapSqlParameterSource()
                .addValue("headEngineerId", headEngineerId)
                .addValue("claimStatus", claimStatus),
            rowMapper
        )
    }
}