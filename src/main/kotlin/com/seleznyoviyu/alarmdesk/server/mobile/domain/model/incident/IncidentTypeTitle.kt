package com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident

data class IncidentTypeTitle(
    val id: Long?,
    val title: String,
    val isLast: Boolean
)
