package com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.engineer

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.engineer.request.EngineerSubordinateUpdateRequest
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.engineer.EngineerSubordinateUpdateRequestJson
import org.mapstruct.InjectionStrategy
import org.mapstruct.Mapper

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
interface EngineerSubordinateUpdateRequestDomainMapper {
    fun map(source: EngineerSubordinateUpdateRequestJson): EngineerSubordinateUpdateRequest
}