package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.location

import com.seleznyoviyu.alarmdesk.server.mobile.da.client.location.LocationListAllClient
import com.seleznyoviyu.alarmdesk.server.mobile.domain.mapper.location.LocationMapper
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.location.Location
import org.springframework.stereotype.Service

@Service
class LocationListAllService(
    private val client: LocationListAllClient,
    private val mapper: LocationMapper
) {
    fun listAll(): List<Location> {
        return client.listAll().map(mapper::map)
    }
}