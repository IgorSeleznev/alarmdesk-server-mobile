package com.seleznyoviyu.alarmdesk.server.mobile.web.resolver

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.security.UserAccountSecurityDetails
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user.UserAccount
import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.user.UserAccountEntityFindByLoginService
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Component

@Component
class HttpSessionAuthenticatedUserResolver(
    private val findByLoginService: UserAccountEntityFindByLoginService
    //private val membersManager: AuthenticatedMembersManager<UserAccount, String>
) {
    fun resolve(): UserAccount {
        val principal = SecurityContextHolder.getContext().authentication.principal
        if (principal is UserAccountSecurityDetails) {
            return principal.userAccount
        }
        if (principal is UserDetails) {
            return findByLoginService.findByLogin(
                principal.username
            ).get()
        }
        return findByLoginService.findByLogin(
            principal.toString()
        ).get()
        //return (SecurityContextHolder.getContext().authentication.principal as UserAccount)
    }
}