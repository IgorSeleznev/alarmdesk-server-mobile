package com.seleznyoviyu.alarmdesk.server.mobile.domain.mapper.claim

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.SimplifiedClaim
import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.claim.SimplifiedClaimEntity
import org.mapstruct.InjectionStrategy
import org.mapstruct.Mapper


@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
interface SimplifiedClaimMapper {
    fun map(source: SimplifiedClaimEntity): SimplifiedClaim
}