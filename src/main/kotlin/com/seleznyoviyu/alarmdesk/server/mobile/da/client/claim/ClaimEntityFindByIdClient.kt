package com.seleznyoviyu.alarmdesk.server.mobile.da.client.claim

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.claim.ClaimStatus
import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.claim.ClaimEntity
import com.seleznyoviyu.alarmdesk.server.mobile.da.exception.NotFoundException
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.IncidentPriorityType
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Component
import java.time.Instant

@Component
class ClaimEntityFindByIdClient(
    private val objectMapper: ObjectMapper,
    private val jdbcTemplate: NamedParameterJdbcTemplate
) {
    fun findById(id: Long): ClaimEntity {
        val result = jdbcTemplate.query(
            """
SELECT
    id,
    location_id,
    status,
    engineer_id,
    author_id,
    comment,
    incident_type_id,
    incident_priority,
    incident_type_parameter_values::jsonb,
    status_update_author_id,
    create_datetime,
    status_update_datetime,
    engineer_assign_datetime,
    finish_datetime,
    technical_details::jsonb,
    status_log::jsonb,
    comment_log::jsonb
FROM claim
WHERE id = :id
            """.trimIndent(),
            MapSqlParameterSource().addValue("id", id)
        ) { resultSet, _ ->
            ClaimEntity(
                id = resultSet.getLong(1),
                locationId = resultSet.getLong(2),
                status = ClaimStatus.valueOf(
                    resultSet.getString(3)
                ),
                engineerId = resultSet.getLong(4),
                authorId = resultSet.getLong(5),
                comment = resultSet.getString(6),
                incidentTypeId = resultSet.getLong(7),
                incidentTypeParameterValues = objectMapper.readValue(
                    resultSet.getString(8)
                ),
                incidentPriority = IncidentPriorityType.valueOf(
                    resultSet.getString(9)
                ),
                statusUpdateAuthorId = resultSet.getLong(10),
                createDateTime = resultSet.getObject(11, Instant::class.java),
                statusUpdateDateTime = resultSet.getObject(12, Instant::class.java),
                engineerAssignDateTime = resultSet.getObject(13, Instant::class.java),
                finishDateTime = resultSet.getObject(14, Instant::class.java),
                technicalDetails = objectMapper.readValue(
                    resultSet.getString(15)
                ),
                statusLog = objectMapper.readValue(
                    resultSet.getString(16)
                ),
                commentLog = objectMapper.readValue(
                    resultSet.getString(17)
                )
            )
        }
        if (result.isNotEmpty()) {
            return result[0]
        }
        throw NotFoundException("Не найдена заявка с id = $id")
    }
}