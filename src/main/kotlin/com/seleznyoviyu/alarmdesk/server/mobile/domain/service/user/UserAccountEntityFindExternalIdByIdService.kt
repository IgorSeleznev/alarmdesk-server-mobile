package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.user

import com.seleznyoviyu.alarmdesk.server.mobile.da.client.user.UserAccountEntityFindByExternalIdClient
import com.seleznyoviyu.alarmdesk.server.mobile.domain.mapper.user.UserAccountMapper
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user.UserAccount
import org.springframework.stereotype.Service
import java.util.*

@Service
class UserAccountEntityFindExternalIdByIdService(
    private val findClient: UserAccountEntityFindByExternalIdClient,
    private val mapper: UserAccountMapper
) {
    fun findByExternalId(externalId: UUID): UserAccount {
        return mapper.map(
            findClient.findByExternalId(externalId)
        )
    }
}