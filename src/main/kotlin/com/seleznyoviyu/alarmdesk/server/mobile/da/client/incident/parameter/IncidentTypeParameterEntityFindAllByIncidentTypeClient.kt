package com.seleznyoviyu.alarmdesk.server.mobile.da.client.incident.parameter

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.incident.IncidentValueType
import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.incident.parameter.IncidentTypeParameterEntity
import com.seleznyoviyu.alarmdesk.server.mobile.da.exception.NotFoundException
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Component
import java.sql.ResultSet

@Component
class IncidentTypeParameterEntityFindAllByIncidentTypeIdClient(
    private val jdbcTemplate: NamedParameterJdbcTemplate,
    private val objectMapper: ObjectMapper
) {
    fun findByIncidentTypeId(id: Long): IncidentTypeParameterEntity {
        val result: List<IncidentTypeParameterEntity> = jdbcTemplate.query(
            """
select
    id,
    title,
    incident_value_type,
    sort_order,
    options
from incident_type_parameter
where id = :id
            """.trimIndent(),
            MapSqlParameterSource().addValue("id", id)
        ) { resultSet: ResultSet, _: Int ->
            IncidentTypeParameterEntity(
                id = resultSet.getLong(1),
                title = resultSet.getString(2),
                incidentValueType = IncidentValueType.valueOf(
                    resultSet.getString(3)
                ),
                sortOrder = resultSet.getInt(4),
                options = objectMapper.readValue(
                    resultSet.getString(4)
                )
            )
        }

        if (result.isNotEmpty()) {
            return result[0]
        }
        throw NotFoundException("Список параметров для типа инцидента не найден по указанному идентификатору типа инцидента $id")
    }
}