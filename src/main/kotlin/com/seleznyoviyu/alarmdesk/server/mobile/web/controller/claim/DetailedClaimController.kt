package com.seleznyoviyu.alarmdesk.server.mobile.web.controller.claim

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.BUSINESS_ADMIN
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.DEVELOPER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.ENGINEER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.HEAD_ENGINEER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.OPERATOR
import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.claim.detailed.DetailedClaimService
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.claim.DetailedClaimJson
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.common.IdRequestJson
import com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.claim.DetailedClaimJsonMapper
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.annotation.security.RolesAllowed

@RestController
@RequestMapping("/claim")
class DetailedClaimController(
    private val service: DetailedClaimService,
    private val mapper: DetailedClaimJsonMapper
) {

    @RolesAllowed(
        "ROLE_$ENGINEER",
        "ROLE_$HEAD_ENGINEER",
        "ROLE_$OPERATOR",
        "ROLE_$DEVELOPER",
        "ROLE_$BUSINESS_ADMIN"
    )
    @PostMapping("/detailed")
    fun getClaim(@RequestBody request: IdRequestJson): DetailedClaimJson {
        return mapper.map(
            service.detailedClaim(request.id)
        )
    }
}