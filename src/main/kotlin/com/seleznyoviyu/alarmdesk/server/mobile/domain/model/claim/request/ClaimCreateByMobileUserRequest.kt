package com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.request

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.ClaimIncidentTypeParameterValue

data class ClaimCreateByMobileUserRequest(
    val authorId: Long,
    val incidentTypeId: Long,
    val incidentTypeParameterValues: Map<Long, ClaimIncidentTypeParameterValue>,
    val comment: String?
)