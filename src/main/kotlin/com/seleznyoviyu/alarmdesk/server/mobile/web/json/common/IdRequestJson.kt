package com.seleznyoviyu.alarmdesk.server.mobile.web.json.common

data class IdRequestJson(
    val id: Long
)