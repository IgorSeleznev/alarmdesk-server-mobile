package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.user

import com.seleznyoviyu.alarmdesk.server.mobile.web.configuration.UserAccountSecurityRepository
import org.springframework.context.annotation.Primary
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Primary
@Service
class UserAccountSecurityDetailsService(
    private val userAccountSecurityRepository: UserAccountSecurityRepository
) : UserDetailsService {
    override fun loadUserByUsername(username: String?): UserDetails {
        return userAccountSecurityRepository
            .findByLogin(username)
            .orElseThrow {
                UsernameNotFoundException(
                    java.lang.String.format("User: %s, not found", username)
                )
            }
    }
}