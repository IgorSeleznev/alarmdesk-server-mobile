package com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident

data class SimplifiedClaimIncidentType(
    val id: Long?,
    val title: String
)