package com.seleznyoviyu.alarmdesk.server.mobile.common.model.user

object UserRole {
    const val WEB_USER = "WEB_USER"
    const val MOBILE_USER = "MOBILE_USER"
    const val ENGINEER = "ENGINEER"
    const val OPERATOR = "OPERATOR"
    const val HEAD_ENGINEER = "HEAD_ENGINEER"
    const val DEVELOPER = "DEVELOPER"
    const val SYSTEM_ADMIN = "SYSTEM_ADMIN"
    const val BUSINESS_ADMIN = "BUSINESS_ADMIN"
}