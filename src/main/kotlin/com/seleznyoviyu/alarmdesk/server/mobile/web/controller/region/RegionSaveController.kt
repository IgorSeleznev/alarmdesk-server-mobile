package com.seleznyoviyu.alarmdesk.server.mobile.web.controller.region

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.BUSINESS_ADMIN
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.DEVELOPER
import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.region.RegionSaveService
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.common.IdResponseJson
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.region.RegionJson
import com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.region.RegionDomainMapper
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.annotation.security.RolesAllowed

@RestController
@RequestMapping("/region")
class RegionSaveController(
    private val service: RegionSaveService,
    private val mapper: RegionDomainMapper
) {
    @RolesAllowed(
        "ROLE_$DEVELOPER",
        "ROLE_$BUSINESS_ADMIN"
    )
    @PostMapping("/save")
    fun save(@RequestBody request: RegionJson): IdResponseJson {
        return IdResponseJson(
            id = service.save(
                mapper.map(request)
            )
        )
    }
}