package com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.notification

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.notification.ClaimStatusUpdateNotification
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.notification.ClaimStatusUpdateNotificationJson
import org.mapstruct.InjectionStrategy
import org.mapstruct.Mapper

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
interface ClaimStatusUpdatedNotificationJsonMapper {
    fun map(source: ClaimStatusUpdateNotification): ClaimStatusUpdateNotificationJson
}