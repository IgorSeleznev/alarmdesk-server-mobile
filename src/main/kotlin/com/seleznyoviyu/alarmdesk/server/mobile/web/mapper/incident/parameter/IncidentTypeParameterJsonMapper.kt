package com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.incident.parameter

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.IncidentTypeParameter
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.incident.parameter.IncidentTypeParameterJson
import org.mapstruct.InjectionStrategy
import org.mapstruct.Mapper

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
interface IncidentTypeParameterJsonMapper {
    fun map(source: IncidentTypeParameter): IncidentTypeParameterJson
}