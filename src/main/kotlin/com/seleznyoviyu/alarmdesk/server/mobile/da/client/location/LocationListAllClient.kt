package com.seleznyoviyu.alarmdesk.server.mobile.da.client.location

import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.location.LocationEntity
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Component

@Component
class LocationListAllClient(
    private val jdbcTemplate: NamedParameterJdbcTemplate
) {
    fun listAll(): List<LocationEntity> {
        return jdbcTemplate.query(
            """
SELECT 
    id,
    ukm_location,
    title,
    region_id,
    address
FROM location
            """.trimIndent()
        ) { resultSet, _ ->
            LocationEntity(
                id = resultSet.getLong(1),
                ukmLocation = resultSet.getLong(2),
                title = resultSet.getString(3),
                regionId = resultSet.getLong(4),
                address = resultSet.getString(5)
            )
        }
    }
}