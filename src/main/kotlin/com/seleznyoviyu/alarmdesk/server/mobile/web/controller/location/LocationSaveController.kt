package com.seleznyoviyu.alarmdesk.server.mobile.web.controller.location

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.BUSINESS_ADMIN
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.DEVELOPER
import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.location.LocationSaveService
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.common.IdResponseJson
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.location.LocationJson
import com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.location.LocationDomainMapper
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.annotation.security.RolesAllowed

@RestController
@RequestMapping("/location")
class LocationSaveController(
    private val service: LocationSaveService,
    private val mapper: LocationDomainMapper
) {
    @RolesAllowed(
        "ROLE_$DEVELOPER",
        "ROLE_$BUSINESS_ADMIN"
    )
    @PostMapping("/save")
    fun save(@RequestBody request: LocationJson): IdResponseJson {
        return IdResponseJson(
            service.save(
                mapper.map(request)
            )
        )
    }
}