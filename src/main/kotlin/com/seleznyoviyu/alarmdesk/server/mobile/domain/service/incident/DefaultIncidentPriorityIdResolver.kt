package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.incident

import com.seleznyoviyu.alarmdesk.server.mobile.da.client.incident.IncidentTypeEntityFindByIdClient
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.IncidentPriorityType
import org.springframework.stereotype.Component
import java.util.*

@Component
class DefaultIncidentPriorityIdResolver(
    private val findClient: IncidentTypeEntityFindByIdClient
) {
    fun defaultPriority(incidentTypeId: Long): IncidentPriorityType {
        return findClient.findById(incidentTypeId).defaultPriority
    }
}