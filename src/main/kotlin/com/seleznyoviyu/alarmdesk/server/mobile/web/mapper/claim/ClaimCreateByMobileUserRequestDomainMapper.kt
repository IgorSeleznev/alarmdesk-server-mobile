package com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.claim

import com.seleznyoviyu.alarmdesk.server.mobile.web.json.claim.ClaimCreateByMobileUserRequestJson
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.request.ClaimCreateByMobileUserRequest
import org.mapstruct.InjectionStrategy
import org.mapstruct.Mapper

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
interface ClaimCreateByMobileUserRequestDomainMapper {
    fun map(source: ClaimCreateByMobileUserRequestJson): ClaimCreateByMobileUserRequest
}