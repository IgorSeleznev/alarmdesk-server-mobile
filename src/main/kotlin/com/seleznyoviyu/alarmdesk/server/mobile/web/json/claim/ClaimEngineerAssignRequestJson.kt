package com.seleznyoviyu.alarmdesk.server.mobile.web.json.claim

import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming

@JsonNaming(PropertyNamingStrategies.KebabCaseStrategy::class)
data class ClaimEngineerAssignRequestJson(
    val claimId: Long,
    val engineerId: Long
)