package com.seleznyoviyu.alarmdesk.server.mobile.web.configuration

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.security.UserAccountSecurityDetails
import io.jsonwebtoken.*
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.lang.String.format
import java.security.SignatureException
import java.util.*

@Component
class JwtTokenUtil {

    companion object {
        const val JWT_SECRET = "UK4amqdW5Z4vCh2yMac0ufjAL"
        const val JWT_ISSUER = "alarmdesk.seleznyoviyu.com"
        const val ACCESS_TOKEN_HEADER = "ALARMDESK-ACCESS-TOKEN"
        const val REFRESH_TOKEN_HEADER = "ALARMDESK-REFRESH-TOKEN"
    }

    private val logger =
        LoggerFactory.getLogger("com.seleznyoviyu.alarmdesk.server.mobile.common.security.jwt.JwtTokenUtil")

    fun generateAccessToken(user: UserAccountSecurityDetails): String? {
        return Jwts.builder()
            .setSubject(format("%s,%s", user.userAccount.externalId, user.userAccount.login))
            .setIssuer(JWT_ISSUER)
            .setIssuedAt(Date())
            //.setExpiration(Date(System.currentTimeMillis() + 93 * 24 * 60 * 60 * 1000)) // 93 days
            .setExpiration(Date(System.currentTimeMillis() + 30 * 60 * 1000)) // 30 minutes
            .signWith(SignatureAlgorithm.HS512, JWT_SECRET)
            .compact()
    }

    fun getUserId(token: String?): String? {
        val claims: Claims = Jwts.parser()
            .setSigningKey(JWT_SECRET)
            .parseClaimsJws(token)
            .body
        return claims.subject.split(",")[0]
    }

    fun getExternalId(token: String?): UUID? {
        val claims: Claims = Jwts.parser()
            .setSigningKey(JWT_SECRET)
            .parseClaimsJws(token)
            .body
        return UUID.fromString(
            claims.subject.split(",")[0]
        )
    }

    fun getUsername(token: String?): String? {
        val claims: Claims = Jwts.parser()
            .setSigningKey(JWT_SECRET)
            .parseClaimsJws(token)
            .body
        return claims.subject.split(",")[1]
    }

    fun getExpirationDate(token: String?): Date? {
        val claims: Claims = Jwts.parser()
            .setSigningKey(JWT_SECRET)
            .parseClaimsJws(token)
            .body
        return claims.expiration
    }

    fun validate(token: String?): Boolean {
        try {
            Jwts.parser().setSigningKey(JWT_SECRET).parseClaimsJws(token)
            return true
        } catch (exception: SignatureException) {
            logger.error("Invalid JWT signature - {}", exception.message)
        } catch (exception: MalformedJwtException) {
            logger.error("Invalid JWT token - {}", exception.message)
        } catch (exception: ExpiredJwtException) {
            logger.error("Expired JWT token - {}", exception.message)
        } catch (exception: UnsupportedJwtException) {
            logger.error("Unsupported JWT token - {}", exception.message)
        } catch (exception: IllegalArgumentException) {
            logger.error("JWT claims string is empty - {}", exception.message)
        }
        return false
    }
}