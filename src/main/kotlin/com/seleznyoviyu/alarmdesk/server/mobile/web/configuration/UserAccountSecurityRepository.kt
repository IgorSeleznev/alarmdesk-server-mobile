package com.seleznyoviyu.alarmdesk.server.mobile.web.configuration

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.security.UserAccountSecurityAuthority
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.security.UserAccountSecurityDetails
import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.user.UserFindByLoginAndPasswordService
import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.user.UserFindByLoginService
import org.springframework.stereotype.Component
import java.util.*

@Component
class UserAccountSecurityRepository(
    private val findByLoginByPasswordService: UserFindByLoginAndPasswordService,
    private val findByLoginService: UserFindByLoginService
) {
    fun findByLoginAndPassword(login: String, password: String): Optional<UserAccountSecurityDetails>? {
        val userAccount = findByLoginByPasswordService.findByLoginAndPassword(login, password)
        if (userAccount.isEmpty) {
            return Optional.empty()
        }
        return Optional.of(
            UserAccountSecurityDetails(
                userAccount = userAccount.get(),
                authorities = mutableListOf(
                    UserAccountSecurityAuthority(
                        authority = "ROLE_${userAccount.get().role}"
                    )
                )
            )
        )
    }

    fun findByLogin(login: String?): Optional<UserAccountSecurityDetails> {
        if (login == null) {
            return Optional.empty()
        }
        val userAccount = findByLoginService.findByLogin(login)
        if (userAccount.isEmpty) {
            return Optional.empty()
        }
        return Optional.of(
            UserAccountSecurityDetails(
                userAccount = userAccount.get(),
                authorities = mutableListOf(
                    UserAccountSecurityAuthority(
                        authority = "ROLE_${userAccount.get().role}"
                    )
                )
            )
        )
    }
}