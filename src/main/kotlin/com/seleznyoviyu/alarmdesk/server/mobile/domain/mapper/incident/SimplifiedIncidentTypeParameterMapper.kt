package com.seleznyoviyu.alarmdesk.server.mobile.domain.mapper.incident

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.IncidentTypeParameter
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.SimplifiedIncidentTypeParameter
import org.mapstruct.InjectionStrategy
import org.mapstruct.Mapper


@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
interface SimplifiedIncidentTypeParameterMapper {
    fun map(source: IncidentTypeParameter): SimplifiedIncidentTypeParameter
}