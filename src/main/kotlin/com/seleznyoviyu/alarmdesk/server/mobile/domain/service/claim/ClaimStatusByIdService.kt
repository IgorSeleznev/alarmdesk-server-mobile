package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.claim

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.claim.ClaimStatus
import com.seleznyoviyu.alarmdesk.server.mobile.da.client.claim.ClaimEntityStatusByIdClient
import org.springframework.stereotype.Service

@Service
class ClaimStatusByIdService(
    private val client: ClaimEntityStatusByIdClient
) {
    fun statusById(id: Long): ClaimStatus {
        return client.statusById(id)
    }
}