package com.seleznyoviyu.alarmdesk.server.mobile.domain.model.notification

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.IncidentPriorityType
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user.SimplifiedUserAccount
import java.time.Instant

data class ClaimAssignedNotification(
    val claimId: Long,
    val engineer: SimplifiedUserAccount,
    val author: SimplifiedUserAccount,
    val assignDateTime: Instant,
    val incidentTypeId: Long,
    val priority: IncidentPriorityType,
    val incidentTypesTitles: List<String>,
    val comment: String?
) : Notification