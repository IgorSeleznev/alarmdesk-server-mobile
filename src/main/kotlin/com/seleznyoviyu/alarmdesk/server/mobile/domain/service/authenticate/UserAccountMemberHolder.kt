package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.authenticate
/*
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user.UserAccount
import org.seleznyoviyu.utils.security.authentication.MemberHolder
*/
class UserAccountMemberHolder/*(
    private val userAccount: UserAccount
) : MemberHolder<UserAccount, String> {
    override fun getMember(): UserAccount {
        return userAccount
    }

    override fun memberIsEqualTo(userAccount: UserAccount): Boolean {
        return this.userAccount.id == userAccount.id
    }

    override fun isGranted(role: String): Boolean {
        return userAccount.role == role
    }

    override fun isGranted(privileges: MutableList<String>): Boolean {
        return privileges.contains(userAccount.role)
    }
}*/