package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.incident

import com.seleznyoviyu.alarmdesk.server.mobile.da.client.incident.EquipmentDescribeListClient
import com.seleznyoviyu.alarmdesk.server.mobile.domain.mapper.incident.SimplifiedIncidentTypeMapper
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.SimplifiedIncidentType
import org.springframework.stereotype.Service

@Service
class EquipmentDescribeListService(
    private val client: EquipmentDescribeListClient,
    private val mapper: SimplifiedIncidentTypeMapper
) {
    fun listAll(): List<SimplifiedIncidentType> {
        return client.listAll().map(mapper::map)
    }
}