package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.user

import com.seleznyoviyu.alarmdesk.server.mobile.da.client.user.UserAccountEntityFindByLoginAndPasswordClient
import com.seleznyoviyu.alarmdesk.server.mobile.domain.mapper.user.UserAccountMapper
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user.UserAccount
import org.springframework.stereotype.Service
import java.util.*

@Service
class UserFindByLoginAndPasswordService(
    private val findClient: UserAccountEntityFindByLoginAndPasswordClient,
    private val mapper: UserAccountMapper
) {
    fun findByLoginAndPassword(login: String, password: String): Optional<UserAccount> {
        val entity = findClient.findByLoginByPassword(login, password)
        if (entity.isEmpty) {
            return Optional.empty<UserAccount>()
        }
        return Optional.of(
            mapper.map(entity.get())
        )
    }
}