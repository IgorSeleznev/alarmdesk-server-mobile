package com.seleznyoviyu.alarmdesk.server.mobile.da.entity.refreshtoken

import java.time.Instant
import java.util.*

data class RefreshTokenEntity(
    val id: Long,
    val token: UUID,
    val createDateTime: Instant,
    val lifetime: Long,
    val userAccountId: Long
)
