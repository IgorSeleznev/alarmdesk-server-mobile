package com.seleznyoviyu.alarmdesk.server.mobile.web.controller.user

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.BUSINESS_ADMIN
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.DEVELOPER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.HEAD_ENGINEER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.SYSTEM_ADMIN
import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.user.UserAccountValidateAndCreateService
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.common.IdResponseJson
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.user.UserAccountSaveRequestJson
import com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.user.UserAccountSaveRequestDomainMapper
import com.seleznyoviyu.alarmdesk.server.mobile.web.resolver.HttpSessionAuthenticatedUserResolver
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.annotation.security.RolesAllowed

@RestController
@RequestMapping("/user-account")
class UserAccountSaveController(
    private val service: UserAccountValidateAndCreateService,
    private val mapper: UserAccountSaveRequestDomainMapper,
    private val userResolver: HttpSessionAuthenticatedUserResolver
) {
    @RolesAllowed(
        "ROLE_$DEVELOPER",
        "ROLE_$SYSTEM_ADMIN",
        "ROLE_$BUSINESS_ADMIN",
        "ROLE_$HEAD_ENGINEER"
    )
    @PostMapping("/save")
    fun createUser(@RequestBody request: UserAccountSaveRequestJson): IdResponseJson {
        return IdResponseJson(
            id = service.saveUserAccount(
                mapper.map(request),
                userResolver.resolve()
            )
        )
    }
}