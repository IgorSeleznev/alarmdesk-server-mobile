package com.seleznyoviyu.alarmdesk.server.mobile.domain.model.location

data class Location(
    val id: Long?,
    val ukmLocation: Long,
    val regionId: Long,
    val title: String,
    val address: String
)