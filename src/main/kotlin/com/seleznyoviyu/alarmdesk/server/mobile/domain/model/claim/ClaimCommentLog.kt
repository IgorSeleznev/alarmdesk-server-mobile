package com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.claim.ClaimStatus
import java.time.Instant

data class ClaimCommentLog(
    val datetime: Instant,
    val author: UserAccountBrief,
    val status: ClaimStatus,
    val comment: String?
)
