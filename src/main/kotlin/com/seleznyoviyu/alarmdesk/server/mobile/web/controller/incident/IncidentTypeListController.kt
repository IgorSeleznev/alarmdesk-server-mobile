package com.seleznyoviyu.alarmdesk.server.mobile.web.controller.incident

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.BUSINESS_ADMIN
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.DEVELOPER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.ENGINEER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.HEAD_ENGINEER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.MOBILE_USER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.OPERATOR
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.SYSTEM_ADMIN
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.WEB_USER
import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.incident.IncidentTypeListService
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.common.IdRequestJson
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.incident.IncidentTypeJson
import com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.incident.IncidentTypeJsonMapper
import org.springframework.web.bind.annotation.*
import javax.annotation.security.RolesAllowed

@RestController
@RequestMapping("/incident-type")
class IncidentTypeListController(
    private val service: IncidentTypeListService,
    private val mapper: IncidentTypeJsonMapper
) {
    @RolesAllowed(
        "ROLE_$MOBILE_USER",
        "ROLE_$DEVELOPER",
        "ROLE_$OPERATOR",
        "ROLE_$ENGINEER",
        "ROLE_$SYSTEM_ADMIN",
        "ROLE_$BUSINESS_ADMIN",
        "ROLE_$WEB_USER",
        "ROLE_$HEAD_ENGINEER"
    )
    @PostMapping("/list-by-parent")
    fun incidentTypeListByParent(@RequestBody request: IdRequestJson): List<IncidentTypeJson> {
        return service.listByParent(request.id).map(mapper::map)
    }

    @RolesAllowed(
        "ROLE_$MOBILE_USER",
        "ROLE_$DEVELOPER",
        "ROLE_$OPERATOR",
        "ROLE_$ENGINEER",
        "ROLE_$SYSTEM_ADMIN",
        "ROLE_$BUSINESS_ADMIN",
        "ROLE_$WEB_USER",
        "ROLE_$HEAD_ENGINEER"
    )
    @GetMapping("/list-root")
    fun rootIncidentTypeList(): List<IncidentTypeJson> {
        return service.listRoot().map(mapper::map)
    }
}