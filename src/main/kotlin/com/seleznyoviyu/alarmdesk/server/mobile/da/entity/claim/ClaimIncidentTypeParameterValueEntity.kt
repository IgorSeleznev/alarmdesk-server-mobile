package com.seleznyoviyu.alarmdesk.server.mobile.da.entity.claim

data class ClaimIncidentTypeParameterValueEntity(
    val value: String?,
    val options: Map<Long, String>
)