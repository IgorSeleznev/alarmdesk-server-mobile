package com.seleznyoviyu.alarmdesk.server.mobile.web.json.location

import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming

@JsonNaming(PropertyNamingStrategies.KebabCaseStrategy::class)
data class LocationJson(
    val id: Long?,
    val ukmLocation: Long,
    val title: String,
    val regionId: Long,
    val address: String
)
