package com.seleznyoviyu.alarmdesk.server.mobile.web.controller.claim

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.DEVELOPER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.ENGINEER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.HEAD_ENGINEER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.OPERATOR
import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.claim.ClaimCommentUpdateService
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.claim.request.ClaimCommentUpdateRequestJson
import com.seleznyoviyu.alarmdesk.server.mobile.web.resolver.HttpSessionAuthenticatedUserResolver
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.annotation.security.RolesAllowed

@RestController
@RequestMapping("/claim")
class ClaimCommentUpdateController(
    private val service: ClaimCommentUpdateService,
    private val userResolver: HttpSessionAuthenticatedUserResolver
) {
    @RolesAllowed(
        "ROLE_$ENGINEER",
        "ROLE_$OPERATOR",
        "ROLE_$HEAD_ENGINEER",
        "ROLE_$DEVELOPER"
    )
    @PutMapping("/comment-update")
    fun update(@RequestBody request: ClaimCommentUpdateRequestJson) {
        service.updateComment(
            request.claimId,
            request.comment,
            userResolver.resolve().id!!
        )
    }
}