package com.seleznyoviyu.alarmdesk.server.mobile.da.client.user

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Component

@Component
class UserAccountIdListByRoleClient(
    private val jdbcTemplate: NamedParameterJdbcTemplate
) {
    fun listByRoles(roles: List<String>): List<Long> {
        val rolesSql = roles.joinToString(
                prefix = "('",
                separator = "', ''",
                postfix = "')"
                //limit = 3,
                //truncated = "...",
                //transform = { it.toUpperCase() }
        )
        return jdbcTemplate.query(
            """
SELECT id
FROM user_account
WHERE role IN $rolesSql           
            """.trimIndent()
        ) { resultSet, _ ->
            resultSet.getLong(1)
        }
    }
}