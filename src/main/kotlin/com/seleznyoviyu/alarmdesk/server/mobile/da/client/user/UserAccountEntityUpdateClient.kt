package com.seleznyoviyu.alarmdesk.server.mobile.da.client.user

import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.user.UserAccountEntity
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Component

@Component
class UserAccountEntityUpdateClient(
    private val jdbcTemplate: NamedParameterJdbcTemplate
) {
    fun update(userAccount: UserAccountEntity): Long {
        jdbcTemplate.update(
            """
UPDATE user_account
SET 
    name = :name,
    login = :login,
    password_hash = :passwordHash,
    role = :role,
    region_id = :regionId,
    external_id = :externalId
WHERE id = :id
            """.trimIndent(),
            MapSqlParameterSource()
                .addValue("id", userAccount.id)
                .addValue("name", userAccount.name)
                .addValue("login", userAccount.login)
                .addValue("passwordHash", userAccount.passwordHash)
                .addValue("role", userAccount.role)
                .addValue("regionId", userAccount.regionId)
                .addValue("externalId", userAccount.externalId)
        )
        return userAccount.id!!
    }
}