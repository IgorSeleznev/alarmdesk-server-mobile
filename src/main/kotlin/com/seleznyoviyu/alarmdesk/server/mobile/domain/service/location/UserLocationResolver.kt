package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.location

import com.seleznyoviyu.alarmdesk.server.mobile.da.client.location.LocationEntityFindByIdClient
import com.seleznyoviyu.alarmdesk.server.mobile.domain.mapper.location.LocationMapper
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.location.Location
import org.springframework.stereotype.Component

@Component
class UserLocationResolver(
    val mapper: LocationMapper,
    val findClient: LocationEntityFindByIdClient
) {
    fun resolve(userId: Long): Location {
        return mapper.map(
            findClient.findById(userId)
        )
    }
}