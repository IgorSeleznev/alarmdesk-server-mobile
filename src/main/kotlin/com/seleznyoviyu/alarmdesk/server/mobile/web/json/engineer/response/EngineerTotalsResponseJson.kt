package com.seleznyoviyu.alarmdesk.server.mobile.web.json.engineer.response

data class EngineerTotalsResponseJson(
    val justCreated: Int,
    val inProgress: Int,
    val finished: Int
)
