package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.claim

import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.notification.ClaimEngineerAssignNotifyService
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.request.ClaimSaveService
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.Instant

@Service
class ClaimEngineerAssignService(
    private val findService: ClaimFindByIdService,
    private val saveService: ClaimSaveService,
    private val notifyService: ClaimEngineerAssignNotifyService
) {
    @Transactional
    fun engineerAssign(claimId: Long, engineerId: Long) {
        val claim = findService.findById(claimId).copy(
            engineerId = engineerId,
            engineerAssignDateTime = Instant.now()
        )
        saveService.save(claim)
        notifyService.notify(claim)
    }
}