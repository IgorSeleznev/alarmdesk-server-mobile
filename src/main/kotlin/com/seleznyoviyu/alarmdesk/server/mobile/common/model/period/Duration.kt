package com.seleznyoviyu.alarmdesk.server.mobile.common.model.period

data class Duration(
    val duration: Int,
    val periodType: DurationPeriodType
)
