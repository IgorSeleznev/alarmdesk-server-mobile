package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.claim.detailed

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.DetailedClaim
import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.claim.detailed.constructor.DetailedClaimBuilder
import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.claim.detailed.constructor.DetailedClaimConstructor
import org.springframework.stereotype.Service

@Service
class DetailedClaimService(
    private val constructor: DetailedClaimConstructor
) {
    fun detailedClaim(claimId: Long): DetailedClaim {
        return constructor.construct(
            DetailedClaimBuilder(claimId)
        ).build()
    }
}