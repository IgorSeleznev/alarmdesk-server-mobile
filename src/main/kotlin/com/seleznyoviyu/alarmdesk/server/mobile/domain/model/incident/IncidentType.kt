package com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident

data class IncidentType(
    val id: Long?,
    val parentId: Long?,
    val title: String,
    val defaultPriority: IncidentPriorityType,
    val parameters: List<IncidentTypeParameter>,
    val isEquipmentDescribe: Boolean,
    val incidentResolveSla: Int,
    val incidentReactionSla: Int,
    val isLast: Boolean
)
