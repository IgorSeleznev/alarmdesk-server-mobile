package com.seleznyoviyu.alarmdesk.server.mobile.da.client.engineer

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.claim.ClaimStatus
import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.engineer.EngineerTotalsEntity
import com.seleznyoviyu.alarmdesk.server.mobile.da.exception.NotFoundException
import com.seleznyoviyu.alarmdesk.server.mobile.da.rowmapper.engineer.EngineerTotalsEntityRowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Component

@Component
class SubordinateEngineersTotalsClient(
    private val jdbcTemplate: NamedParameterJdbcTemplate,
    private val rowMapper: EngineerTotalsEntityRowMapper
) {
    //private val claimTableName: String = (ClaimEntity::class.findAnnotation<Table>()!!).value

    fun subordinatesTotals(headEngineerId: Long): EngineerTotalsEntity {
        val result = jdbcTemplate.query(
            """
SELECT sum(CASE WHEN status = '${ClaimStatus.CREATED}' THEN 1 ELSE 0 END) as justCreatedCount, 
       sum(CASE WHEN status in ('${ClaimStatus.IN_PROGRESS}', '${ClaimStatus.ON_HOLD}') THEN 1 ELSE 0 END) as inProgressCount,
       sum(CASE WHEN status = '${ClaimStatus.FINISHED}' THEN 1 ELSE 0 END) as finishedCount
FROM claim c
INNER public.subordinate_engineer se ON c.engineer_id = se.engineer_user_account_id 
WHERE se.head_engineer_user_account_id = :headEngineerId 
            """.trimIndent(),
            MapSqlParameterSource().addValue("headEngineerId", headEngineerId),
            rowMapper
        )

        if (result.size == 0) {
            throw NotFoundException("Claims not found for engineer $headEngineerId")
        }
        return result[0]
    }
}