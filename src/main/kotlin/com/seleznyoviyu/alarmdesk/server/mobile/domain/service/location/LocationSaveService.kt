package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.location

import com.seleznyoviyu.alarmdesk.server.mobile.da.client.common.NextIdClient
import com.seleznyoviyu.alarmdesk.server.mobile.da.client.location.LocationInsertClient
import com.seleznyoviyu.alarmdesk.server.mobile.da.client.location.LocationUpdateClient
import com.seleznyoviyu.alarmdesk.server.mobile.domain.mapper.location.LocationEntityMapper
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.location.Location
import org.springframework.stereotype.Service

@Service
class LocationSaveService(
    private val insertClient: LocationInsertClient,
    private val updateClient: LocationUpdateClient,
    private val nextIdClient: NextIdClient,
    private val mapper: LocationEntityMapper
) {
    fun save(location: Location): Long {
        return if (location.id == null) {
            insertClient.insert(
                mapper.map(
                    location
                ).copy(
                    id = nextIdClient.nextId("location_seq")
                )
            )
        } else {
            updateClient.update(
                mapper.map(location)
            )
        }
    }
}