package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.user

import com.seleznyoviyu.alarmdesk.server.mobile.da.client.user.UserAccountEntityFindByLoginClient
import com.seleznyoviyu.alarmdesk.server.mobile.domain.mapper.user.UserAccountMapper
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user.UserAccount
import org.springframework.stereotype.Service
import java.util.*

@Service
class UserAccountEntityFindByLoginService(
    private val client: UserAccountEntityFindByLoginClient,
    private val mapper: UserAccountMapper
) {
    fun findByLogin(login: String): Optional<UserAccount> {
        val userAccount = client.findByLogin(login)
        if (userAccount.isEmpty) {
            return Optional.empty()
        }
        return Optional.of(
            mapper.map(
                userAccount.get()
            )
        )
    }
}