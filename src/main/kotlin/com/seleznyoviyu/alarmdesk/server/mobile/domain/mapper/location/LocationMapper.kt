package com.seleznyoviyu.alarmdesk.server.mobile.domain.mapper.location

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.location.Location
import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.location.LocationEntity
import org.mapstruct.InjectionStrategy
import org.mapstruct.Mapper


@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
interface LocationMapper {
    fun map(source: LocationEntity): Location
}