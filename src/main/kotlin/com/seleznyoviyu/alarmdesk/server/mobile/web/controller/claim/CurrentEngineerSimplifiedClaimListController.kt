package com.seleznyoviyu.alarmdesk.server.mobile.web.controller.claim

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.DEVELOPER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.ENGINEER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.HEAD_ENGINEER
import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.claim.SimplifiedClaimListService
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.claim.SimplifiedClaimJson
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.claim.request.ClaimStatusRequestJson
import com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.claim.SimplifiedClaimJsonMapper
import com.seleznyoviyu.alarmdesk.server.mobile.web.resolver.HttpSessionAuthenticatedUserResolver
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.annotation.security.RolesAllowed

@RestController
@RequestMapping("/claim")
class CurrentEngineerSimplifiedClaimListController(
    private val service: SimplifiedClaimListService,
    private val mapper: SimplifiedClaimJsonMapper,
    private val userResolver: HttpSessionAuthenticatedUserResolver
) {
    @RolesAllowed(
        "ROLE_$ENGINEER",
        "ROLE_$HEAD_ENGINEER",
        "ROLE_$DEVELOPER"
    )
    @GetMapping("/list/by-status-by-current-engineer")
    fun list(@RequestBody request: ClaimStatusRequestJson): List<SimplifiedClaimJson> {
        return service.list(
            request.status, userResolver.resolve().id!!
        ).map(mapper::map)
    }
}