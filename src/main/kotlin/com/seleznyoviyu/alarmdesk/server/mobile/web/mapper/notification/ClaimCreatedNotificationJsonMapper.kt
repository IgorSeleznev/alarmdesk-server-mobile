package com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.notification

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.notification.ClaimCreatedNotification
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.notification.ClaimCreatedNotificationJson
import org.mapstruct.InjectionStrategy
import org.mapstruct.Mapper

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
interface ClaimCreatedNotificationJsonMapper {
    fun map(source: ClaimCreatedNotification): ClaimCreatedNotificationJson
}