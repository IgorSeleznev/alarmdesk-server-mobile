package com.seleznyoviyu.alarmdesk.server.mobile.da.client.refreshtoken

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Component
import java.util.*

@Component
class RefreshTokenEntityDeleteClient(
    private val jdbcTemplate: NamedParameterJdbcTemplate
) {
    fun delete(refreshToken: UUID) {
        jdbcTemplate.update(
            """
DELETE FROM refresh_token
WHERE token = :token                
            """.trimIndent(),
            MapSqlParameterSource().addValue("token", refreshToken)
        )
    }
}