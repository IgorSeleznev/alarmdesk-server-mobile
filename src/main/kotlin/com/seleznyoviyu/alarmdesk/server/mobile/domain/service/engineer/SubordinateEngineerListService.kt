package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.engineer

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.claim.ClaimStatus
import com.seleznyoviyu.alarmdesk.server.mobile.da.client.engineer.UserAccountSubordinateListByClaimStatusClient
import com.seleznyoviyu.alarmdesk.server.mobile.da.client.engineer.UserAccountSubordinateListClient
import com.seleznyoviyu.alarmdesk.server.mobile.domain.mapper.engineer.UserAccountEntitySimplifyUserAccountMapper
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user.SimplifiedUserAccount
import org.springframework.stereotype.Service

@Service
class SubordinateEngineerListService(
    private val findClient: UserAccountSubordinateListClient,
    private val findClientByStatus: UserAccountSubordinateListByClaimStatusClient,
    private val mapper: UserAccountEntitySimplifyUserAccountMapper
) {
    fun list(status: ClaimStatus, headEngineerId: Long): List<SimplifiedUserAccount> {
        return findClientByStatus.subordinateListByClamStatus(status, headEngineerId).map(mapper::map)
    }

    fun list(headEngineerId: Long): List<SimplifiedUserAccount> {
        return findClient.subordinateList(headEngineerId).map(mapper::map)
    }
}