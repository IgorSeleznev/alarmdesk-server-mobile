package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.util

import org.springframework.stereotype.Service
import java.security.MessageDigest

@Service
class Sha256UtilService {

    fun sha256(source: String): String {
        return MessageDigest.getInstance("SHA3-256").digest(
            source.toByteArray(Charsets.UTF_8)
        ).joinToString("") { "%02x".format(it) }
    }
}