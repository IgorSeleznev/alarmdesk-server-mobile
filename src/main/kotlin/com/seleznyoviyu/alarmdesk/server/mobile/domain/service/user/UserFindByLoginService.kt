package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.user

import com.seleznyoviyu.alarmdesk.server.mobile.da.client.user.UserAccountEntityFindByLoginClient
import com.seleznyoviyu.alarmdesk.server.mobile.domain.mapper.user.UserAccountMapper
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user.UserAccount
import org.springframework.stereotype.Service
import java.util.*

@Service
class UserFindByLoginService(
    private val findClient: UserAccountEntityFindByLoginClient,
    private val mapper: UserAccountMapper
) {
    fun findByLogin(login: String): Optional<UserAccount> {
        val entity = findClient.findByLogin(login)
        if (entity.isEmpty) {
            return Optional.empty<UserAccount>()
        }
        return Optional.of(
            mapper.map(entity.get())
        )
    }
}