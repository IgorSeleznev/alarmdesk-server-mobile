package com.seleznyoviyu.alarmdesk.server.mobile.common

import com.fasterxml.jackson.core.util.DefaultPrettyPrinter
import com.fasterxml.jackson.databind.MapperFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule

fun getObjectMapper(): ObjectMapper {
    return JsonMapper.builder()
        .defaultPrettyPrinter(DefaultPrettyPrinter())
        .addModule(KotlinModule())
        .enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS)
        .enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
        .propertyNamingStrategy(PropertyNamingStrategies.KEBAB_CASE)
        .build()
    /*return ObjectMapper()
        .setDefaultPrettyPrinter(DefaultPrettyPrinter())
        .enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS)
        .enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
        .setPropertyNamingStrategy(PropertyNamingStrategies.KEBAB_CASE)
        .registerKotlinModule()*/

}