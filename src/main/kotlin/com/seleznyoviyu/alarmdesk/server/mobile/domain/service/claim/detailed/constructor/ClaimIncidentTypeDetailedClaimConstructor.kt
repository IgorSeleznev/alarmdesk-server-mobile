package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.claim.detailed.constructor

import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.claim.detailed.constructor.ClaimLocationDetailedClaimConstructor.Companion.LOCATION_CLAIM_DETAILED_CONSTRUCTOR_BEAN_NAME
import com.seleznyoviyu.alarmdesk.server.mobile.da.client.incident.IncidentTypeEntityFindByIdClient
import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.claim.detailed.constructor.ClaimIncidentTypeDetailedClaimConstructor.Companion.INCIDENT_TYPE_CLAIM_DETAILED_CONSTRUCTOR_BEAN_NAME
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Component

@Qualifier(INCIDENT_TYPE_CLAIM_DETAILED_CONSTRUCTOR_BEAN_NAME)
@Component
class ClaimIncidentTypeDetailedClaimConstructor(
    private val clint: IncidentTypeEntityFindByIdClient,
    @Qualifier(LOCATION_CLAIM_DETAILED_CONSTRUCTOR_BEAN_NAME)
    private val constructor: DetailedClaimConstructor
) : DetailedClaimConstructor {

    companion object {
        const val INCIDENT_TYPE_CLAIM_DETAILED_CONSTRUCTOR_BEAN_NAME = "ClaimIncidentTypeDetailedClaimConstructor"
    }

    override fun construct(builder: DetailedClaimBuilder): DetailedClaimBuilder {
        builder.incident = clint.findById(builder.claimEntity.incidentTypeId)
        return constructor.construct(builder)
    }
}