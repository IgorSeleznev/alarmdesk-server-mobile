package com.seleznyoviyu.alarmdesk.server.mobile.da.rowmapper.engineer

import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.engineer.EngineerTotalsEntity
import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Component
import java.sql.ResultSet

@Component
class EngineerTotalsEntityRowMapper : RowMapper<EngineerTotalsEntity> {
    override fun mapRow(resultSet: ResultSet, rowNum: Int): EngineerTotalsEntity? {
        return EngineerTotalsEntity(
            justCreated = resultSet.getInt(1),
            inProgress = resultSet.getInt(2),
            finished = resultSet.getInt(3)
        )
    }
}