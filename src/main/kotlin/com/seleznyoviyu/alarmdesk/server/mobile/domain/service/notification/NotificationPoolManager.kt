package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.notification

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.notification.Notification
import org.springframework.stereotype.Component
import java.util.*
import java.util.concurrent.ConcurrentHashMap

@Component
class NotificationPoolManager {

    private val pool: MutableMap<Long, Queue<Notification>> = ConcurrentHashMap()

    fun put(userAccountId: Long, notification: Notification) {
        if (!pool.containsKey(userAccountId)) {
            init(userAccountId)
        }
        pool[userAccountId]!!.add(notification)
    }

    fun clear(userAccountId: Long) {
        pool.remove(userAccountId)
    }

    fun nextNotification(userAccountId: Long): Notification {
        if (!pool.containsKey(userAccountId)) {
            init(userAccountId)
        }
        return pool[userAccountId]!!.poll()
    }

    private fun init(userAccountId: Long) {
        pool[userAccountId] = LinkedList()
    }
}