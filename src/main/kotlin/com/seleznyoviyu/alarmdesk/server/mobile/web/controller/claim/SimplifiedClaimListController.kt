package com.seleznyoviyu.alarmdesk.server.mobile.web.controller.claim

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.DEVELOPER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.ENGINEER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.HEAD_ENGINEER
import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.claim.SimplifiedClaimListService
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.claim.SimplifiedClaimJson
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.claim.request.SimplifiedClaimListRequestJson
import com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.claim.SimplifiedClaimJsonMapper
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.annotation.security.RolesAllowed

@RestController
@RequestMapping("/claim")
class SimplifiedClaimListController(
    private val service: SimplifiedClaimListService,
    private val mapper: SimplifiedClaimJsonMapper
) {

    @RolesAllowed(
        "ROLE_$DEVELOPER",
        "ROLE_$ENGINEER",
        "ROLE_$HEAD_ENGINEER"
    )
    @PostMapping("/list/by-status-by-engineer")
    fun list(@RequestBody request: SimplifiedClaimListRequestJson): List<SimplifiedClaimJson> {
        return service.list(request.status, request.engineerId).map(mapper::map)
    }
}