package com.seleznyoviyu.alarmdesk.server.mobile.web.controller.engineer

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.DEVELOPER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.ENGINEER
import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.engineer.EngineerTotalsService
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.common.IdRequestJson
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.engineer.response.EngineerTotalsResponseJson
import com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.engineer.EngineerTotalsResponseJsonMapper
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.annotation.security.RolesAllowed

@RestController
@RequestMapping("/engineer")
class EngineerTotalsController(
    private val service: EngineerTotalsService,
    private val mapper: EngineerTotalsResponseJsonMapper,
) {
    @RolesAllowed(
        "ROLE_$ENGINEER",
        "ROLE_$DEVELOPER"
    )
    @PostMapping("/totals")
    fun engineerTotals(@RequestBody request: IdRequestJson): EngineerTotalsResponseJson {
        return mapper.map(
            service.engineerTotals(request.id)
        )
    }
}