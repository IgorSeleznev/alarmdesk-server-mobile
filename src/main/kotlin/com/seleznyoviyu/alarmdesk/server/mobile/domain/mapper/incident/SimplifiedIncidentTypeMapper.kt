package com.seleznyoviyu.alarmdesk.server.mobile.domain.mapper.incident

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.SimplifiedIncidentType
import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.incident.SimplifiedIncidentTypeEntity
import org.mapstruct.InjectionStrategy
import org.mapstruct.Mapper


@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
interface SimplifiedIncidentTypeMapper {
    fun map(source: SimplifiedIncidentTypeEntity): SimplifiedIncidentType
}