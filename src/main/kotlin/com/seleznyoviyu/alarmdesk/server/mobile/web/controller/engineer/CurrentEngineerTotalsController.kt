package com.seleznyoviyu.alarmdesk.server.mobile.web.controller.engineer

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.DEVELOPER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.ENGINEER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.HEAD_ENGINEER
import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.engineer.EngineerTotalsService
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.engineer.response.EngineerTotalsResponseJson
import com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.engineer.EngineerTotalsResponseJsonMapper
import com.seleznyoviyu.alarmdesk.server.mobile.web.resolver.HttpSessionAuthenticatedUserResolver
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.annotation.security.RolesAllowed

@RestController
@RequestMapping("/engineer")
class CurrentEngineerTotalsController(
    private val service: EngineerTotalsService,
    private val mapper: EngineerTotalsResponseJsonMapper,
    private val userResolver: HttpSessionAuthenticatedUserResolver
) {

    @RolesAllowed(
        "ROLE_$DEVELOPER",
        "ROLE_$ENGINEER",
        "ROLE_$HEAD_ENGINEER"
    )
    @GetMapping("/current/totals")
    fun statistics(): EngineerTotalsResponseJson {
        return mapper.map(
            service.engineerTotals(userResolver.resolve().id!!)
        )
    }
}