package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.claim.detailed.constructor

interface DetailedClaimConstructor {
    fun construct(builder: DetailedClaimBuilder): DetailedClaimBuilder
}