package com.seleznyoviyu.alarmdesk.server.mobile.domain.model.security

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user.UserAccount

data class JwtTokenBuildByRefreshTokenResult(
    val jwtToken: String,
    val userAccount: UserAccount
)
