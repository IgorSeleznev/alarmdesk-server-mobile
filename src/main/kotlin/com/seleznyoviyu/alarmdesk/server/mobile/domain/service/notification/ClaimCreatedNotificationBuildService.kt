package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.notification

import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.user.UserAccountFindByIdService
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.Claim
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.notification.ClaimCreatedNotification
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user.SimplifiedUserAccount
import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.incident.IncidentTypeTitleListByLeafService
import org.springframework.stereotype.Service

@Service
class ClaimCreatedNotificationBuildService(
    private val userFindByIdService: UserAccountFindByIdService,
    private val incidentTypeTitleService: IncidentTypeTitleListByLeafService
) {
    fun buildNotification(claim: Claim): ClaimCreatedNotification {
        val author = userFindByIdService.findById(claim.authorId)
        val titles = incidentTypeTitleService.listTitleByLeaf(claim.incidentTypeId)
        return ClaimCreatedNotification(
            claimId = claim.id!!,
            author = SimplifiedUserAccount(
                id = author.id!!,
                name = author.name
            ),
            createdDateTime = claim.createDateTime,
            incidentTitles = titles,
            incidentTypeParameterValues = claim.incidentTypeParameterValues,
            incidentTypeId = claim.incidentTypeId,
            comment = claim.comment
        )
    }
}