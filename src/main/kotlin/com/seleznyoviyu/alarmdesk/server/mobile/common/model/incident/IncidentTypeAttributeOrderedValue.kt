package com.seleznyoviyu.alarmdesk.server.mobile.common.model.incident

data class IncidentTypeAttributeOrderedValue(
    val orderNumber: Int,
    val value: String
)
