package com.seleznyoviyu.alarmdesk.server.mobile.da.entity.location

data class LocationEntity(
    val id: Long?,
    val title: String,
    val regionId: Long,
    val address: String,
    val ukmLocation: Long
)
