package com.seleznyoviyu.alarmdesk.server.mobile.web.controller.advice

import com.seleznyoviyu.alarmdesk.server.mobile.common.exception.ForbiddenException
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.mobile.authenticate.AuthenticateResponseJson
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@ControllerAdvice
class ControllerAdvice {

    @ExceptionHandler(ForbiddenException::class)
    fun forbiddenExceptionHandle(): ResponseEntity<AuthenticateResponseJson?>? {
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build<AuthenticateResponseJson>()
    }
}