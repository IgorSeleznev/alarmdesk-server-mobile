package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.notification

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole
import com.seleznyoviyu.alarmdesk.server.mobile.da.client.user.UserAccountIdListByRoleClient
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.Claim
import org.springframework.stereotype.Component

@Component
class ClaimCreateByMobileUserNotifyManager(
    private val notifyService: ClaimCreateByMobileUserNotifyService,
    private val listByRoleService: UserAccountIdListByRoleClient,
    private val notificationBuildService: ClaimCreatedNotificationBuildService,
) {
    fun notify(claim: Claim) {
        notifyService.notify(
            usersIdList = listByRoleService.listByRoles(
                listOf(UserRole.OPERATOR)
            ),
            notification = notificationBuildService.buildNotification(claim)
        )
    }
}