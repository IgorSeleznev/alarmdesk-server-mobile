package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.claim

import com.seleznyoviyu.alarmdesk.server.mobile.da.client.claim.ClaimEntityUpdateClient
import com.seleznyoviyu.alarmdesk.server.mobile.domain.mapper.claim.ClaimEntityMapper
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.ClaimCommentLog
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.ClaimStatusLog
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.request.ClaimStatusUpdateRequest
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.Instant

@Service
class ClaimStatusUpdateService(
    private val updateClient: ClaimEntityUpdateClient,
    private val mapper: ClaimEntityMapper
) {
    @Transactional
    fun updateStatus(request: ClaimStatusUpdateRequest) {
        val now = Instant.now()
        val entity = mapper.map(request.sourceClaim)
        updateClient.update(
            mapper.map(
                request.sourceClaim.copy(
                    status = request.requestBrief.newStatus,
                    authorId = request.requestBrief.authorId,
                    statusUpdateDateTime = now,
                    comment = request.requestBrief.comment,
                    statusLog = entity.statusLog.toMutableList().apply {
                        add(
                            ClaimStatusLog(
                                datetime = now,
                                author = request.author,
                                status = request.requestBrief.newStatus
                            )
                        )
                    },
                    commentLog = request.sourceClaim.commentLog.toMutableList().apply {
                        add(
                            ClaimCommentLog(
                                datetime = now,
                                author = request.author,
                                status = request.requestBrief.newStatus,
                                comment = request.requestBrief.comment
                            )
                        )
                    }
                )
            )
        )
    }
}