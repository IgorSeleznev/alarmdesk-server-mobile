package com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.ClaimIncidentTypeParameterValue
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.IncidentPriorityType
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.claim.ClaimStatus
import java.time.Instant

data class Claim(
    val id: Long?,
    val locationId: Long,
    val status: ClaimStatus,
    val authorId: Long,
    val engineerId: Long?,
    val comment: String?,
    val incidentTypeId: Long,
    val incidentTypeParameterValues: Map<Long, ClaimIncidentTypeParameterValue>,
    val incidentPriority: IncidentPriorityType,
    val statusUpdateAuthorId: Long,
    val createDateTime: Instant,
    val statusUpdateDateTime: Instant,
    val engineerAssignDateTime: Instant?,
    val finishDateTime: Instant?,
    val technicalDetails: Map<String, Map<String, String>>?,
    val statusLog: List<ClaimStatusLog>,
    val commentLog: List<ClaimCommentLog>
/*    val uuid: UUID,
    val incidentTypeUUID: UUID,
    val incidentPriorityUUID: UUID,
    val incidentTypeParameterValues: Map<UUID, ClaimIncidentTypeParameterValue>,
    val locationUUID: UUID,
    val status: ClaimStatus,
    val engineerUUID: UUID?,
    val authorUUID: UUID,
    val statusUpdateAuthorUUID: UUID,
    val createDateTime: Instant,
    val statusUpdateDateTime: Instant,
    val finishDateTime: Instant?,
    val comments: List<ClaimComment>,
    val statusLog: List<ClaimStatusLog>,
    val technicalDetails: Map<String, Map<String, String>>?*/
)
