package com.seleznyoviyu.alarmdesk.server.mobile.web.json.mobile.authenticate

data class AuthenticateRequestJson(
    val login: String,
    val password: String
)