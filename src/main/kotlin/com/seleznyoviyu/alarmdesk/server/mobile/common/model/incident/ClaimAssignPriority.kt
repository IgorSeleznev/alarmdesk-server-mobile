package com.seleznyoviyu.alarmdesk.server.mobile.common.model.incident

enum class ClaimAssignPriority {
    IN_TAIL,
    FORWARD,
    FORCE_TO_AHEAD
}