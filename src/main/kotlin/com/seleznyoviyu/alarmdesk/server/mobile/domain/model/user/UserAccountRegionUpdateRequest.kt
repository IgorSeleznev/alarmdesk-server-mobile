package com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user

data class UserAccountRegionUpdateRequest(
    val userAccountId: Long,
    val regionId: Long
)
