package com.seleznyoviyu.alarmdesk.server.mobile.da.entity.incident.parameter

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.incident.IncidentValueType

data class IncidentTypeParameterEntity(
    val id: Long,
    val title: String,
    val incidentValueType: IncidentValueType,
    val sortOrder: Int,
    val options: Map<Long, String>
)