package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.claim

import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.notification.ClaimCreateByMobileUserNotifyManager
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.request.ClaimCreateByMobileUserRequest
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.request.ClaimSaveService
import org.springframework.stereotype.Service

@Service
class ClaimCreateByMobileService(
    private val claimBuildService: ClaimBuildByMobileService,
    private val claimSaveService: ClaimSaveService,
    private val notifyService: ClaimCreateByMobileUserNotifyManager
) {
    fun createClaim(request: ClaimCreateByMobileUserRequest) {
        val claim = claimBuildService.build(request)
        claimSaveService.save(claim)
        notifyService.notify(claim)
    }
}