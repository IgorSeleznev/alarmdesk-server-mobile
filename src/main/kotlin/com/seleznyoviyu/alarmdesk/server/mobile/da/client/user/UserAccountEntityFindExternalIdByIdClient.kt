package com.seleznyoviyu.alarmdesk.server.mobile.da.client.user

import com.seleznyoviyu.alarmdesk.server.mobile.da.exception.NotFoundException
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Component
import java.sql.ResultSet
import java.util.*

@Component
class UserAccountEntityFindExternalIdByIdClient(
    private val jdbcTemplate: NamedParameterJdbcTemplate
) {
    fun findById(id: Long): UUID {
        val result: List<UUID> = jdbcTemplate.query(
            """
select external_id
from user_account
where id = :id
            """.trimIndent(),
            MapSqlParameterSource().addValue("id", id)
        ) { resultSet: ResultSet, _: Int ->
            resultSet.getObject(1, UUID::class.java)
        }

        if (result.isNotEmpty()) {
            return result[0]
        }
        throw NotFoundException("Не найден пользователь с id = $id")
    }
}