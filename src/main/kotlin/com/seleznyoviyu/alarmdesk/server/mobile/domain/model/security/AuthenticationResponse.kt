package com.seleznyoviyu.alarmdesk.server.mobile.domain.model.security

data class AuthenticationResponse(
    val accessToken: String,
    val refreshToken: String
)
