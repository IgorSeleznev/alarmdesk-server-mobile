package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.engineer

import com.seleznyoviyu.alarmdesk.server.mobile.da.client.engineer.SubordinateEngineersTotalsClient
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.engineer.response.EngineerTotalsResponse
import org.springframework.stereotype.Service

@Service
class SubordinateEngineersTotalsService(
    private val totalsDao: SubordinateEngineersTotalsClient
) {
    fun subordinatesTotals(headEngineerId: Long): EngineerTotalsResponse {
        val response = totalsDao.subordinatesTotals(headEngineerId)
        return EngineerTotalsResponse(
            response.justCreated,
            response.inProgress,
            response.finished
        )
    }
}