package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.incident

import com.seleznyoviyu.alarmdesk.server.mobile.da.client.incident.IncidentTypeEntityFindAllByParentIdClient
import com.seleznyoviyu.alarmdesk.server.mobile.da.client.incident.IncidentTypeEntityFindAllWithNoParentIdClient
import com.seleznyoviyu.alarmdesk.server.mobile.domain.mapper.incident.SimplifiedIncidentTypeParameterMapper
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.SimplifiedIncidentType
import org.springframework.stereotype.Service

@Service
class SimplifiedIncidentTypeListService(
    private val findClient: IncidentTypeEntityFindAllByParentIdClient,
    private val findRootClient: IncidentTypeEntityFindAllWithNoParentIdClient,
    private val parameterMapper: SimplifiedIncidentTypeParameterMapper
) {
    fun listByParent(parentId: Long): List<SimplifiedIncidentType> {
        return findClient.findByParentId(parentId)
            .map {
                SimplifiedIncidentType(
                    id = it.id,
                    parentId = it.parentId,
                    title = it.title,
                    isLast = it.isLast,
                    parameters = it.parameters.map(parameterMapper::map)
                )
            }
    }

    fun listRoot(): List<SimplifiedIncidentType> {
        return findRootClient.findWithNoParentId().map {
            SimplifiedIncidentType(
                id = it.id,
                parentId = it.parentId,
                title = it.title,
                isLast = it.isLast,
                parameters = it.parameters.map(parameterMapper::map)
            )
        }
    }
}