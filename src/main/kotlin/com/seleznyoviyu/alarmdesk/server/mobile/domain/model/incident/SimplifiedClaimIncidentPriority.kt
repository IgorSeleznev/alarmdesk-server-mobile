package com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident

data class SimplifiedClaimIncidentPriority(
    val name: String,
    val title: String
)
