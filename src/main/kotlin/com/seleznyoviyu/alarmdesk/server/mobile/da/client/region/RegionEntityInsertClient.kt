package com.seleznyoviyu.alarmdesk.server.mobile.da.client.region

import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.region.RegionEntity
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Component

@Component
class RegionEntityInsertClient(
    private val jdbcTemplate: NamedParameterJdbcTemplate
) {
    fun insert(region: RegionEntity): Long {
        jdbcTemplate.update(
            """
INSERT INTO region (id, title)
VALUES (:id, :title)
            """.trimIndent(),
            MapSqlParameterSource()
                .addValue("id", region.id)
                .addValue("title", region.title)
        )
        return region.id!!
    }
}