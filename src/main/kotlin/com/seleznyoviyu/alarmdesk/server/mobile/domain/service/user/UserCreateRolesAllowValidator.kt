package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.user

import com.seleznyoviyu.alarmdesk.server.mobile.common.exception.NotAllowedException
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole
import org.springframework.stereotype.Component

@Component
class UserCreateRolesAllowValidator {

    companion object {
        val BUSINESS_ADMIN_CREATE_ROLES_ALLOW = arrayOf(
            UserRole.OPERATOR,
            UserRole.ENGINEER,
            UserRole.HEAD_ENGINEER,
            UserRole.MOBILE_USER
        )
        val SYSTEM_ADMIN_CREATE_ROLES_ALLOW = arrayOf(
            UserRole.BUSINESS_ADMIN,
            UserRole.SYSTEM_ADMIN
        )
        val HEAD_ENGINEER_CREATE_ROLES_ALLOW = arrayOf(
            UserRole.ENGINEER,
            UserRole.MOBILE_USER
        )
        val ENGINEER_CREATE_ROLES_ALLOW = arrayOf(
            UserRole.MOBILE_USER
        )
    }

    fun validate(authorRole: String, createUserRole: String) {
        if (authorRole == UserRole.BUSINESS_ADMIN
            && createUserRole in BUSINESS_ADMIN_CREATE_ROLES_ALLOW
        ) {
            return
        }
        if (authorRole == UserRole.SYSTEM_ADMIN
            && createUserRole in SYSTEM_ADMIN_CREATE_ROLES_ALLOW
        ) {
            return
        }
        if (authorRole == UserRole.DEVELOPER) {
            return
        }
        if (authorRole == UserRole.HEAD_ENGINEER
            && createUserRole in HEAD_ENGINEER_CREATE_ROLES_ALLOW
        ) {
            return
        }
        if (authorRole == UserRole.ENGINEER
            && createUserRole in ENGINEER_CREATE_ROLES_ALLOW
        ) {
            return
        }
        throw NotAllowedException("Пользователь не может создать другого пользователя с указанной ролью")
    }
}