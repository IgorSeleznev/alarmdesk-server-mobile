package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.claim.merger

import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.claim.ClaimEntity
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.Claim
import org.springframework.stereotype.Component

@Component
class ClaimDomainWithEntityMerger {

    fun merge(source: Claim, target: ClaimEntity, id: Long? = target.id): ClaimEntity {
        return target.copy(
            id = id,
            locationId = source.locationId,
            status = source.status,
            engineerId = source.engineerId,
            authorId = source.authorId,
            statusUpdateAuthorId = source.statusUpdateAuthorId,
            createDateTime = source.createDateTime,
            statusUpdateDateTime = source.statusUpdateDateTime,
            finishDateTime = source.finishDateTime
        )
    }
}