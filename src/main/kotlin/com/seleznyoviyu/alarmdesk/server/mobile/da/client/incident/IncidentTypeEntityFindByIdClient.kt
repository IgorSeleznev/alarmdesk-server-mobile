package com.seleznyoviyu.alarmdesk.server.mobile.da.client.incident

import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.incident.IncidentTypeEntity
import com.seleznyoviyu.alarmdesk.server.mobile.da.exception.NotFoundException
import com.seleznyoviyu.alarmdesk.server.mobile.da.rowmapper.incident.IncidentTypeEntityRowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Component

@Component
class IncidentTypeEntityFindByIdClient(
    private val jdbcTemplate: NamedParameterJdbcTemplate,
    private val rowMapper: IncidentTypeEntityRowMapper
) {
    fun findById(id: Long): IncidentTypeEntity {
        val result: List<IncidentTypeEntity> = jdbcTemplate.query(
            """
select
    i.id,
    i.title,
    i.parent_incident_type_id,
    i.default_incident_priority,
    i.parameters,
    i.is_equipment_describe,
    i.incident_resolve_sla,
    i.incident_reaction_sla,
    CASE WHEN i1.id IS NULL THEN true ELSE false END as is_last
from incident_type i
left join incident_type i1 on i.id = i1.parent_incident_type_id
where i.id = :id
            """.trimIndent(),
            MapSqlParameterSource().addValue("id", id),
            rowMapper
        )

        if (result.isNotEmpty()) {
            return result[0]
        }
        throw NotFoundException("Отсутствует результат при попытке получить следующее значение sequence")
    }
}