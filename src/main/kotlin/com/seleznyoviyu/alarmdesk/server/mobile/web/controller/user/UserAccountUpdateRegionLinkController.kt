package com.seleznyoviyu.alarmdesk.server.mobile.web.controller.user

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.BUSINESS_ADMIN
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.DEVELOPER
import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.user.UserAccountRegionUpdateService
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.user.UserAccountRegionUpdateRequestJson
import com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.user.UserAccountRegionUpdateRequestMapper
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.annotation.security.RolesAllowed

@RestController
@RequestMapping("/user-account")
class UserAccountRegionUpdateController(
    private val service: UserAccountRegionUpdateService,
    private val mapper: UserAccountRegionUpdateRequestMapper
) {
    @RolesAllowed(
        "ROLE_$DEVELOPER",
        "ROLE_$BUSINESS_ADMIN"
    )
    @PutMapping("/update-region")
    fun updateRegion(@RequestBody request: UserAccountRegionUpdateRequestJson) {
        service.updateRegion(
            mapper.map(request)
        )
    }
}