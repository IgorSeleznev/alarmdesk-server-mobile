package com.seleznyoviyu.alarmdesk.server.mobile.da.entity.claim

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.claim.ClaimStatus
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.UserAccountBrief
import java.time.Instant

data class ClaimStatusLogEntity(
    val datetime: Instant,
    val authorId: UserAccountBrief,
    val status: ClaimStatus
)
