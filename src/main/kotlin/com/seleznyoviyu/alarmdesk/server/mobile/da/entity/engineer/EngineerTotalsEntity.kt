package com.seleznyoviyu.alarmdesk.server.mobile.da.entity.engineer

data class EngineerTotalsEntity(
    val justCreated: Int,
    val inProgress: Int,
    val finished: Int
)
