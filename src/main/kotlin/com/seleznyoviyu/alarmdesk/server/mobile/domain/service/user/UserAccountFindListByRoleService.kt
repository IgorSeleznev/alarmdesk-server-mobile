package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.user

import com.seleznyoviyu.alarmdesk.server.mobile.da.client.user.UserAccountIdListByRoleClient
import org.springframework.stereotype.Service

@Service
class UserAccountFindListByRoleService(
    private val client: UserAccountIdListByRoleClient
) {
    fun listByRoles(roles: List<String>): List<Long> {
        return client.listByRoles(roles)
    }
}