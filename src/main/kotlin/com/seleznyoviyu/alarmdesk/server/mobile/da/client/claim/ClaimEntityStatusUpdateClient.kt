package com.seleznyoviyu.alarmdesk.server.mobile.da.client.claim

import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.claim.ClaimEntityStatusUpdateRequest
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Component
import java.sql.Timestamp
import java.sql.Types

@Component
class ClaimEntityStatusUpdateClient(
    private val jdbcTemplate: NamedParameterJdbcTemplate
) {
    fun updateStatus(request: ClaimEntityStatusUpdateRequest) {
        jdbcTemplate.update(
            """
UPDATE claim
SET status = :status,
    author_id = :authorId,
    status_update_datetime = :statusUpdateDateTime,
    comment = :comment
WHERE id = :id                
            """.trimIndent(),
            MapSqlParameterSource()
                .addValue("id", request.claimId)
                .addValue("status", request.newStatus.toString())
                .addValue("authorId", request.authorId)
                .addValue(
                    "statusUpdateDateTime",
                    Timestamp.from(
                        request.statusUpdateDateTime,
                    ),
                    Types.TIMESTAMP
                )
                .addValue("comment", request.comment)
        )
    }
}