package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.user

import com.seleznyoviyu.alarmdesk.server.mobile.da.client.user.UserAccountEntityFindExternalIdByIdClient
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user.UserAccount
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user.UserAccountSaveRequest
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Component

@Component
class UserAccountSaveBuilder(
    //private val sha256UtilService: Sha256UtilService
    private val passwordEncoder: PasswordEncoder,
    private val findByIdClient: UserAccountEntityFindExternalIdByIdClient
) {
    fun build(request: UserAccountSaveRequest): UserAccount {
        return UserAccount(
            id = request.id,
            name = request.name,
            login = request.login,
            passwordHash = passwordEncoder.encode(request.password),
            //sha256UtilService.sha256(request.password),
            role = request.role,
            regionId = request.regionId,
            externalId = if (request.id != null) {
                findByIdClient.findById(request.id)
            } else {
                null
            }
        )
    }
}