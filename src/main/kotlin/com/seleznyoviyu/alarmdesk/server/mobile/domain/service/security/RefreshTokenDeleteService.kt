package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.security

import com.seleznyoviyu.alarmdesk.server.mobile.da.client.refreshtoken.RefreshTokenEntityDeleteClient
import org.springframework.stereotype.Service
import java.util.*

@Service
class RefreshTokenDeleteService(
    private val deleteClient: RefreshTokenEntityDeleteClient
) {
    fun deleteRefreshToken(refreshToken: UUID) {
        deleteClient.delete(refreshToken)
    }
}