package com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.user

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user.SimplifiedUserAccount
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.user.SimplifyUserAccountJson
import org.mapstruct.InjectionStrategy
import org.mapstruct.Mapper

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
interface SimplifyUserAccountJsonMapper {
    fun map(source: SimplifiedUserAccount): SimplifyUserAccountJson
}