package com.seleznyoviyu.alarmdesk.server.mobile.da.entity.claim

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.claim.ClaimStatus
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.ClaimCommentLog
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.ClaimStatusLog
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.IncidentPriorityType
import java.time.Instant

data class ClaimEntity(
    val id: Long?,
    val locationId: Long,
    val status: ClaimStatus,
    val authorId: Long,
    val engineerId: Long?,
    val comment: String,
    val incidentTypeId: Long,
    val incidentTypeParameterValues: Map<Long, ClaimIncidentTypeParameterValueEntity>,
    val incidentPriority: IncidentPriorityType,
    val statusUpdateAuthorId: Long,
    val createDateTime: Instant,
    val statusUpdateDateTime: Instant,
    val engineerAssignDateTime: Instant?,
    val finishDateTime: Instant?,
    val technicalDetails: Map<String, Map<String, String>>?,
    val statusLog: List<ClaimStatusLog>,
    val commentLog: List<ClaimCommentLog>
)