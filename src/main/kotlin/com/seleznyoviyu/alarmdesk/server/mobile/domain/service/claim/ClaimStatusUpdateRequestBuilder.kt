package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.claim

import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.user.UserAccountFindByIdService
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.UserAccountBrief
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.request.ClaimStatusUpdateBriefRequest
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.request.ClaimStatusUpdateRequest
import org.springframework.stereotype.Component

@Component
class ClaimStatusUpdateRequestBuilder(
    private val userFindService: UserAccountFindByIdService,
    private val findService: ClaimFindByIdService
) {
    fun buildRequest(request: ClaimStatusUpdateBriefRequest): ClaimStatusUpdateRequest {
        val user = userFindService.findById(request.authorId)
        return ClaimStatusUpdateRequest(
            requestBrief = request,
            author = UserAccountBrief(
                userAccountId = user.id!!,
                name = user.name,
                login = user.login
            ),
            sourceClaim = findService.findById(request.claimId)
        )
    }
}