package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.incident

import com.seleznyoviyu.alarmdesk.server.mobile.da.client.incident.IncidentTypeEntityFindAllByParentIdClient
import com.seleznyoviyu.alarmdesk.server.mobile.da.client.incident.IncidentTypeEntityFindAllWithNoParentIdClient
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.IncidentTypeTitle
import org.springframework.stereotype.Service

@Service
class IncidentTypeTitleListService(
    private val findClient: IncidentTypeEntityFindAllByParentIdClient,
    private val findRootClient: IncidentTypeEntityFindAllWithNoParentIdClient
) {
    fun listByParent(parentId: Long): List<IncidentTypeTitle> {
        return findClient.findByParentId(parentId)
            .map {
                IncidentTypeTitle(
                    id = it.id,
                    title = it.title,
                    isLast = it.isLast
                )
            }
    }

    fun listRoot(): List<IncidentTypeTitle> {
        return findRootClient.findWithNoParentId().map {
            IncidentTypeTitle(
                id = it.id,
                title = it.title,
                isLast = it.isLast
            )
        }
    }
}