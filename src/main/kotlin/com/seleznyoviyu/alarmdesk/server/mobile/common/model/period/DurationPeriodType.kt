package com.seleznyoviyu.alarmdesk.server.mobile.common.model.period

enum class DurationPeriodType {
    SECONDS,
    MINUTES,
    HOURS,
    DAYS,
    WEEKS,
    MONTHS,
    QUARTERS,
    YEARS
}