package com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.incident.IncidentValueType
import java.util.*

data class IncidentTypeParameter(
    val id: Long?,
    val title: String,
    val incidentValueType: IncidentValueType,
    val sortOrder: Int,
    val options: Map<Long, String>?
)
