package com.seleznyoviyu.alarmdesk.server.mobile.da.client.user

import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.user.UserAccountEntity
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Component
import java.sql.ResultSet
import java.util.*

@Component
class UserAccountEntityFindByLoginClient(
    private val jdbcTemplate: NamedParameterJdbcTemplate
) {
    fun findByLogin(login: String): Optional<UserAccountEntity> {
        val result = jdbcTemplate.query(
            """
select
    id,
    name,
    login,
    password_hash,
    role,
    region_id,
    external_id
from user_account
where login = :login
            """.trimIndent(),
            MapSqlParameterSource()
                .addValue("login", login)
        ) { resultSet: ResultSet, _: Int ->
            UserAccountEntity(
                id = resultSet.getLong(1),
                name = resultSet.getString(2),
                login = resultSet.getString(3),
                passwordHash = resultSet.getString(4),
                role = resultSet.getString(5),
                regionId = resultSet.getLong(6),
                externalId = resultSet.getObject(7, UUID::class.java)
            )
        }

        if (result.isNotEmpty()) {
            return Optional.of(result[0])
        }
        return Optional.empty()
    }
}