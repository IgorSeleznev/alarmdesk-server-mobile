package com.seleznyoviyu.alarmdesk.server.mobile.web.json.notification

import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.ClaimIncidentTypeParameterValue
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user.SimplifiedUserAccount
import java.time.Instant

@JsonNaming(PropertyNamingStrategies.KebabCaseStrategy::class)
data class ClaimCreatedNotificationJson(
    val claimId: Long,
    val author: SimplifiedUserAccount,
    val createdDateTime: Instant,
    val incidentTitles: List<String>,
    val incidentTypeId: Long,
    val incidentTypeParameterValues: Map<Long, ClaimIncidentTypeParameterValue>,
    val comment: String?
)
