package com.seleznyoviyu.alarmdesk.server.mobile.da.client.incident

import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.incident.SimplifiedIncidentTypeEntity
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Component

@Component
class EquipmentDescribeListClient(
    private val jdbcTemplate: NamedParameterJdbcTemplate,
) {
    fun listAll(): List<SimplifiedIncidentTypeEntity> {
        return jdbcTemplate.query(
            """
SELECT i.id, 
       i.parent_incident_type_id, 
       i.title, 
       CASE WHEN i1.id IS NULL THEN true ELSE false END as is_last
FROM incident_type i
LEFT JOIN incident_type i1 ON i1.parent_incident_type_id = i.id
WHERE i.is_equipment_describe = TRUE
ORDER BY i.title        
            """.trimIndent()
        ) { resultSet, _ ->
            SimplifiedIncidentTypeEntity(
                id = resultSet.getLong(1),
                parentId = resultSet.getLong(2),
                title = resultSet.getString(3),
                isLast = resultSet.getBoolean(4)
            )
        }
    }
}