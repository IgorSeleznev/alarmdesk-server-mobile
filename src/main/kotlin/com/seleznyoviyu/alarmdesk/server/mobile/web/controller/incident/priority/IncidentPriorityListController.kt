package com.seleznyoviyu.alarmdesk.server.mobile.web.controller.incident.priority

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.BUSINESS_ADMIN
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.DEVELOPER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.ENGINEER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.HEAD_ENGINEER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.MOBILE_USER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.OPERATOR
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.SYSTEM_ADMIN
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.WEB_USER
import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.incident.IncidentPriorityListService
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.claim.SimplifiedClaimIncidentPriorityJson
import com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.incident.priority.SimplifiedClaimIncidentPriorityJsonMapper
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.annotation.security.RolesAllowed

@RestController
@RequestMapping("/incident-type")
class IncidentPriorityListController(
    private val priorityListService: IncidentPriorityListService,
    private val mapper: SimplifiedClaimIncidentPriorityJsonMapper
) {
    @RolesAllowed(
        "ROLE_$MOBILE_USER",
        "ROLE_$DEVELOPER",
        "ROLE_$OPERATOR",
        "ROLE_$ENGINEER",
        "ROLE_$SYSTEM_ADMIN",
        "ROLE_$BUSINESS_ADMIN",
        "ROLE_$WEB_USER",
        "ROLE_$HEAD_ENGINEER"
    )
    @GetMapping("/priority/list")
    fun incidentPriorityList(): List<SimplifiedClaimIncidentPriorityJson> {
        return priorityListService.list().map(mapper::map)
    }
}