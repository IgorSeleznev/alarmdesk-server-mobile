package com.seleznyoviyu.alarmdesk.server.mobile.da.client.incident

import com.seleznyoviyu.alarmdesk.server.mobile.da.exception.NotFoundException
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Component

@Component
class IncidentTypeTitleFindByIdClient(
    private val jdbcTemplate: NamedParameterJdbcTemplate
) {
    fun titleById(incidentTypeId: Long): String {
        val result = jdbcTemplate.query(
            """
SELECT title
FROM incident_type
WHERE id = :id                
            """.trimIndent(),
            MapSqlParameterSource().addValue("id", incidentTypeId)
        ) { resultSet, _ ->
            resultSet.getString(1)
        }

        if (result.isNotEmpty()) {
            return result[0]
        }

        throw NotFoundException("Не найден тип инцидента для id = $incidentTypeId")
    }
}