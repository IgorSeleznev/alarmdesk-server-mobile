package com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident

enum class IncidentPriorityType(
    val title: String,
    val sortOrder: Int
) {
    LOWEST("самый низкий", 0),
    LOW("низкий", 1),
    MEDIUM("средний", 2),
    HIGH("высокий", 3),
    CRITICAL("критичный", 4);

    companion object {
        fun check(value: String): Boolean {
            return try {
                valueOf(value)
                true
            } catch (exception: Exception) {
                false
            }
        }
    }
}