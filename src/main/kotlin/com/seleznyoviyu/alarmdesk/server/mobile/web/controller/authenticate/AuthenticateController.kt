package com.seleznyoviyu.alarmdesk.server.mobile.web.controller.authenticate

import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.security.AuthenticationService
import com.seleznyoviyu.alarmdesk.server.mobile.web.configuration.JwtTokenUtil
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.mobile.authenticate.AuthenticateRequestJson
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.mobile.authenticate.AuthenticateResponseJson
import com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.security.AuthenticateRequestDomainMapper
import com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.security.AuthenticateResponseJsonMapper
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class AuthenticateController(
    private val authenticationService: AuthenticationService,
    private val requestMapper: AuthenticateRequestDomainMapper,
    private val responseMapper: AuthenticateResponseJsonMapper
) {
    /*fun authenticate(@RequestBody request: AuthenticateRequestJson): AuthenticateResponseJson {
        return AuthenticateResponseJson(
            service.authenticate(request.login, request.password)
        )
    }*/
    @PostMapping("/authenticate")
    fun login(@RequestBody request: AuthenticateRequestJson): ResponseEntity<AuthenticateResponseJson?>? {
        return try {
            val authentication = authenticationService.authenticate(
                requestMapper.map(request)
            )
            ResponseEntity.ok()
                //.header(HttpHeaders.AUTHORIZATION, jwtToken)
                .header(JwtTokenUtil.ACCESS_TOKEN_HEADER, authentication.accessToken)
                .header(JwtTokenUtil.REFRESH_TOKEN_HEADER, authentication.refreshToken)
                .body(
                    responseMapper.map(authentication)
                )
        } catch (ex: BadCredentialsException) {
            ResponseEntity.status(HttpStatus.UNAUTHORIZED).build<AuthenticateResponseJson>()
        }
    }
}