package com.seleznyoviyu.alarmdesk.server.mobile.da.client.location

import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.location.LocationEntity
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Component

@Component
class LocationUpdateClient(
    private val jdbcTemplate: NamedParameterJdbcTemplate
) {
    fun update(location: LocationEntity): Long {
        jdbcTemplate.update(
            """
UPDATE location
SET ukm_location = :ukmLocation,
    title = :title,
    region_id = :regionId,
    address = :address
WHERE id = :id
            """.trimIndent(),
            MapSqlParameterSource()
                .addValue("ukmLocation", location.ukmLocation)
                .addValue("title", location.title)
                .addValue("regionId", location.regionId)
                .addValue("address", location.address)
                .addValue("id", location.id)
        )
        return location.id!!
    }
}