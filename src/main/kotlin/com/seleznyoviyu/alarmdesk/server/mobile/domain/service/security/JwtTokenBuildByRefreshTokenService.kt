package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.security

import com.seleznyoviyu.alarmdesk.server.mobile.web.configuration.JwtTokenUtil
import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.user.UserAccountFindByIdService
import com.seleznyoviyu.alarmdesk.server.mobile.common.exception.ForbiddenException
import com.seleznyoviyu.alarmdesk.server.mobile.da.client.refreshtoken.UserAccountIdFindByRefreshTokenClient
import com.seleznyoviyu.alarmdesk.server.mobile.da.exception.NotFoundException
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.security.JwtTokenBuildByRefreshTokenResult
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.security.UserAccountSecurityAuthority
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.security.UserAccountSecurityDetails
import org.springframework.stereotype.Service
import java.util.*

@Service
class JwtTokenBuildByRefreshTokenService(
    private val userAccountIdFindClient: UserAccountIdFindByRefreshTokenClient,
    private val jwtTokenUtil: JwtTokenUtil,
    private val userFindService: UserAccountFindByIdService,
) {
    fun buildJwtToken(refreshToken: String): JwtTokenBuildByRefreshTokenResult {
        val userAccountId = try {
            userAccountIdFindClient.findUserAccountIdByRefreshToken(
                refreshToken = UUID.fromString(refreshToken)
            )
        } catch (exception: NotFoundException) {
            throw ForbiddenException()
        }
        val userAccount = userFindService.findById(userAccountId)
        return JwtTokenBuildByRefreshTokenResult(
            jwtToken = jwtTokenUtil.generateAccessToken(
                UserAccountSecurityDetails(
                    userAccount = userAccount,
                    authorities = mutableListOf(
                        UserAccountSecurityAuthority(
                            userAccount.role
                        )
                    )
                )
            )!!,
            userAccount = userAccount
        )
    }
}