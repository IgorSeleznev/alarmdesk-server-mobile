package com.seleznyoviyu.alarmdesk.server.mobile.domain.mapper.incident

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.IncidentType
import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.incident.IncidentTypeEntity
import org.mapstruct.InjectionStrategy
import org.mapstruct.Mapper


@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
interface IncidentTypeMapper {
    fun map(source: IncidentTypeEntity): IncidentType
}