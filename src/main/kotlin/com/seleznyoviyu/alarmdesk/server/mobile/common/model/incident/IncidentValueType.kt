package com.seleznyoviyu.alarmdesk.server.mobile.common.model.incident

enum class IncidentValueType {
    CHECK,
    RADIO,
    DATE,
    TIME,
    DATETIME,
    DURATION,
    NUMBER,
    STRING
}