package com.seleznyoviyu.alarmdesk.server.mobile.web.json.notification

import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.claim.ClaimStatus
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user.SimplifiedUserAccount
import java.time.Instant

@JsonNaming(PropertyNamingStrategies.KebabCaseStrategy::class)
data class ClaimStatusUpdateNotificationJson(
    val claimId: Long,
    val statusUpdateAuthor: SimplifiedUserAccount,
    val statusUpdateDateTime: Instant,
    val previousStatus: ClaimStatus,
    val newStatus: ClaimStatus,
    val comment: String?
)