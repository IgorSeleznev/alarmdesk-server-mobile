package com.seleznyoviyu.alarmdesk.server.mobile.web.json.region

data class RegionJson(
    val id: Long?,
    val title: String
)
