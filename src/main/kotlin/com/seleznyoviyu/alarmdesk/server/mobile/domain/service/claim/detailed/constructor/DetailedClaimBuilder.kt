package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.claim.detailed.constructor

import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.claim.ClaimEntity
import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.incident.IncidentTypeEntity
import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.location.LocationEntity
import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.user.UserAccountEntity
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.ClaimCommentLog
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.ClaimStatusLog
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.DetailedClaim
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.DetailedClaimIncidentTypeParameterValue
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.IncidentPriorityType
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.SimplifiedClaimIncidentType
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.location.SimplifiedLocation
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user.SimplifiedUserAccount
import java.util.*

class DetailedClaimBuilder(
    val claimId: Long
) {
    lateinit var claimEntity: ClaimEntity
    lateinit var incident: IncidentTypeEntity
    lateinit var incidentPriority: IncidentPriorityType
    lateinit var statusLog: List<ClaimStatusLog>
    lateinit var commentLog: List<ClaimCommentLog>
    lateinit var engineer: Optional<UserAccountEntity>
    lateinit var author: UserAccountEntity
    lateinit var statusUpdateAuthor: UserAccountEntity
    lateinit var location: LocationEntity
    lateinit var values: Map<Long, DetailedClaimIncidentTypeParameterValue>

    fun build(): DetailedClaim {
        return DetailedClaim(
            incident = SimplifiedClaimIncidentType(
                incident.id,
                incident.title
            ),
            incidentPriority = claimEntity.incidentPriority,
            location = SimplifiedLocation(
                id = location.id!!,
                title = location.title
            ),
            author = SimplifiedUserAccount(
                author.id!!,
                author.name
            ),
            engineer = if (engineer.isEmpty) null else SimplifiedUserAccount(
                id = engineer.get().id!!,
                name = engineer.get().name
            ),
            statusUpdateAuthor = SimplifiedUserAccount(
                statusUpdateAuthor.id!!,
                statusUpdateAuthor.name
            ),
            createDateTime = claimEntity.createDateTime,
            finishDateTime = claimEntity.finishDateTime,
            id = claimEntity.id,
            status = claimEntity.status,
            statusUpdateDateTime = claimEntity.statusUpdateDateTime,
            technicalDetails = claimEntity.technicalDetails,
            incidentTypeParameterValues = values,
            commentLog = claimEntity.commentLog,
            statusLog = claimEntity.statusLog,
            comment = claimEntity.comment
        )
    }
}