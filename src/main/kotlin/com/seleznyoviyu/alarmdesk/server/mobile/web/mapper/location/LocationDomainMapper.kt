package com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.location

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.location.Location
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.location.LocationJson
import org.mapstruct.InjectionStrategy
import org.mapstruct.Mapper

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
interface LocationDomainMapper {
    fun map(source: LocationJson): Location
}
