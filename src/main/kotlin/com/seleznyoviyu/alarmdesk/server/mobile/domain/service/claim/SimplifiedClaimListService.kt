package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.claim

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.claim.ClaimStatus
import com.seleznyoviyu.alarmdesk.server.mobile.da.client.claim.SimplifiedClaimFindByStatusByEngineerIdClient
import com.seleznyoviyu.alarmdesk.server.mobile.domain.mapper.claim.SimplifiedClaimMapper
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.SimplifiedClaim
import org.springframework.stereotype.Service

@Service
class SimplifiedClaimListService(
    private val client: SimplifiedClaimFindByStatusByEngineerIdClient,
    private val mapper: SimplifiedClaimMapper
) {
    fun list(status: ClaimStatus, engineerId: Long): List<SimplifiedClaim> {
        return client.findSimplifiedByStatusByEngineerId(status, engineerId).map(mapper::map)
    }
}