package com.seleznyoviyu.alarmdesk.server.mobile.da.client.user

import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.user.UserAccountEntity
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Component

@Component
class UserAccountEntityInsertClient(
    private val jdbcTemplate: NamedParameterJdbcTemplate
) {
    fun insert(userAccount: UserAccountEntity): Long {
        jdbcTemplate.update(
            """
INSERT INTO user_account (
    id,
    name,
    login,
    password_hash,
    role,
    region_id,
    external_id
) values (
    :id,
    :name,
    :login,
    :passwordHash,
    :role,
    :regionId,
    :externalId
)            """.trimIndent(),
            MapSqlParameterSource()
                .addValue("id", userAccount.id)
                .addValue("name", userAccount.name)
                .addValue("login", userAccount.login)
                .addValue("passwordHash", userAccount.passwordHash)
                .addValue("role", userAccount.role)
                .addValue("regionId", userAccount.regionId)
                .addValue("externalId", userAccount.externalId)
        )
        return userAccount.id!!
    }
}