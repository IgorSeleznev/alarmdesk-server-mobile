package com.seleznyoviyu.alarmdesk.server.mobile.domain.mapper.region

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.region.Region
import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.region.RegionEntity
import org.mapstruct.InjectionStrategy
import org.mapstruct.Mapper


@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
interface RegionMapper {
    fun map(source: RegionEntity): Region
}