package com.seleznyoviyu.alarmdesk.server.mobile.web.json.user

import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming

@JsonNaming(PropertyNamingStrategies.KebabCaseStrategy::class)
data class UserAccountSaveRequestJson(
    val id: Long?,
    val name: String,
    val login: String,
    val password: String,
    val role: String,
    val regionId: Long
)
