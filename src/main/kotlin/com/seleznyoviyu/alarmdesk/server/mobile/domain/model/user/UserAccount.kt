package com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user

import com.seleznyoviyu.alarmdesk.server.mobile.common.security.jwt.IdentifiedUser
import java.util.*

data class UserAccount(
    override val id: Long?,
    val name: String,
    val login: String,
    val passwordHash: String,
    val role: String,
    val regionId: Long,
    val externalId: UUID?
) : IdentifiedUser {
    override fun getUsername(): String {
        return login
    }
}