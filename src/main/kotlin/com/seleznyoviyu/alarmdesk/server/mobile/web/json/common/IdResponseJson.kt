package com.seleznyoviyu.alarmdesk.server.mobile.web.json.common

data class IdResponseJson(
    val id: Long
)
