package com.seleznyoviyu.alarmdesk.server.mobile.web.json.incident

import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.IncidentPriorityType
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.IncidentTypeParameter

@JsonNaming(PropertyNamingStrategies.KebabCaseStrategy::class)
data class IncidentTypeJson(
    val id: Long?,
    val parentId: Long?,
    val title: String,
    val defaultPriority: IncidentPriorityType,
    val parameters: List<IncidentTypeParameter>,
    val isEquipmentDescribe: Boolean,
    val incidentResolveSla: Int,
    val incidentReactionSla: Int,
    val isLast: Boolean
)
