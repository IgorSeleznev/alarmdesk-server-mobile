package com.seleznyoviyu.alarmdesk.server.mobile.web.controller.incident

import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.incident.SimplifiedIncidentTypeListService
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.incident.SimplifiedIncidentTypeJson
import com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.incident.SimplifiedIncidentTypeJsonMapper
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/incident-type")
class SimplifiedIncidentTypeListController(
    private val service: SimplifiedIncidentTypeListService,
    private val mapper: SimplifiedIncidentTypeJsonMapper
) {
    /*@RolesAllowed(
        "ROLE_$MOBILE_USER",
        "ROLE_$DEVELOPER",
        "ROLE_$OPERATOR",
        "ROLE_$ENGINEER",
        "ROLE_$SYSTEM_ADMIN",
        "ROLE_$BUSINESS_ADMIN",
        "ROLE_$WEB_USER",
        "ROLE_$HEAD_ENGINEER"
    )*/
    @GetMapping("/list-by-parent-simplified/{id}")
    fun incidentTypeListByParent(@PathVariable("id") id: Long): List<SimplifiedIncidentTypeJson> {
        return service.listByParent(id).map(mapper::map)
    }

    /*@RolesAllowed(
        "ROLE_$MOBILE_USER",
        "ROLE_$DEVELOPER",
        "ROLE_$OPERATOR",
        "ROLE_$ENGINEER",
        "ROLE_$SYSTEM_ADMIN",
        "ROLE_$BUSINESS_ADMIN",
        "ROLE_$WEB_USER",
        "ROLE_$HEAD_ENGINEER"
    )*/
    @GetMapping("/list-root-simplified")
    fun rootIncidentTypeList(): List<SimplifiedIncidentTypeJson> {
        return service.listRoot().map(mapper::map)
    }
}