package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.user

import com.seleznyoviyu.alarmdesk.server.mobile.da.client.user.UserAccountEntityFindByIdClient
import com.seleznyoviyu.alarmdesk.server.mobile.da.exception.NotFoundException
import com.seleznyoviyu.alarmdesk.server.mobile.domain.mapper.user.UserAccountMapper
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user.UserAccount
import org.springframework.stereotype.Service

@Service
class UserAccountFindByIdService(
    private val client: UserAccountEntityFindByIdClient,
    private val mapper: UserAccountMapper
) {
    fun findById(id: Long): UserAccount {
        return mapper.map(
            client
                .findById(id)
                .orElseThrow {
                    throw NotFoundException("Не найден пользователь с идентификатором $id")
                }
        )
    }
}