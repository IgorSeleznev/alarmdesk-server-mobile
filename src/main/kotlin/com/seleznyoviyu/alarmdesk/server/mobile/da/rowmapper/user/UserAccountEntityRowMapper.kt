package com.seleznyoviyu.alarmdesk.server.mobile.da.rowmapper.user

import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.user.UserAccountEntity
import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Component
import java.sql.ResultSet
import java.util.*

@Component
class UserAccountEntityRowMapper : RowMapper<UserAccountEntity> {
    override fun mapRow(resultSet: ResultSet, rowNum: Int): UserAccountEntity? {
/*
 { resultSet: ResultSet, _: Int ->
            UserAccountEntity(
                id = resultSet.getLong(1),
                name = resultSet.getString(2),
                login = resultSet.getString(3),
                passwordHash = resultSet.getString(4),
                role = resultSet.getString(5),
                regionId = resultSet.getLong(6),
                externalId = resultSet.getObject(7, UUID::class.java)
            )
        }
 */
        return UserAccountEntity(
            id = resultSet.getLong(1),
            name = resultSet.getString(2),
            login = resultSet.getString(3),
            passwordHash = resultSet.getString(4),
            role = resultSet.getString(5),
            regionId = resultSet.getLong(6),
            externalId = resultSet.getObject(7, UUID::class.java)
        )
    }
}