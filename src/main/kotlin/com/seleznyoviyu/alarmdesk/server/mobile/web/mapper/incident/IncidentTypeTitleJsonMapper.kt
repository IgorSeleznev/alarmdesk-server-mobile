package com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.incident

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.IncidentTypeTitle
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.incident.IncidentTypeTitleJson
import org.mapstruct.InjectionStrategy
import org.mapstruct.Mapper

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
interface IncidentTypeTitleJsonMapper {
    fun map(source: IncidentTypeTitle): IncidentTypeTitleJson
}