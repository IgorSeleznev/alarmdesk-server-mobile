package com.seleznyoviyu.alarmdesk.server.mobile.domain.model.location

data class SimplifiedLocation(
    val id: Long,
    val title: String
)
