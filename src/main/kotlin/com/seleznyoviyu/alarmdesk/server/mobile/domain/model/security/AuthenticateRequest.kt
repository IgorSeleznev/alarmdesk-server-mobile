package com.seleznyoviyu.alarmdesk.server.mobile.domain.model.security

data class AuthenticateRequest(
    val login: String,
    val password: String
)
