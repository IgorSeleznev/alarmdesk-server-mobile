package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.claim.detailed.constructor

import com.seleznyoviyu.alarmdesk.server.mobile.da.client.location.LocationEntityFindByIdClient
import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.claim.detailed.constructor.ClaimLocationDetailedClaimConstructor.Companion.LOCATION_CLAIM_DETAILED_CONSTRUCTOR_BEAN_NAME
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Component

@Qualifier(LOCATION_CLAIM_DETAILED_CONSTRUCTOR_BEAN_NAME)
@Component
class ClaimLocationDetailedClaimConstructor(
    private val client: LocationEntityFindByIdClient
) : DetailedClaimConstructor {

    companion object {
        const val LOCATION_CLAIM_DETAILED_CONSTRUCTOR_BEAN_NAME = "ClaimLocationDetailedClaimConstructor"
    }

    override fun construct(builder: DetailedClaimBuilder): DetailedClaimBuilder {
        builder.location = client.findById(builder.claimEntity.locationId)
        return builder
    }
}