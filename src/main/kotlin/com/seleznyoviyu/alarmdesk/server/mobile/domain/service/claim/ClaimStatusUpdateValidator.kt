package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.claim

import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.user.UserAccountFindByIdService
import com.seleznyoviyu.alarmdesk.server.mobile.common.exception.NotAllowedException
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.claim.ClaimStatus
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.request.ClaimStatusUpdateBriefRequest
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user.UserAccount
import org.springframework.stereotype.Component

@Component
class ClaimStatusUpdateValidator(
    private val userFindService: UserAccountFindByIdService,
    private val claimStatusService: ClaimStatusByIdService,
    private val roleStatusesAllowed: Map<String, Map<ClaimStatus, List<ClaimStatus>>>
) {
    fun validate(request: ClaimStatusUpdateBriefRequest) {
        val actualStatus: ClaimStatus = claimStatusService.statusById(request.claimId)
        val user: UserAccount = userFindService.findById(request.authorId)
        val statusesAllowed = roleStatusesAllowed[user.role]
            ?: throw NotAllowedException("Пользователь не может поменять статус заявки")
        val list: List<ClaimStatus> = statusesAllowed[actualStatus]
            ?: throw NotAllowedException("Пользователь не может поменять статус заявки на ${actualStatus.title}")
        if (!list.contains(request.newStatus)) {
            throw NotAllowedException("Пользователь не может поменять статус заявки с ${request.newStatus} на ${actualStatus.title}")
        }
    }
}