package com.seleznyoviyu.alarmdesk.server.mobile.web.json.notification

import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.IncidentPriorityType
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user.SimplifiedUserAccount
import java.time.Instant

@JsonNaming(PropertyNamingStrategies.KebabCaseStrategy::class)
data class ClaimAssignedNotificationJson(
    val claimId: Long,
    val engineer: SimplifiedUserAccount,
    val author: SimplifiedUserAccount,
    val assignDateTime: Instant,
    val incidentTypeId: Long,
    val priority: IncidentPriorityType,
    val incidentTypesTitles: List<String>,
    val comment: String?
)