package com.seleznyoviyu.alarmdesk.server.mobile.web.json.claim.request

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.claim.ClaimStatus

data class ClaimStatusRequestJson(
    val status: ClaimStatus
)
