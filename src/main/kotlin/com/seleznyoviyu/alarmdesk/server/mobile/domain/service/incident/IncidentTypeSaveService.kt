package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.incident

import com.seleznyoviyu.alarmdesk.server.mobile.da.client.common.NextIdClient
import com.seleznyoviyu.alarmdesk.server.mobile.da.client.incident.IncidentTypeEntityInsertClient
import com.seleznyoviyu.alarmdesk.server.mobile.da.client.incident.IncidentTypeEntityUpdateClient
import com.seleznyoviyu.alarmdesk.server.mobile.domain.mapper.incident.IncidentTypeEntityMapper
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.IncidentType
import org.springframework.stereotype.Service

@Service
class IncidentTypeSaveService(
    private val insertClient: IncidentTypeEntityInsertClient,
    private val updateClient: IncidentTypeEntityUpdateClient,
    private val mapper: IncidentTypeEntityMapper,
    private val nextIdClient: NextIdClient
) {
    fun save(incidentType: IncidentType): Long {
        if (incidentType.id == null) {
            return insertClient.insert(
                mapper.map(incidentType).copy(
                    id = nextIdClient.nextId("incident_type_seq")
                )
            )
        }
        return updateClient.update(
            mapper.map(incidentType)
        )
    }
}