package com.seleznyoviyu.alarmdesk.server.mobile.da.client.claim

import com.fasterxml.jackson.databind.ObjectMapper
import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.claim.ClaimEntity
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Component
import java.sql.Timestamp
import java.sql.Types

@Component
class ClaimEntityUpdateClient(
    private val objectMapper: ObjectMapper,
    private val jdbcTemplate: NamedParameterJdbcTemplate
) {
    fun update(claim: ClaimEntity) {
        jdbcTemplate.update(
            """
update claim
set
    location_id = :locationId,
    status = :status,
    author_id = :authorId,
    engineer_id = :engineerId,
    comment = :comment,
    incident_type_id = :incidentTypeId,
    incident_type_parameter_values = :incidentTypeParameterValues,
    incident_priority = :incidentPriority,
    status_update_author_id = :statusUpdateAuthorId,
    create_datetime = :createdDateTime,
    status_update_datetime = :statusUpdateDateTime,
    engineer_assign_datetime = :engineerAssignDateTime,
    finish_datetime = :finishedDateTime,
    technical_details = :technicalDetails,
    status_log = :statusLog,
    comment_log = :commentLog

where id = :id                
            """.trimIndent(),
            MapSqlParameterSource()
                .addValue("id", claim.id)
                .addValue("locationId", claim.locationId)
                .addValue("status", claim.status.toString())
                .addValue("authorId", claim.authorId)
                .addValue("engineerId", claim.engineerId)
                .addValue("comment", claim.comment)
                .addValue("incidentTypeId", claim.incidentTypeId)
                .addValue(
                    "incidentTypeParameterValues",
                    objectMapper.writeValueAsString(
                        claim.incidentTypeParameterValues
                    )
                )
                .addValue("incidentPriority", claim.incidentPriority)
                .addValue("statusUpdateAuthorId", claim.statusUpdateAuthorId)
                .addValue(
                    "createdDateTime",
                    Timestamp.from(
                        claim.createDateTime,
                    ),
                    Types.TIMESTAMP
                )
                .addValue(
                    "statusUpdateDateTime",
                    Timestamp.from(
                        claim.statusUpdateDateTime,
                    ),
                    Types.TIMESTAMP
                )
                .addValue(
                    "engineerAssignDateTime",
                    Timestamp.from(
                        claim.engineerAssignDateTime,
                    ),
                    Types.TIMESTAMP
                )
                .addValue(
                    "finishedDateTime",
                    Timestamp.from(
                        claim.finishDateTime,
                    ),
                    Types.TIMESTAMP
                )
                .addValue(
                    "technicalDetails",
                    objectMapper.writeValueAsString(
                        claim.technicalDetails
                    )
                )
                .addValue(
                    "statusLog",
                    objectMapper.writeValueAsString(
                        claim.statusLog
                    )
                )
                .addValue(
                    "commentLog",
                    objectMapper.writeValueAsString(
                        claim.commentLog
                    )
                )
        )
    }
}