package com.seleznyoviyu.alarmdesk.server.mobile.web.json.incident.parameter

import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.incident.IncidentValueType

@JsonNaming(PropertyNamingStrategies.KebabCaseStrategy::class)
data class IncidentTypeParameterJson(
    val id: Long?,
    val title: String,
    val incidentValueType: IncidentValueType,
    val sortOrder: Int,
    val options: Map<Long, String>?
)
