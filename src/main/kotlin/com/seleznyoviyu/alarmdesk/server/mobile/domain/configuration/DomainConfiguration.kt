package com.seleznyoviyu.alarmdesk.server.mobile.domain.configuration

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.claim.ClaimStatus
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class DomainConfiguration {

    @Bean
    fun roleClaimStatusesAllowedMap(): Map<String, Map<ClaimStatus, List<ClaimStatus>>> {
        return hashMapOf(
            Pair(
                UserRole.ENGINEER,
                hashMapOf(
                    Pair(ClaimStatus.CREATED, listOf(ClaimStatus.IN_PROGRESS)),
                    Pair(ClaimStatus.IN_PROGRESS, listOf(ClaimStatus.ON_HOLD, ClaimStatus.FINISHED)),
                    Pair(ClaimStatus.ON_HOLD, listOf(ClaimStatus.IN_PROGRESS))
                )
            ),
            Pair(
                UserRole.HEAD_ENGINEER,
                hashMapOf(
                    Pair(ClaimStatus.CREATED, listOf(ClaimStatus.IN_PROGRESS)),
                    Pair(ClaimStatus.IN_PROGRESS, listOf(ClaimStatus.ON_HOLD, ClaimStatus.FINISHED)),
                    Pair(ClaimStatus.ON_HOLD, listOf(ClaimStatus.IN_PROGRESS))
                )
            ),
            Pair(
                UserRole.OPERATOR,
                hashMapOf(
                    Pair(
                        ClaimStatus.IN_PROGRESS,
                        listOf(ClaimStatus.CANCELED, ClaimStatus.FINISHED, ClaimStatus.ON_HOLD, ClaimStatus.CREATED)
                    ),
                    Pair(
                        ClaimStatus.ON_HOLD,
                        listOf(ClaimStatus.IN_PROGRESS, ClaimStatus.CANCELED, ClaimStatus.FINISHED)
                    )
                )
            ),
            Pair(
                UserRole.DEVELOPER,
                hashMapOf(
                    Pair(
                        ClaimStatus.CREATED,
                        listOf(ClaimStatus.CANCELED, ClaimStatus.FINISHED, ClaimStatus.ON_HOLD, ClaimStatus.IN_PROGRESS)
                    ),
                    Pair(
                        ClaimStatus.IN_PROGRESS,
                        listOf(ClaimStatus.CANCELED, ClaimStatus.FINISHED, ClaimStatus.ON_HOLD, ClaimStatus.CREATED)
                    ),
                    Pair(
                        ClaimStatus.ON_HOLD,
                        listOf(ClaimStatus.CANCELED, ClaimStatus.FINISHED, ClaimStatus.IN_PROGRESS, ClaimStatus.CREATED)
                    ),
                    Pair(
                        ClaimStatus.CANCELED,
                        listOf(ClaimStatus.ON_HOLD, ClaimStatus.FINISHED, ClaimStatus.IN_PROGRESS, ClaimStatus.CREATED)
                    ),
                    Pair(
                        ClaimStatus.FINISHED,
                        listOf(ClaimStatus.ON_HOLD, ClaimStatus.CANCELED, ClaimStatus.IN_PROGRESS, ClaimStatus.CREATED)
                    ),
                )
            ),
            Pair(
                UserRole.BUSINESS_ADMIN,
                hashMapOf(
                    Pair(
                        ClaimStatus.CREATED,
                        listOf(ClaimStatus.CANCELED, ClaimStatus.FINISHED, ClaimStatus.ON_HOLD, ClaimStatus.IN_PROGRESS)
                    ),
                    Pair(
                        ClaimStatus.IN_PROGRESS,
                        listOf(ClaimStatus.CANCELED, ClaimStatus.FINISHED, ClaimStatus.ON_HOLD, ClaimStatus.CREATED)
                    ),
                    Pair(
                        ClaimStatus.ON_HOLD,
                        listOf(ClaimStatus.CANCELED, ClaimStatus.FINISHED, ClaimStatus.IN_PROGRESS, ClaimStatus.CREATED)
                    ),
                )
            )
        )
    }
}