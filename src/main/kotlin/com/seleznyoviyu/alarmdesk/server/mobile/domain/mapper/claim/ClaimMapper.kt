package com.seleznyoviyu.alarmdesk.server.mobile.domain.mapper.claim

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.Claim
import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.claim.ClaimEntity
import org.mapstruct.InjectionStrategy
import org.mapstruct.Mapper


@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
interface ClaimMapper {
    fun map(source: ClaimEntity): Claim
}