package com.seleznyoviyu.alarmdesk.server.mobile.common.model.claim

enum class ClaimStatus(
    val title: String
) {
    CREATED("Создана"),
    IN_PROGRESS("В работе"),
    ON_HOLD("Отложена"),
    CANCELED("Отменена"),
    FINISHED("Завершена")
}