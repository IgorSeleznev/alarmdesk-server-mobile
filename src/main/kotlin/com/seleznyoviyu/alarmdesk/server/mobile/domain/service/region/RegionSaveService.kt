package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.region

import com.seleznyoviyu.alarmdesk.server.mobile.da.client.common.NextIdClient
import com.seleznyoviyu.alarmdesk.server.mobile.da.client.region.RegionEntityInsertClient
import com.seleznyoviyu.alarmdesk.server.mobile.da.client.region.RegionEntityUpdateClient
import com.seleznyoviyu.alarmdesk.server.mobile.domain.mapper.region.RegionEntityMapper
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.region.Region
import org.springframework.stereotype.Service

@Service
class RegionSaveService(
    private val updateClient: RegionEntityUpdateClient,
    private val insertClient: RegionEntityInsertClient,
    private val nextIdClient: NextIdClient,
    private val mapper: RegionEntityMapper
) {
    fun save(region: Region): Long {
        if (region.id == null) {
            return insertClient.insert(
                mapper.map(region).copy(id = nextIdClient.nextId("region_seq"))
            )
        }
        return updateClient.update(
            mapper.map(region)
        )
    }
}