package com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.user

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user.UserAccountSaveRequest
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.user.UserAccountSaveRequestJson
import org.mapstruct.InjectionStrategy
import org.mapstruct.Mapper

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
interface UserAccountSaveRequestDomainMapper {
    fun map(source: UserAccountSaveRequestJson): UserAccountSaveRequest
}