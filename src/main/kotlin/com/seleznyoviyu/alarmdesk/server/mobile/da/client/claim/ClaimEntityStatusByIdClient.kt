package com.seleznyoviyu.alarmdesk.server.mobile.da.client.claim

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.claim.ClaimStatus
import com.seleznyoviyu.alarmdesk.server.mobile.da.exception.NotFoundException
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Component
import java.sql.ResultSet

@Component
class ClaimEntityStatusByIdClient(
    private val jdbcTemplate: NamedParameterJdbcTemplate
) {
    fun statusById(id: Long): ClaimStatus {
        val result = jdbcTemplate.query(
            """
SELECT status 
from claim
WHERE id = :id                
            """.trimIndent(),
            MapSqlParameterSource().addValue("id", id)
        ) { resultSet: ResultSet, _ ->
            ClaimStatus.valueOf(
                resultSet.getString(1)
            )
        }
        if (result.isNotEmpty()) {
            return result[0]
        }
        throw NotFoundException("Не найдена заявка с id = $id")
    }
}