package com.seleznyoviyu.alarmdesk.server.mobile.common.security.jwt

interface IdentifiedUser {
    val id: Long?
    fun getUsername(): String
}