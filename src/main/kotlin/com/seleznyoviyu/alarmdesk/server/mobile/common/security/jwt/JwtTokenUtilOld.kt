package com.seleznyoviyu.alarmdesk.server.mobile.common.security.jwt

/*
import io.jsonwebtoken.*
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.lang.String.format
import java.security.SignatureException
import java.util.*


 */
//@Component
class JwtTokenUtilOld /*{
    //    private val jwtSecret = "zdtlD3JK56m6wTTgsNFhqzjqP"
    private val jwtSecret = "UK4amqdW5Z4vCh2yMac0ufjAL"
    private val jwtIssuer = "alarmdesk.savantit.com"

    private val logger = LoggerFactory.getLogger("com.seleznyoviyu.alarmdesk.server.mobile.common.security.jwt.JwtTokenUtil")

    companion object {
        const val TOKEN_HEADER = "ALARMDESK-ACCESS-TOKEN"
    }

    fun generateAccessToken(user: IdentifiedUser): String? {
        return Jwts.builder()
            .setSubject(format("%s,%s", user.id, user.getUsername()))
            .setIssuer(jwtIssuer)
            .setIssuedAt(Date())
            .setExpiration(Date(System.currentTimeMillis() + 7 * 24 * 60 * 60 * 1000)) // 1 week
            .signWith(SignatureAlgorithm.HS512, jwtSecret)
            .compact()
    }

    fun getUserId(token: String?): String? {
        val claims: Claims = Jwts.parser()
            .setSigningKey(jwtSecret)
            .parseClaimsJws(token)
            .body
        return claims.subject.split(",")[0]
    }

    fun getUsername(token: String?): String? {
        val claims: Claims = Jwts.parser()
            .setSigningKey(jwtSecret)
            .parseClaimsJws(token)
            .body
        return claims.subject.split(",")[1]
    }

    fun getExpirationDate(token: String?): Date? {
        val claims: Claims = Jwts.parser()
            .setSigningKey(jwtSecret)
            .parseClaimsJws(token)
            .body
        return claims.expiration
    }

    fun validate(token: String?): Boolean {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token)
            return true
        } catch (exception: SignatureException) {
            logger.error("Invalid JWT signature - {}", exception.message)
        } catch (exception: MalformedJwtException) {
            logger.error("Invalid JWT token - {}", exception.message)
        } catch (exception: ExpiredJwtException) {
            logger.error("Expired JWT token - {}", exception.message)
        } catch (exception: UnsupportedJwtException) {
            logger.error("Unsupported JWT token - {}", exception.message)
        } catch (exception: IllegalArgumentException) {
            logger.error("JWT claims string is empty - {}", exception.message)
        }
        return false
    }
}*/