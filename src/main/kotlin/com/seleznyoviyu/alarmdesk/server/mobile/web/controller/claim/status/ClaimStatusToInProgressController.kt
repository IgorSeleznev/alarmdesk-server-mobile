package com.seleznyoviyu.alarmdesk.server.mobile.web.controller.claim.status

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.claim.ClaimStatus
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.DEVELOPER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.ENGINEER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.HEAD_ENGINEER
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.request.ClaimStatusUpdateBriefRequest
import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.claim.ClaimStatusInProgressAndEngineerAssignService
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.claim.ClaimStatusUpdateBriefRequestJson
import com.seleznyoviyu.alarmdesk.server.mobile.web.resolver.HttpSessionAuthenticatedUserResolver
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.annotation.security.RolesAllowed

@RestController
@RequestMapping("/claim")
class ClaimStatusToInProgressController(
    private val service: ClaimStatusInProgressAndEngineerAssignService,
    private val userResolver: HttpSessionAuthenticatedUserResolver
) {

    @RolesAllowed(
        "ROLE_$DEVELOPER",
        "ROLE_$ENGINEER",
        "ROLE_$HEAD_ENGINEER"
    )
    @PutMapping("/take-in-progress")
    fun statusToInProgress(@RequestBody request: ClaimStatusUpdateBriefRequestJson) {
        service.claimInProgressAndEngineerAssign(
            ClaimStatusUpdateBriefRequest(
                request.claimId,
                request.comment,
                userResolver.resolve().id!!,
                ClaimStatus.IN_PROGRESS
            )
        )
    }
}