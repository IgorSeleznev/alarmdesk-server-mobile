package com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.engineer

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.engineer.response.EngineerTotalsResponse
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.engineer.response.EngineerTotalsResponseJson
import org.mapstruct.InjectionStrategy
import org.mapstruct.Mapper

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
interface EngineerTotalsResponseJsonMapper {
    fun map(source: EngineerTotalsResponse): EngineerTotalsResponseJson
}