package com.seleznyoviyu.alarmdesk.server.mobile.web.json.incident

data class IncidentTypeTitleJson(
    val id: Long,
    val title: String,
    val isLast: Boolean
)
