package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.user

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user.UserAccountRegionUpdateRequest
import org.springframework.stereotype.Service

@Service
class UserAccountRegionUpdateService(
    private val findService: UserAccountFindByIdService,
    private val saveService: UserAccountSaver
) {
    fun updateRegion(request: UserAccountRegionUpdateRequest) {
        saveService.save(
            findService
                .findById(request.userAccountId)
                .copy(regionId = request.regionId)
        )
    }
}