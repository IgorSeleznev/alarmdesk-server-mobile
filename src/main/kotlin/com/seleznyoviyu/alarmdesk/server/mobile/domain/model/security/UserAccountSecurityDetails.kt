package com.seleznyoviyu.alarmdesk.server.mobile.domain.model.security

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user.UserAccount
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

data class UserAccountSecurityDetails(
    val userAccount: UserAccount,
    val authorities: MutableList<UserAccountSecurityAuthority>
) : UserDetails {
    override fun getAuthorities(): MutableCollection<out GrantedAuthority> {
        return authorities
    }

    override fun getPassword(): String {
        return userAccount.passwordHash
    }

    override fun getUsername(): String {
        return userAccount.login
    }

    override fun isAccountNonExpired(): Boolean {
        return true
    }

    override fun isAccountNonLocked(): Boolean {
        return true
    }

    override fun isCredentialsNonExpired(): Boolean {
        return true
    }

    override fun isEnabled(): Boolean {
        return true
    }
}
