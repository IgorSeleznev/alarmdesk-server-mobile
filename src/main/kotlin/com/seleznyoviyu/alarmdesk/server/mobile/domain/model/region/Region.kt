package com.seleznyoviyu.alarmdesk.server.mobile.domain.model.region

data class Region(
    val id: Long?,
    val title: String
)
