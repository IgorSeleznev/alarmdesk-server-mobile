package com.seleznyoviyu.alarmdesk.server.mobile.web.json.user

data class SimplifyUserAccountJson(
    val id: Long,
    val name: String
)
