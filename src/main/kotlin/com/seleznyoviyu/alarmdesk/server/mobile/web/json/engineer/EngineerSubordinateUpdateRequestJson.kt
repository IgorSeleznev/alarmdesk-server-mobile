package com.seleznyoviyu.alarmdesk.server.mobile.web.json.engineer

data class EngineerSubordinateUpdateRequestJson(
    val headEngineerId: Long?,
    val subordinateEngineerId: Long
)
