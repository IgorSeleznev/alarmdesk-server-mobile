package com.seleznyoviyu.alarmdesk.server.mobile.da.client.claim

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.claim.ClaimStatus
import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.claim.SimplifiedClaimEntity
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Component
import java.time.Instant

@Component
class SimplifiedClaimFindByStatusByEngineerIdClient(
    private val jdbcTemplate: NamedParameterJdbcTemplate
) {
    fun findSimplifiedByStatusByEngineerId(status: ClaimStatus, engineerId: Long): List<SimplifiedClaimEntity> {
        return jdbcTemplate.query(
            """
SELECT
    id,
    incident_type_title,
    incident_priority_title,
    location_name,
    status,
    author_name,
    engineer_name,
    created_datetime,
    comment
FROM claim
WHERE status = :status
  AND engineer_id = :engineerId
            """.trimIndent(),
            MapSqlParameterSource()
                .addValue("status", status)
                .addValue("engineerId", engineerId)
        ) { resultSet, _ ->
            SimplifiedClaimEntity(
                id = resultSet.getLong(1),
                incidentTypeTitle = resultSet.getString(2),
                incidentPriorityTitle = resultSet.getString(3),
                locationName = resultSet.getString(4),
                status = ClaimStatus.valueOf(
                    resultSet.getString(5)
                ),
                authorName = resultSet.getString(6),
                engineerName = resultSet.getString(7),
                createdDatetime = resultSet.getObject(8, Instant::class.java),
                comment = resultSet.getString(9)
            )
        }
    }
}