package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.claim

import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.user.UserAccountFindByIdService
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.ClaimCommentLog
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.UserAccountBrief
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.request.ClaimSaveService
import org.springframework.stereotype.Service
import java.time.Instant

@Service
class ClaimCommentUpdateService(
    private val findService: ClaimFindByIdService,
    private val saveService: ClaimSaveService,
    private val userService: UserAccountFindByIdService
) {
    fun updateComment(claimId: Long, comment: String, authorId: Long) {
        val source = findService.findById(claimId)
        val user = userService.findById(authorId)
        saveService.save(
            source.copy(
                commentLog = source.commentLog.toMutableList().apply {
                    add(
                        ClaimCommentLog(
                            datetime = Instant.now(),
                            author = UserAccountBrief(
                                userAccountId = authorId,
                                name = user.name,
                                login = user.login
                            ),
                            status = source.status,
                            comment = comment
                        )
                    )
                }
            )
        )
    }
}