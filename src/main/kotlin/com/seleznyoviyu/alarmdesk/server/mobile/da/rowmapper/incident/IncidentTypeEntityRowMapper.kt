package com.seleznyoviyu.alarmdesk.server.mobile.da.rowmapper.incident

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.incident.IncidentTypeEntity
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.IncidentPriorityType
import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Component
import java.sql.ResultSet

@Component
class IncidentTypeEntityRowMapper(
    private val objectMapper: ObjectMapper
) : RowMapper<IncidentTypeEntity> {
    override fun mapRow(resultSet: ResultSet, rowNum: Int): IncidentTypeEntity? {
        return IncidentTypeEntity(
            id = resultSet.getLong(1),
            title = resultSet.getString(2),
            parentId = resultSet.getLong(3),
            defaultPriority = IncidentPriorityType.valueOf(
                resultSet.getString(4)
            ),
            parameters = objectMapper.readValue(
                resultSet.getString(5)
            ),
            isEquipmentDescribe = resultSet.getBoolean(6),
            incidentResolveSla = resultSet.getInt(7),
            incidentReactionSla = resultSet.getInt(8),
            isLast = resultSet.getBoolean(9)
        )
    }
}