package com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident

data class SimplifiedIncidentType(
    val id: Long?,
    val parentId: Long?,
    val title: String,
    val isLast: Boolean,
    val parameters: List<SimplifiedIncidentTypeParameter>
)
