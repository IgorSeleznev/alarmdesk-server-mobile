package com.seleznyoviyu.alarmdesk.server.mobile.common.exception

class NotAllowedException(message: String) : RuntimeException(message) {
}