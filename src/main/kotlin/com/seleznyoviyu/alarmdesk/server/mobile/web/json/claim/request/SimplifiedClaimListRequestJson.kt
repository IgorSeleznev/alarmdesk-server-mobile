package com.seleznyoviyu.alarmdesk.server.mobile.web.json.claim.request

import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.claim.ClaimStatus

@JsonNaming(PropertyNamingStrategies.KebabCaseStrategy::class)
data class SimplifiedClaimListRequestJson(
    val status: ClaimStatus,
    val engineerId: Long
)