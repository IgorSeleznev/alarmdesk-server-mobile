package com.seleznyoviyu.alarmdesk.server.mobile.web.json.user

import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming

@JsonNaming(PropertyNamingStrategies.KebabCaseStrategy::class)
data class UserAccountRegionUpdateRequestJson(
    val userAccountId: Long,
    val regionId: Long
)
