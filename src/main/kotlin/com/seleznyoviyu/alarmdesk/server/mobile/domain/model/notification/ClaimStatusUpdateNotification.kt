package com.seleznyoviyu.alarmdesk.server.mobile.domain.model.notification

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.claim.ClaimStatus
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user.SimplifiedUserAccount
import java.time.Instant

data class ClaimStatusUpdateNotification(
    val claimId: Long,
    val statusUpdateAuthor: SimplifiedUserAccount,
    val statusUpdateDateTime: Instant,
    val previousStatus: ClaimStatus,
    val newStatus: ClaimStatus,
    val comment: String?
) : Notification