package com.seleznyoviyu.alarmdesk.server.mobile.web.controller.incident

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.BUSINESS_ADMIN
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.DEVELOPER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.ENGINEER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.HEAD_ENGINEER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.MOBILE_USER
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.OPERATOR
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.SYSTEM_ADMIN
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.user.UserRole.WEB_USER
import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.incident.IncidentTypeTitleListService
import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.incident.SimplifiedIncidentTypeListService
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.incident.IncidentTypeTitleJson
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.incident.SimplifiedIncidentTypeJson
import com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.incident.IncidentTypeTitleJsonMapper
import com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.incident.SimplifiedIncidentTypeJsonMapper
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.annotation.security.RolesAllowed

@RestController
@RequestMapping("/incident-type")
class IncidentTypeTitleListController(
    private val service: IncidentTypeTitleListService,
    private val mapper: IncidentTypeTitleJsonMapper
) {
    @RolesAllowed(
        "ROLE_$MOBILE_USER",
        "ROLE_$DEVELOPER",
        "ROLE_$OPERATOR",
        "ROLE_$ENGINEER",
        "ROLE_$SYSTEM_ADMIN",
        "ROLE_$BUSINESS_ADMIN",
        "ROLE_$WEB_USER",
        "ROLE_$HEAD_ENGINEER"
    )
    @GetMapping("/title-list-by-parent-simplified/{id}")
    fun incidentTypeListByParent(@PathVariable("id") id: Long): List<IncidentTypeTitleJson> {
        return service.listByParent(id).map(mapper::map)
    }

    @RolesAllowed(
        "ROLE_$MOBILE_USER",
        "ROLE_$DEVELOPER",
        "ROLE_$OPERATOR",
        "ROLE_$ENGINEER",
        "ROLE_$SYSTEM_ADMIN",
        "ROLE_$BUSINESS_ADMIN",
        "ROLE_$WEB_USER",
        "ROLE_$HEAD_ENGINEER"
    )
    @GetMapping("/title-list-root-simplified")
    fun rootIncidentTypeList(): List<IncidentTypeTitleJson> {
        return service.listRoot().map(mapper::map)
    }
}