package com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim

data class DetailedClaimIncidentTypeParameterValue(
    val title: String,
    val value: String?,
    val options: Map<Long, String>?
)
