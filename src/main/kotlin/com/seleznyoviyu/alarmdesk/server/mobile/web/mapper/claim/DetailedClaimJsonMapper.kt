package com.seleznyoviyu.alarmdesk.server.mobile.web.mapper.claim

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.DetailedClaim
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.claim.DetailedClaimJson
import org.mapstruct.InjectionStrategy
import org.mapstruct.Mapper

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
interface DetailedClaimJsonMapper {
    fun map(source: DetailedClaim): DetailedClaimJson
}