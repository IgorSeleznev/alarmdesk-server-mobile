package com.seleznyoviyu.alarmdesk.server.mobile.web.json.claim

import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.ClaimIncidentTypeParameterValue

@JsonNaming(PropertyNamingStrategies.KebabCaseStrategy::class)
data class ClaimCreateByMobileUserRequestJson(
    val incidentTypeId: Long,
    val incidentTypeParameterValues: Map<Long, ClaimIncidentTypeParameterValue>,
    val comment: String?
)