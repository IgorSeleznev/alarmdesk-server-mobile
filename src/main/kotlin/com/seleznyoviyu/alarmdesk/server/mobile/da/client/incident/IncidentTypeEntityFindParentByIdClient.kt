package com.seleznyoviyu.alarmdesk.server.mobile.da.client.incident

import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.incident.IncidentTypeEntity
import com.seleznyoviyu.alarmdesk.server.mobile.da.exception.NotFoundException
import com.seleznyoviyu.alarmdesk.server.mobile.da.rowmapper.incident.IncidentTypeEntityRowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Component

@Component
class IncidentTypeEntityFindParentByIdClient(
    private val jdbcTemplate: NamedParameterJdbcTemplate,
    private val rowMapper: IncidentTypeEntityRowMapper
) {
    fun findParentById(id: Long): IncidentTypeEntity {
        val result: List<IncidentTypeEntity> = jdbcTemplate.query(
            """
select
    pi.id,
    pi.title,
    pi.parent_incident_type_id,
    pi.default_incident_priority,
    pi.parameters,
    pi.is_equipment_describe,
    pi.incident_resolve_sla,
    pi.incident_reaction_sla,
    false as is_last
from incident_type i
inner join incident_type pi on pi.id = i.parent_incident_type_id
where i.id = :id
            """.trimIndent(),
            MapSqlParameterSource().addValue("id", id),
            rowMapper
        )

        if (result.isNotEmpty()) {
            return result[0]
        }
        throw NotFoundException("Отсутствует родительский тип инцидента для типа инцидента id = $id")
    }
}