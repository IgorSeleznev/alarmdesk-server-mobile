package com.seleznyoviyu.alarmdesk.server.mobile.web.json.refreshtoken

import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming

@JsonNaming(PropertyNamingStrategies.KebabCaseStrategy::class)
data class RefreshTokenRequestJson(
    val refreshToken: String
)
