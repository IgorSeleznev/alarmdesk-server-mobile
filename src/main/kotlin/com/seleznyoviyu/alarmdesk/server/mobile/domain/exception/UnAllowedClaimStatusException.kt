package com.seleznyoviyu.alarmdesk.server.mobile.domain.exception

import com.seleznyoviyu.alarmdesk.server.mobile.common.model.claim.ClaimStatus

class UnAllowedClaimStatusException(
    actualClaimStatus: ClaimStatus?,
    newClaimStatus: ClaimStatus?
) : RuntimeException(
    if (actualClaimStatus != null && newClaimStatus != null) {
        "Нельзя установить статус $newClaimStatus, потому что заявка находится в статусе $actualClaimStatus"
    } else if (actualClaimStatus == null && newClaimStatus == null) {
        "Недопустимое значение статуса для заявки"
    } else if (actualClaimStatus != null) {
        "Нельзя установить значение статуса $actualClaimStatus для заявки"
    } else {
        "Нельзя установить значение статуса $newClaimStatus для заявки"
    }
)