package com.seleznyoviyu.alarmdesk.server.mobile.da.client.region


import com.seleznyoviyu.alarmdesk.server.mobile.da.entity.region.RegionEntity
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Component

@Component
class RegionEntityListClient(
    private val jdbcTemplate: NamedParameterJdbcTemplate
) {
    fun listAll(): List<RegionEntity> {
        return jdbcTemplate.query(
            """
SELECT id, title
FROM region                
            """.trimIndent()
        ) { resultSet, _ ->
            RegionEntity(
                resultSet.getLong(1),
                resultSet.getString(2)
            )
        }
    }
}