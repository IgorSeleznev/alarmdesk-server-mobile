package com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.request

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.Claim
import com.seleznyoviyu.alarmdesk.server.mobile.da.client.claim.ClaimEntityInsertClient
import com.seleznyoviyu.alarmdesk.server.mobile.da.client.claim.ClaimEntityUpdateClient
import com.seleznyoviyu.alarmdesk.server.mobile.da.client.common.NextIdClient
import com.seleznyoviyu.alarmdesk.server.mobile.domain.mapper.claim.ClaimEntityMapper
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class ClaimSaveService(
    private val mapper: ClaimEntityMapper,
    private val updateClient: ClaimEntityUpdateClient,
    private val insertClient: ClaimEntityInsertClient,
    private val nextIdClient: NextIdClient
) {
    @Transactional
    fun save(claim: Claim) {
        val entity = mapper.map(claim)
        if (claim.id == null) {
            insertClient.insert(
                entity.copy(
                    id = nextIdClient.nextId("claim_seq")
                )
            )
        } else {
            updateClient.update(entity)
        }
    }
}