package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.claim

import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.incident.DefaultIncidentPriorityIdResolver
import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.location.UserLocationResolver
import com.seleznyoviyu.alarmdesk.server.mobile.domain.service.user.UserAccountFindByIdService
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.claim.ClaimStatus
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.Claim
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.ClaimCommentLog
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.ClaimStatusLog
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.UserAccountBrief
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.claim.request.ClaimCreateByMobileUserRequest
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.ClaimIncidentTypeParameterValue
import org.springframework.stereotype.Service
import java.time.Instant

@Service
class ClaimBuildByMobileService(
    private val locationResolver: UserLocationResolver,
    private val priorityResolver: DefaultIncidentPriorityIdResolver,
    private val userFindService: UserAccountFindByIdService
) {
    fun build(request: ClaimCreateByMobileUserRequest): Claim {
        val userAccount = userFindService.findById(request.authorId)
        val author = UserAccountBrief(
            userAccountId = request.authorId,
            name = userAccount.name,
            login = userAccount.login
        )
        return Claim(
            id = null,
            incidentTypeId = request.incidentTypeId,
            incidentTypeParameterValues = request.incidentTypeParameterValues.entries.associateBy(
                { it.key },
                {
                    ClaimIncidentTypeParameterValue(
                        it.value.value,
                        it.value.options
                    )
                }
            ),
            incidentPriority = priorityResolver.defaultPriority(request.incidentTypeId),
            locationId = locationResolver.resolve(request.authorId).id!!,
            status = ClaimStatus.CREATED,
            engineerId = null,
            authorId = request.authorId,
            statusUpdateAuthorId = request.authorId,
            createDateTime = Instant.now(),
            statusUpdateDateTime = Instant.now(),
            engineerAssignDateTime = null,
            finishDateTime = null,
            comment = request.comment,
            statusLog = listOf(
                ClaimStatusLog(
                    author = author,
                    datetime = Instant.now(),
                    status = ClaimStatus.CREATED
                )
            ),
            commentLog = listOf(
                ClaimCommentLog(
                    datetime = Instant.now(),
                    author = author,
                    status = ClaimStatus.CREATED,
                    comment = request.comment
                )
            ),
            technicalDetails = null
        )
    }
}