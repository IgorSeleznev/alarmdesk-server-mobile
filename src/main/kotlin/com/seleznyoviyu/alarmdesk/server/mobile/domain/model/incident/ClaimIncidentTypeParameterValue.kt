package com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident

data class ClaimIncidentTypeParameterValue(
    val value: String?,
    val options: Map<Long, String>?
)