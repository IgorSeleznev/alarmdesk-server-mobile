package com.seleznyoviyu.alarmdesk.server.mobile.domain.service.security

import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.security.AuthenticationResponse
import org.springframework.stereotype.Service
import java.util.*

@Service
class UserAccountSecurityTokensRefreshService(
    private val jwtTokenBuildService: JwtTokenBuildByRefreshTokenService,
    private val saveService: RefreshTokenSaveService,
    private val refreshTokenDeleteService: RefreshTokenDeleteService
) {
    fun refreshTokens(previousRefreshToken: String): AuthenticationResponse {
        val jwtBuildResult = jwtTokenBuildService.buildJwtToken(previousRefreshToken)
        val newRefreshToken = UUID.randomUUID()
        val result = AuthenticationResponse(
            accessToken = jwtBuildResult.jwtToken,
            refreshToken = newRefreshToken.toString()
        )
        saveService.save(
            refreshToken = newRefreshToken,
            userAccountExternalId = jwtBuildResult.userAccount.externalId!!
        )
        refreshTokenDeleteService.deleteRefreshToken(UUID.fromString(previousRefreshToken))
        return result
    }
}