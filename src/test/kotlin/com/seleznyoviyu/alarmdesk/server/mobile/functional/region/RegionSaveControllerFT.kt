package com.seleznyoviyu.alarmdesk.server.mobile.functional.region

import com.google.gson.GsonBuilder
import com.seleznyoviyu.alarmdesk.server.mobile.functional.AbstractFunctionalTestBase
import com.seleznyoviyu.alarmdesk.server.mobile.web.configuration.JwtTokenUtil.Companion.ACCESS_TOKEN_HEADER
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.region.RegionJson
import io.restassured.RestAssured.given
import io.restassured.http.ContentType.JSON
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import java.util.*

class RegionSaveControllerFT : AbstractFunctionalTestBase() {

    @Test
    fun regionCreateTest() {
        //val client = OkHttpClient.Builder().build()
        val expectedTitle = UUID.randomUUID().toString()
        var regionSaveRequest = RegionJson(
            id = null,
            title = expectedTitle
        )

        val accessToken = authenticate()

        var regionSaveResponse = given().contentType(JSON)
            .header(ACCESS_TOKEN_HEADER, "Bearer $accessToken")
            //.header(USER_HEADER_NAME, TEST_ADMIN_USER)
            .body(
                GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXX")
                    .create()
                    .toJson(regionSaveRequest)
            )
            .`when`()
            .baseUri("http://localhost")
            .port(BACKEND_PORT)
            .basePath("/region")
            .post("/save")
            .prettyPeek()
            .then()
            .extract()
            .jsonPath()
        //println(response)
        val regionSaveResponseBody = regionSaveResponse.getMap<String, Any>("")

        assertThat(regionSaveResponseBody).isNotNull()

        val regionId = regionSaveResponseBody["id"] as Int
        cacheId("region", regionId.toLong())

        val actualTitle = queryForObject(
            """select title from region where id = :id""",
            MapSqlParameterSource().addValue("id", regionId),
        ) { resultSet, _ ->
            resultSet.getString(1)
        }

        assertThat(actualTitle).isEqualTo(expectedTitle)
    }

    @Test
    fun regionUpdateTest() {
        //val client = OkHttpClient.Builder().build()
        val oldTitle = UUID.randomUUID().toString()
        val regionId = nextId("region_seq")
        update(
            """
INSERT INTO region (
    id, title
) VALUES (
    :id, :title
)                
            """.trimIndent(),
            MapSqlParameterSource()
                .addValue("id", regionId)
                .addValue("title", oldTitle)
        )

        val expectedTitle = UUID.randomUUID().toString()

        val accessToken = authenticate()

        val regionSaveRequest = RegionJson(
            id = regionId,
            title = expectedTitle
        )
        val regionSaveResponse = given().contentType(JSON)
            .header(ACCESS_TOKEN_HEADER, "Bearer $accessToken")
            //.header(USER_HEADER_NAME, TEST_ADMIN_USER)
            .body(
                GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXX")
                    .create()
                    .toJson(regionSaveRequest)
            )
            .`when`()
            .baseUri("http://localhost")
            .port(BACKEND_PORT)
            .basePath("/region")
            .post("/save")
            .prettyPeek()
            .then()
            .extract()
            .jsonPath()
        //println(response)
        val regionSaveResponseBody = regionSaveResponse.getMap<String, Any>("")

        assertThat(regionSaveResponseBody).isNotNull()

        val actualTitle = queryForObject(
            """select title from region where id = :id""",
            MapSqlParameterSource().addValue("id", regionId),
        ) { resultSet, _ ->
            resultSet.getString(1)
        }

        assertThat(actualTitle).isEqualTo(expectedTitle)
    }

}