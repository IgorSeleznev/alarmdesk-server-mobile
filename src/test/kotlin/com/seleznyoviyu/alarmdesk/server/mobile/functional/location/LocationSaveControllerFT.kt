package com.seleznyoviyu.alarmdesk.server.mobile.functional.location

import com.google.gson.GsonBuilder
import com.seleznyoviyu.alarmdesk.server.mobile.functional.AbstractFunctionalTestBase
import com.seleznyoviyu.alarmdesk.server.mobile.web.configuration.JwtTokenUtil.Companion.ACCESS_TOKEN_HEADER
import com.seleznyoviyu.alarmdesk.server.mobile.common.getObjectMapper
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.location.LocationJson
import io.restassured.RestAssured.given
import io.restassured.http.ContentType.JSON
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import java.util.*

class LocationSaveControllerFT : AbstractFunctionalTestBase() {

    @Test
    fun locationCreateTest() {
        //val client = OkHttpClient.Builder().build()
        val title = UUID.randomUUID().toString()
        val address = UUID.randomUUID().toString()
        val ukmLocation = Random().nextLong()
        val regionId: Long = 0
        var locationExpected = LocationJson(
            ukmLocation = ukmLocation,
            title = title,
            address = address,
            regionId = regionId,
            id = null
        )

        val accessToken = authenticate()

        var locationSaveResponse = given().contentType(JSON)
            .header(ACCESS_TOKEN_HEADER, "Bearer $accessToken")
            //.header(USER_HEADER_NAME, TEST_ADMIN_USER)
            .body(
                getObjectMapper().writeValueAsString(locationExpected)
                //GsonBuilder()
                //    .setFieldNamingPolicy(FieldNamingPolicy._WITH_UNDERSCORES)
                //    .create()
                //GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXX")
                //    .create()
                //    .toJson(locationExpected)
            )
            .`when`()
            .baseUri("http://localhost")
            .port(BACKEND_PORT)
            .basePath("/location")
            .post("/save")
            .prettyPeek()
            .then()
            .extract()
            .jsonPath()
        //println(response)
        val locationSaveResponseBody = locationSaveResponse.getMap<String, Object>("")

        assertThat(locationSaveResponseBody).isNotNull()

        val actualLocationId = locationSaveResponseBody["id"] as Int
        cacheId("location", actualLocationId.toLong())

        val locationActual = queryForObject(
            """
select id, title, ukm_location, region_id, address
from location 
where id = :id""".trimIndent(),
            MapSqlParameterSource().addValue("id", actualLocationId),
        ) { resultSet, _ ->
            LocationJson(
                id = resultSet.getLong(1),
                title = resultSet.getString(2),
                ukmLocation = resultSet.getLong(3),
                regionId = resultSet.getLong(4),
                address = resultSet.getString(5)
            )
        }

        assertThat(locationActual).isEqualTo(locationExpected.copy(id = actualLocationId.toLong()))
    }

    @Test
    fun locationUpdateTest() {
        //val client = OkHttpClient.Builder().build()
        val oldTitle = UUID.randomUUID().toString()
        val locationId = nextId("location_seq")
        val ukmLocation = Random().nextLong()
        val regionId: Long = 0
        val address = UUID.randomUUID().toString()
        update(
            """
INSERT INTO location (
    id, title, ukm_location, region_id, address
) VALUES (
    :id, :title, :ukmLocation, :regionId, :address
)                
            """.trimIndent(),
            MapSqlParameterSource()
                .addValue("id", locationId)
                .addValue("title", oldTitle)
                .addValue("ukmLocation", ukmLocation)
                .addValue("regionId", regionId)
                .addValue("address", address)
        )

        val expectedTitle = UUID.randomUUID().toString()

        val accessToken = authenticate()

        val locationSaveRequest = LocationJson(
            id = locationId,
            title = expectedTitle,
            ukmLocation = ukmLocation,
            regionId = regionId,
            address = address
        )
        val locationSaveResponse = given().contentType(JSON)
            .header(ACCESS_TOKEN_HEADER, "Bearer $accessToken")
            //.header(USER_HEADER_NAME, TEST_ADMIN_USER)
            .body(
                GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXX")
                    .create()
                    .toJson(locationSaveRequest)
            )
            .`when`()
            .baseUri("http://localhost")
            .port(BACKEND_PORT)
            .basePath("/location")
            .post("/save")
            .prettyPeek()
            .then()
            .extract()
            .jsonPath()
        //println(response)
        val locationSaveResponseBody = locationSaveResponse.getMap<String, Object>("")

        assertThat(locationSaveResponseBody).isNotNull()

        val actualTitle = queryForObject(
            """select title from location where id = :id""",
            MapSqlParameterSource().addValue("id", locationId),
        ) { resultSet, _ ->
            resultSet.getString(1)
        }

        assertThat(actualTitle).isEqualTo(expectedTitle)

        //assertThat(body).containsKey("accessToken")
        //assertThat(body).containsKey("refreshToken")
        //assertThat(body["accessToken"]).isNotNull()
        //assertThat(body["refreshToken"]).isNotNull()
    }

}