package com.seleznyoviyu.alarmdesk.server.mobile.functional.authentication

import com.google.gson.GsonBuilder
import com.seleznyoviyu.alarmdesk.server.mobile.functional.AbstractFunctionalTestBase
import com.seleznyoviyu.alarmdesk.server.mobile.web.configuration.JwtTokenUtil
import com.seleznyoviyu.alarmdesk.server.mobile.common.getObjectMapper
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.mobile.authenticate.AuthenticateRequestJson
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.user.UserAccountSaveRequestJson
import io.restassured.RestAssured
import io.restassured.RestAssured.given
import io.restassured.http.ContentType
import io.restassured.http.ContentType.JSON
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import java.util.*

class AuthenticationControllerFT : AbstractFunctionalTestBase() {

    @Test
    fun authenticateTest() {
        //val client = OkHttpClient.Builder().build()
        val request = AuthenticateRequestJson(
            login = "test",
            password = "test"
        )
        val response = given().contentType(JSON)
            //.header(ROLE_HEADER_NAME, TEST_ADMIN)
            //.header(USER_HEADER_NAME, TEST_ADMIN_USER)
            .body(
                GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXX")
                    .create()
                    .toJson(request)
            )
            .`when`()
            .baseUri("http://localhost")
            .port(BACKEND_PORT)
            .basePath("/authenticate")
            .post("")
            .prettyPeek()
            .then()
            .extract()
            .jsonPath()
        //println(response)
        val body = response.getMap<String, Any>("")
        assertThat(body).isNotNull()
        assertThat(body).containsKey("accessToken")
        assertThat(body).containsKey("refreshToken")
        assertThat(body["accessToken"]).isNotNull()
        assertThat(body["refreshToken"]).isNotNull()
    }

    @Test
    fun authenticateWithWrongPasswordTest() {
        //val client = OkHttpClient.Builder().build()
        val request = AuthenticateRequestJson(
            login = "test",
            password = "dev"
        )
        val response = given().contentType(JSON)
            //.header(ROLE_HEADER_NAME, TEST_ADMIN)
            //.header(USER_HEADER_NAME, TEST_ADMIN_USER)
            .body(
                GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXX")
                    .create()
                    .toJson(request)
            )
            .`when`()
            .baseUri("http://localhost")
            .port(BACKEND_PORT)
            .basePath("/authenticate")
            .post("")
        //println(response)

        assertThat(response.statusCode).isEqualTo(401)
        assertThat(response.body.asString()).isEmpty()
    }

    @Test
    fun authenticateWithWrongLoginTest() {
        //val client = OkHttpClient.Builder().build()
        val request = AuthenticateRequestJson(
            login = "test",
            password = "dev"
        )
        val response = given().contentType(JSON)
            //.header(ROLE_HEADER_NAME, TEST_ADMIN)
            //.header(USER_HEADER_NAME, TEST_ADMIN_USER)
            .body(
                GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXX")
                    .create()
                    .toJson(request)
            )
            .`when`()
            .baseUri("http://localhost")
            .port(BACKEND_PORT)
            .basePath("/authenticate")
            .post("")
        //println(response)

        assertThat(response.statusCode).isEqualTo(401)
        assertThat(response.body.asString()).isEmpty()
    }

    @Test
    fun wrongRoleAccessFailedTest() {
        val name = UUID.randomUUID().toString()
        val login = UUID.randomUUID().toString()
        val password = UUID.randomUUID().toString()
        val passwordHash = BCryptPasswordEncoder().encode(password)
        val role = "MOBILE_USER"
        val regionId: Long = 0
        val id: Long = nextId("user_account_seq")
        val externalId = UUID.randomUUID()

        update(
            """
INSERT INTO user_account (
    id,
    name,
    login,
    password_hash,
    role,
    region_id,
    external_id
) VALUES (
    :id,
    :name,
    :login,
    :passwordHash,
    :role,
    :regionId,
    :externalId
)                
            """.trimIndent(),
            MapSqlParameterSource()
                .addValue("id", id)
                .addValue("name", name)
                .addValue("login", login)
                .addValue("passwordHash", passwordHash)
                .addValue("role", role)
                .addValue("regionId", regionId)
                .addValue("externalId", externalId)
        )

        val accessToken = authenticate(
            AuthenticateRequestJson(
                login = login,
                password = password
            )
        )

        val expectedName = UUID.randomUUID().toString()
        val updatedPassword = UUID.randomUUID().toString()

        val userAccountSaveRequest = UserAccountSaveRequestJson(
            id = id,
            name = expectedName,
            login = login,
            password = updatedPassword,
            role = role,
            regionId = regionId
        )

        val response = RestAssured.given().contentType(ContentType.JSON)
            .header(JwtTokenUtil.ACCESS_TOKEN_HEADER, "Bearer $accessToken")
            //.header(USER_HEADER_NAME, TEST_ADMIN_USER)
            .body(
                getObjectMapper().writeValueAsString(userAccountSaveRequest)
                //GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXX")
                //    .create()
                //    .toJson(userAccountSaveRequest)
            )
            .`when`()
            .baseUri("http://localhost")
            .port(BACKEND_PORT)
            .basePath("/user-account")
            .post("/save")
        //println(response)


        assertThat(response.statusCode).isEqualTo(403)
        //val body = response.body.jsonPath().getMap<String, Any>("")
            //assertThat(body[]).isEqualTo(403)

    }
}