package com.seleznyoviyu.alarmdesk.server.mobile.functional.incident.parameter

import com.seleznyoviyu.alarmdesk.server.mobile.functional.AbstractFunctionalTestBase
import com.seleznyoviyu.alarmdesk.server.mobile.web.configuration.JwtTokenUtil
import com.seleznyoviyu.alarmdesk.server.mobile.common.getObjectMapper
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.user.UserAccount
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.user.UserAccountSaveRequestJson
import io.restassured.RestAssured
import io.restassured.http.ContentType
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import java.util.*

class IncidentParameterListControllerFT : AbstractFunctionalTestBase() {

    @Test
    fun incidentParameterListTest() {
        //val client = OkHttpClient.Builder().build()
        val name = UUID.randomUUID().toString()
        val login = UUID.randomUUID().toString()
        val password = UUID.randomUUID().toString()
        val role = "MOBILE_USER"
        val regionId: Long = 0

        val userAccountSaveRequest = UserAccountSaveRequestJson(
            id = null,
            name = name,
            login = login,
            password = password,
            role = role,
            regionId = regionId
        )

        val accessToken = authenticate()

        val userAccountSaveResponse = RestAssured.given().contentType(ContentType.JSON)
            .header(JwtTokenUtil.ACCESS_TOKEN_HEADER, "Bearer $accessToken")
            //.header(USER_HEADER_NAME, TEST_ADMIN_USER)
            .body(
                getObjectMapper().writeValueAsString(userAccountSaveRequest)
                //GsonBuilder()
                //    .setFieldNamingPolicy(FieldNamingPolicy._WITH_UNDERSCORES)
                //    .create()
                //GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXX")
                //    .create()
                //    .toJson(locationExpected)
            )
            .`when`()
            .baseUri("http://localhost")
            .port(BACKEND_PORT)
            .basePath("/user-account")
            .post("/save")
            .prettyPeek()
            .then()
            .extract()
            .jsonPath()
        //println(response)
        val userAccountSaveResponseBody = userAccountSaveResponse.getMap<String, Object>("")

        Assertions.assertThat(userAccountSaveResponseBody).isNotNull()

        val userAccountId = userAccountSaveResponseBody["id"] as Int

        val userAccountActual = queryForObject(
            """
SELECT 
    id,
    name,
    login,
    password_hash,
    role,
    region_id,
    external_id
FROM user_account
WHERE id = :id
            """.trimIndent(),
            MapSqlParameterSource().addValue("id", userAccountId),
        ) { resultSet, _ ->
            UserAccount(
                id = resultSet.getLong(1),
                name = resultSet.getString(2),
                login = resultSet.getString(3),
                passwordHash = resultSet.getString(4),
                role = resultSet.getString(5),
                regionId = resultSet.getLong(6),
                externalId = resultSet.getObject(7, UUID::class.java)
            )
        }

        Assertions.assertThat(userAccountActual!!.name).isEqualTo(userAccountSaveRequest.name)
        Assertions.assertThat(userAccountActual.login).isEqualTo(userAccountSaveRequest.login)
        Assertions.assertThat(
            BCryptPasswordEncoder(10).matches(userAccountSaveRequest.password, userAccountActual.passwordHash)
        ).isTrue()
        Assertions.assertThat(userAccountActual.role).isEqualTo(userAccountSaveRequest.role)
        Assertions.assertThat(userAccountActual.regionId).isEqualTo(userAccountSaveRequest.regionId)
    }

}