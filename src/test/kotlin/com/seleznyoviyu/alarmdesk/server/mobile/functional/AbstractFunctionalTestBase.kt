package com.seleznyoviyu.alarmdesk.server.mobile.functional

import com.google.gson.GsonBuilder
import com.seleznyoviyu.alarmdesk.server.mobile.common.getObjectMapper
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.mobile.authenticate.AuthenticateRequestJson
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import io.restassured.RestAssured
import io.restassured.http.ContentType
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.jdbc.core.namedparam.SqlParameterSource
import java.util.*

abstract class AbstractFunctionalTestBase {

    private val properties = Properties()
    private val cachedIds = mutableMapOf<String, MutableList<Long>>()
    private val objectMapper = getObjectMapper()

    private lateinit var jdbcTemplate: NamedParameterJdbcTemplate

    constructor() {
        properties.load(this::class.java.classLoader.getResourceAsStream("application-test.properties"))
    }

    companion object {
        const val BACKEND_PORT = 9690
        const val POSTGRESQL_PORT = 5432
        const val MYSQL_PORT = 3306
    }

    @BeforeEach
    fun before() {
        jdbcTemplate = jdbcTemplate()
    }

    @AfterEach
    fun after() {
        for (entry in cachedIds) {
            val tableName = entry.key
            val sql = """
DELETE FROM $tableName WHERE id = :id                     
                """.trimIndent()
            for (id in entry.value) {
                update(
                    sql,
                    MapSqlParameterSource().addValue("id", id)
                )
            }
        }
    }

    fun authenticate(): String {
        return authenticate(
            AuthenticateRequestJson(
                login = "test",
                password = "test"
            )
        )
    }

    fun authenticate(authenticateRequest: AuthenticateRequestJson): String {
        val authenticateResponse = RestAssured.given().contentType(ContentType.JSON)
            //.header(ROLE_HEADER_NAME, TEST_ADMIN)
            //.header(USER_HEADER_NAME, TEST_ADMIN_USER)
            .body(
                GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXX")
                    .create()
                    .toJson(authenticateRequest)
            )
            .`when`()
            .baseUri("http://localhost")
            .port(BACKEND_PORT)
            .basePath("/authenticate")
            .post("")
            .prettyPeek()
            .then()
            .extract()
            .jsonPath()
        //println(response)
        val authenticateResponseBody = authenticateResponse.getMap<String, Object>("")
        return authenticateResponseBody["accessToken"]!!.toString()
    }

    fun cacheId(tableName: String, id: Long) {
        cachedIds.getOrPut(tableName) { mutableListOf() }.add(id)
    }

    private fun jdbcTemplate(): NamedParameterJdbcTemplate {
        return NamedParameterJdbcTemplate(
            HikariDataSource(
                hikariConfig()
            )
        )
    }

    fun nextId(sequenceName: String): Long {
        val id = queryForObject(
            """
SELECT nextval('$sequenceName')                
            """.trimIndent()
        ) { resultSet, _ ->
            resultSet.getLong(1)
        }!!
        cachedIds.getOrPut(sequenceName.removeSuffix("_seq")) { mutableListOf() }.add(id)

        return id
    }

    fun update(query: String, parameterSource: SqlParameterSource) {
        jdbcTemplate.update(
            query,
            parameterSource
        )
    }

    fun <T> queryForObject(query: String, parameterSource: SqlParameterSource, rowMapper: RowMapper<T>): T? {
        return jdbcTemplate.queryForObject(
            query,
            parameterSource,
            rowMapper
        )
    }

    fun <T> query(query: String, parameterSource: SqlParameterSource, rowMapper: RowMapper<T>): List<T> {
        return jdbcTemplate.query(
            query,
            parameterSource,
            rowMapper
        )
    }

    fun update(query: String) {
        jdbcTemplate.update(
            query,
            MapSqlParameterSource()
        )
    }

    fun <T> queryForObject(query: String, rowMapper: RowMapper<T>): T? {
        return jdbcTemplate.queryForObject(
            query,
            MapSqlParameterSource(),
            rowMapper
        )
    }

    fun <T> query(query: String, rowMapper: RowMapper<T>): List<T> {
        return jdbcTemplate.query(
            query,
            rowMapper
        )
    }

    private fun hikariConfig(): HikariConfig {
        val config = HikariConfig();
//        dataSource.setDriverClassName(environment.getProperty(DB_DRIVERNAME_PROPERTY));
//        dataSource.setUrl(environment.getProperty(DB_URL_PROPERTY));
//        dataSource.setUsername(environment.getProperty(DB_USERNAME_PROPERTY));
//        dataSource.setPassword(environment.getProperty(DB_PASSWORD_PROPERTY));

        config.poolName = "postgresConnectionPool"
        config.connectionTestQuery = "select 1"
        config.driverClassName = "org.postgresql.Driver"
        //config.setDataSourceClassName("postgresDataSource")
        config.jdbcUrl = properties.getProperty("spring.datasource.url")
        config.username = properties.getProperty("spring.datasource.username")
        config.password = properties.getProperty("spring.datasource.password")
        //config.addDataSourceProperty("characterEncoding","utf8");
        config.addDataSourceProperty("encoding", "UTF-8")
        config.addDataSourceProperty("useUnicode", "true")

        return config
    }

    fun objectMapper() = this.objectMapper
}