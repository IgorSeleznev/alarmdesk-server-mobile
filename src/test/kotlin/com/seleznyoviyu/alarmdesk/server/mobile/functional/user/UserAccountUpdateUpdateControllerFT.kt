package com.seleznyoviyu.alarmdesk.server.mobile.functional.user

import com.seleznyoviyu.alarmdesk.server.mobile.functional.AbstractFunctionalTestBase
import com.seleznyoviyu.alarmdesk.server.mobile.web.configuration.JwtTokenUtil
import com.seleznyoviyu.alarmdesk.server.mobile.common.getObjectMapper
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.user.UserAccountRegionUpdateRequestJson
import io.restassured.RestAssured
import io.restassured.http.ContentType
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import java.util.*

class UserAccountUpdateUpdateControllerFT : AbstractFunctionalTestBase() {

    @Test
    fun updateRegion() {
        val name = UUID.randomUUID().toString()
        val login = UUID.randomUUID().toString()
        val password = UUID.randomUUID().toString()
        val passwordHash = BCryptPasswordEncoder().encode(password)
        val role = "MOBILE_USER"
        val regionId: Long = 0
        val userAccountId: Long = nextId("user_account_seq")
        val externalId = UUID.randomUUID()

        update(
            """
INSERT INTO user_account (
    id,
    name,
    login,
    password_hash,
    role,
    region_id,
    external_id
) VALUES (
    :id,
    :name,
    :login,
    :passwordHash,
    :role,
    :regionId,
    :externalId
)                
            """.trimIndent(),
            MapSqlParameterSource()
                .addValue("id", userAccountId)
                .addValue("name", name)
                .addValue("login", login)
                .addValue("passwordHash", passwordHash)
                .addValue("role", role)
                .addValue("regionId", regionId)
                .addValue("externalId", externalId)
        )

        val accessToken = authenticate()

        val expectedRegionId: Long = 999999

        val request = UserAccountRegionUpdateRequestJson(
            userAccountId = userAccountId,
            regionId = expectedRegionId
        )

        val userAccountSaveResponse = RestAssured.given().contentType(ContentType.JSON)
            .header(JwtTokenUtil.ACCESS_TOKEN_HEADER, "Bearer $accessToken")
            //.header(USER_HEADER_NAME, TEST_ADMIN_USER)
            .body(
                getObjectMapper().writeValueAsString(request)
                //GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXX")
                //    .create()
                //    .toJson(userAccountSaveRequest)
            )
            .`when`()
            .baseUri("http://localhost")
            .port(BACKEND_PORT)
            .basePath("/user-account")
            .put("/update-region")
            .prettyPeek()
            .then()
            .extract()
            .jsonPath()
        //println(response)
//        val userAccountRegionUpdateResponseBody = userAccountSaveResponse.getMap<String, Object>("")

//        Assertions.assertThat(userAccountRegionUpdateResponseBody).isNotNull()

        val actualRegionId = queryForObject(
            """
SELECT region_id
FROM user_account
WHERE id = :id                
            """.trimIndent(),
            MapSqlParameterSource().addValue("id", userAccountId)
        ) { resultSet, _ ->
            resultSet.getLong(1)
        }

        Assertions.assertThat(actualRegionId).isEqualTo(expectedRegionId)
    }
}