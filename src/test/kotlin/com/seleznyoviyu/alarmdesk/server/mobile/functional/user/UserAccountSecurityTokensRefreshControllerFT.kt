package com.seleznyoviyu.alarmdesk.server.mobile.functional.user

import com.google.gson.GsonBuilder
import com.seleznyoviyu.alarmdesk.server.mobile.functional.AbstractFunctionalTestBase
import com.seleznyoviyu.alarmdesk.server.mobile.web.configuration.JwtTokenUtil
import com.seleznyoviyu.alarmdesk.server.mobile.common.getObjectMapper
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.mobile.authenticate.AuthenticateRequestJson
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.refreshtoken.RefreshTokenRequestJson
import io.restassured.RestAssured
import io.restassured.http.ContentType
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class UserAccountSecurityTokensRefreshControllerFT : AbstractFunctionalTestBase() {

    @Test
    fun securityTokensRefreshTest() {
        val request = AuthenticateRequestJson(
            login = "test",
            password = "test"
        )
        val response = RestAssured.given().contentType(ContentType.JSON)
            //.header(ROLE_HEADER_NAME, TEST_ADMIN)
            //.header(USER_HEADER_NAME, TEST_ADMIN_USER)
            .body(
                GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXX")
                    .create()
                    .toJson(request)
            )
            .`when`()
            .baseUri("http://localhost")
            .port(BACKEND_PORT)
            .basePath("/authenticate")
            .post("")
            .prettyPeek()
            .then()
            .extract()
            .jsonPath()
        //println(response)
        val body = response.getMap<String, Any>("")
        val accessToken = body["accessToken"].toString()
        val refreshToken = body["refreshToken"].toString()

        val refreshRequest = RefreshTokenRequestJson(
            refreshToken = refreshToken
        )

        val refreshResponse = RestAssured.given().contentType(ContentType.JSON)
            .header(JwtTokenUtil.ACCESS_TOKEN_HEADER, "Bearer $accessToken")
            //.header(ROLE_HEADER_NAME, TEST_ADMIN)
            //.header(USER_HEADER_NAME, TEST_ADMIN_USER)
            .body(
                getObjectMapper().writeValueAsString(refreshRequest)
                //GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXX")
                //    .create()
                //    .toJson(refreshRequest)
            )
            .`when`()
            .baseUri("http://localhost")
            .port(BACKEND_PORT)
            .basePath("/user-account")
            .post("/refresh-token")
            .prettyPeek()
            .then()
            .extract()
            .jsonPath()
        val refreshResponseBody = refreshResponse.getMap<String, Any>("")

        assertThat(refreshResponseBody).isNotNull()
        assertThat(refreshResponseBody).containsKey("accessToken")
        assertThat(refreshResponseBody).containsKey("refreshToken")

        val newRefreshToken = refreshResponseBody["refreshToken"]
        assertThat(newRefreshToken).isNotEqualTo(refreshToken)
    }

    @Test
    fun oldRefreshTokenIsExpiredTest() {
        val request = AuthenticateRequestJson(
            login = "test",
            password = "test"
        )
        val response = RestAssured.given().contentType(ContentType.JSON)
            //.header(ROLE_HEADER_NAME, TEST_ADMIN)
            //.header(USER_HEADER_NAME, TEST_ADMIN_USER)
            .body(
                GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXX")
                    .create()
                    .toJson(request)
            )
            .`when`()
            .baseUri("http://localhost")
            .port(BACKEND_PORT)
            .basePath("/authenticate")
            .post("")
            .prettyPeek()
            .then()
            .extract()
            .jsonPath()
        //println(response)
        val body = response.getMap<String, Any>("")
        val accessToken = body["accessToken"].toString()
        val refreshToken = body["refreshToken"].toString()

        val refreshRequest = RefreshTokenRequestJson(
            refreshToken = refreshToken
        )

        val refreshResponse = RestAssured.given().contentType(ContentType.JSON)
            .header(JwtTokenUtil.ACCESS_TOKEN_HEADER, "Bearer $accessToken")
            //.header(ROLE_HEADER_NAME, TEST_ADMIN)
            //.header(USER_HEADER_NAME, TEST_ADMIN_USER)
            .body(
                getObjectMapper().writeValueAsString(refreshRequest)
                //GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXX")
                //    .create()
                //    .toJson(refreshRequest)
            )
            .`when`()
            .baseUri("http://localhost")
            .port(BACKEND_PORT)
            .basePath("/user-account")
            .post("refresh-token")
            .prettyPeek()
            .then()
            .extract()
            .jsonPath()
        val refreshResponseBody = refreshResponse.getMap<String, Any>("")

        assertThat(refreshResponseBody).isNotNull()
        assertThat(refreshResponseBody).containsKey("accessToken")
        assertThat(refreshResponseBody).containsKey("refreshToken")

        val secondTimeUsedrefreshTokenResponse = RestAssured.given().contentType(ContentType.JSON)
            .header(JwtTokenUtil.ACCESS_TOKEN_HEADER, "Bearer $accessToken")
            //.header(ROLE_HEADER_NAME, TEST_ADMIN)
            //.header(USER_HEADER_NAME, TEST_ADMIN_USER)
            .body(
                getObjectMapper().writeValueAsString(refreshRequest)
                //GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXX")
                //    .create()
                //    .toJson(refreshRequest)
            )
            .`when`()
            .baseUri("http://localhost")
            .port(BACKEND_PORT)
            .basePath("/user-account")
            .post("refresh-token")

        assertThat(secondTimeUsedrefreshTokenResponse.statusCode).isEqualTo(403)

        //val newRefreshToken = refreshResponseBody["refreshToken"]
        //assertThat(newRefreshToken).isNotEqualTo(refreshToken)
    }

    @Test
    fun secondRefreshTokenIsWorksTest() {
        val request = AuthenticateRequestJson(
            login = "test",
            password = "test"
        )
        val response = RestAssured.given().contentType(ContentType.JSON)
            //.header(ROLE_HEADER_NAME, TEST_ADMIN)
            //.header(USER_HEADER_NAME, TEST_ADMIN_USER)
            .body(
                GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXX")
                    .create()
                    .toJson(request)
            )
            .`when`()
            .baseUri("http://localhost")
            .port(BACKEND_PORT)
            .basePath("/authenticate")
            .post("")
            .prettyPeek()
            .then()
            .extract()
            .jsonPath()
        //println(response)
        val body = response.getMap<String, Any>("")
        val accessToken = body["accessToken"].toString()
        val refreshToken = body["refreshToken"].toString()

        val refreshRequest = RefreshTokenRequestJson(
            refreshToken = refreshToken
        )

        val refreshResponse = RestAssured.given().contentType(ContentType.JSON)
            .header(JwtTokenUtil.ACCESS_TOKEN_HEADER, "Bearer $accessToken")
            //.header(ROLE_HEADER_NAME, TEST_ADMIN)
            //.header(USER_HEADER_NAME, TEST_ADMIN_USER)
            .body(
                getObjectMapper().writeValueAsString(refreshRequest)
                //GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXX")
                //    .create()
                //    .toJson(refreshRequest)
            )
            .`when`()
            .baseUri("http://localhost")
            .port(BACKEND_PORT)
            .basePath("/user-account")
            .post("refresh-token")
            .prettyPeek()
            .then()
            .extract()
            .jsonPath()
        val refreshResponseBody = refreshResponse.getMap<String, Any>("")

        assertThat(refreshResponseBody).isNotNull()
        assertThat(refreshResponseBody).containsKey("accessToken")
        assertThat(refreshResponseBody).containsKey("refreshToken")

        val firstTimeRefreshToken = refreshResponseBody["refreshToken"]
        assertThat(firstTimeRefreshToken).isNotEqualTo(refreshToken)

        val secondTimeRefreshRequest = RefreshTokenRequestJson(
            refreshToken = firstTimeRefreshToken.toString()
        )

        val secondTimeUsedrefreshTokenResponse = RestAssured.given().contentType(ContentType.JSON)
            .header(JwtTokenUtil.ACCESS_TOKEN_HEADER, "Bearer $accessToken")
            //.header(ROLE_HEADER_NAME, TEST_ADMIN)
            //.header(USER_HEADER_NAME, TEST_ADMIN_USER)
            .body(
                getObjectMapper().writeValueAsString(secondTimeRefreshRequest)
                //GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXX")
                //    .create()
                //    .toJson(refreshRequest)
            )
            .`when`()
            .baseUri("http://localhost")
            .port(BACKEND_PORT)
            .basePath("/user-account")
            .post("refresh-token")
            .prettyPeek()
            .then()
            .extract()
            .jsonPath()
        val secondTimeRefreshResponseBody = secondTimeUsedrefreshTokenResponse.getMap<String, Any>("")

        assertThat(refreshResponseBody).isNotNull()
        assertThat(refreshResponseBody).containsKey("accessToken")
        assertThat(refreshResponseBody).containsKey("refreshToken")

        val secondTimeRefreshToken = secondTimeRefreshResponseBody["refreshToken"]
        assertThat(secondTimeRefreshToken).isNotEqualTo(firstTimeRefreshToken)
        assertThat(secondTimeRefreshToken).isNotEqualTo(refreshToken)
    }
}