package com.seleznyoviyu.alarmdesk.server.mobile.functional.incident

import com.seleznyoviyu.alarmdesk.server.mobile.functional.AbstractFunctionalTestBase
import com.seleznyoviyu.alarmdesk.server.mobile.web.configuration.JwtTokenUtil
import com.seleznyoviyu.alarmdesk.server.mobile.common.model.incident.IncidentValueType
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.IncidentPriorityType
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.IncidentTypeParameter
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.incident.IncidentTypeJson
import io.restassured.RestAssured
import io.restassured.http.ContentType
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import java.util.*
import kotlin.random.Random

class SimplifiedIncidentTypeListControllerFT : AbstractFunctionalTestBase() {

    @Test
    fun simplifiedIncidentTypeListByRootTest() {
        val count = Random.nextInt(3, 12)
        val list = mutableListOf<IncidentTypeJson>()
        for (i in 1..count) {
            list.add(
                incidentType()
            )
            if (Random.nextBoolean()) {
                val childCount = Random.nextInt(3, 12)
                for (j in 1..childCount) {
                    incidentType(
                        list[list.size - 1].id!!
                    )
                }
            }
        }

        val accessToken = authenticate()

        val incidentTypeListResponse = RestAssured.given().contentType(ContentType.JSON)
            .header(JwtTokenUtil.ACCESS_TOKEN_HEADER, "Bearer $accessToken")
            //.header(USER_HEADER_NAME, TEST_ADMIN_USER)
            .`when`()
            .baseUri("http://localhost")
            .port(BACKEND_PORT)
            .basePath("/incident-type")
            .get("/list-root-simplified")
            .prettyPeek()
            .then()
            .extract()
            .jsonPath()
        //println(response)
        val incidentTypeListResponseBody = incidentTypeListResponse.getList<MutableMap<String, Any>>("")

        Assertions.assertThat(incidentTypeListResponseBody).hasSize(list.size)
        Assertions.assertThat(incidentTypeListResponseBody).containsAll(
            list.map {
                mutableMapOf(
                    Pair("id", it.id!!.toInt()),
                    Pair("parent_incident_type_id", it.parentId!!.toInt()),
                    Pair("title", it.title),
                    Pair("default_incident_priority", it.defaultPriority.toString()),
                    Pair("parameters", it.parameters),
                    Pair("is_equipment_describe", it.isEquipmentDescribe),
                    Pair("incident_resolve_sla", it.incidentResolveSla),
                    Pair("incident_reaction_sla", it.incidentReactionSla)
                )
            }
        )
    }

    private fun incidentType(): IncidentTypeJson {
        return incidentType(null)
    }

    private fun incidentType(parentId: Long?): IncidentTypeJson {
        val title = UUID.randomUUID().toString()
        val incidentTypeId = nextId("incident_type_seq")
        val count = Random.nextInt(3, 12)
        val incidentTypeParameters = mutableListOf<IncidentTypeParameter>()
        val priority = IncidentPriorityType.values()[
                Random.nextInt(IncidentPriorityType.values().size)
        ]
        for (i in 1..count) {
            incidentTypeParameters.add(
                incidentTypeParameter()
            )
        }
        val isEquipmentDescribe = Random.nextBoolean()
        val incidentResolveSla = Random.nextInt()
        val incidentReactionSla = Random.nextInt()

        update(
            """
INSERT INTO incident_type (
     id,
     parent_incident_type_id,
     title, 
     default_incident_priority,
     parameters,
     is_equipment_describe,
     incident_resolve_sla,
     incident_reaction_sla
) VALUES (
     :id,
     :parent_incident_type_id,
     :title, 
     :default_incident_priority,
     :parameters::jsonb,
     :is_equipment_describe,
     :incident_resolve_sla,
     :incident_reaction_sla
)                
            """.trimIndent(),
            MapSqlParameterSource()
                .addValue("id", incidentTypeId)
                .addValue("parent_incident_type_id", parentId)
                .addValue("title", title)
                .addValue("default_incident_priority", priority.toString())
                .addValue("parameters", objectMapper().writeValueAsString(incidentTypeParameters))
                .addValue("is_equipment_describe", isEquipmentDescribe)
                .addValue("incident_resolve_sla", incidentResolveSla)
                .addValue("incident_reaction_sla", incidentReactionSla)
        )
        return IncidentTypeJson(
            id = incidentTypeId,
            title = title,
            parentId = parentId,
            isLast = true,
            defaultPriority = priority,
            isEquipmentDescribe = isEquipmentDescribe,
            parameters = incidentTypeParameters,
            incidentResolveSla = incidentResolveSla,
            incidentReactionSla = incidentReactionSla
        )
    }

    private fun incidentTypeParameter(): IncidentTypeParameter {
        return IncidentTypeParameter(
            id = Random.nextLong(),
            title = UUID.randomUUID().toString(),
            incidentValueType = IncidentValueType.values()[
                    Random.nextInt(
                        IncidentValueType.values().size
                    )
            ],
            sortOrder = Random.nextInt(),
            options = mutableMapOf()
        )
    }
}