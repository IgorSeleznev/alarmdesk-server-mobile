package com.seleznyoviyu.alarmdesk.server.mobile.functional.incident.priority

import com.seleznyoviyu.alarmdesk.server.mobile.functional.AbstractFunctionalTestBase
import com.seleznyoviyu.alarmdesk.server.mobile.web.configuration.JwtTokenUtil
import com.seleznyoviyu.alarmdesk.server.mobile.domain.model.incident.IncidentPriorityType
import io.restassured.RestAssured
import io.restassured.http.ContentType
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class IncidentPriorityListControllerFT : AbstractFunctionalTestBase() {

    @Test
    fun incidentPriorityListTest() {
        val accessToken = authenticate()

        val incidentPriorityListResponse = RestAssured.given().contentType(ContentType.JSON)
            .header(JwtTokenUtil.ACCESS_TOKEN_HEADER, "Bearer $accessToken")
            //.header(USER_HEADER_NAME, TEST_ADMIN_USER)
            .`when`()
            .baseUri("http://localhost")
            .port(BACKEND_PORT)
            .basePath("/incident-type")
            .get("/priority/list")
            .prettyPeek()
            .then()
            .extract()
            .jsonPath()
        //println(response)
        val incidentPriorityListResponseBody = incidentPriorityListResponse.getList<Map<String, Object>>("")

        assertThat(incidentPriorityListResponseBody).isNotNull()
        assertThat(incidentPriorityListResponseBody).hasSize(
            IncidentPriorityType.values().size
        )

        for (map in incidentPriorityListResponseBody) {
            assertThat(IncidentPriorityType.check(map["name"].toString())).isTrue()
            assertThat(
                IncidentPriorityType.valueOf(map["name"].toString()).title
            ).isEqualTo(map["title"])
        }
    }
}