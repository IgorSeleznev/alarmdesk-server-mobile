package com.seleznyoviyu.alarmdesk.server.mobile.functional.region

import com.seleznyoviyu.alarmdesk.server.mobile.functional.AbstractFunctionalTestBase
import com.seleznyoviyu.alarmdesk.server.mobile.web.configuration.JwtTokenUtil
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.region.RegionJson
import io.restassured.RestAssured
import io.restassured.http.ContentType
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import java.util.*
import kotlin.random.Random

class RegionListControllerFT : AbstractFunctionalTestBase() {

    @Test
    fun regionListTest() {
        val count = Random.nextInt(3, 12)
        val list = mutableListOf<RegionJson>()
        for (i in 1..count) {
            list.add(region())
        }

        val accessToken = authenticate()

        val regionListResponse = RestAssured.given().contentType(ContentType.JSON)
            .header(JwtTokenUtil.ACCESS_TOKEN_HEADER, "Bearer $accessToken")
            //.header(USER_HEADER_NAME, TEST_ADMIN_USER)
            .`when`()
            .baseUri("http://localhost")
            .port(BACKEND_PORT)
            .basePath("/region")
            .get("/list-all")
            .prettyPeek()
            .then()
            .extract()
            .jsonPath()
        //println(response)
        val regionListResponseBody = regionListResponse.getList<MutableMap<String, Any>>("")

        assertThat(regionListResponseBody).hasSizeGreaterThanOrEqualTo(list.size)
        assertThat(regionListResponseBody).containsAll(
            list.map {
                mutableMapOf(
                    Pair("id", it.id!!.toInt()),
                    Pair("title", it.title)
                )
            }
        )
    }

    private fun region(): RegionJson {
        val title = UUID.randomUUID().toString()
        val regionId = nextId("region_seq")
        update(
            """
INSERT INTO region (
    id, title
) VALUES (
    :id, :title
)                
            """.trimIndent(),
            MapSqlParameterSource()
                .addValue("id", regionId)
                .addValue("title", title)
        )
        return RegionJson(
            id = regionId,
            title = title
        )
    }
}