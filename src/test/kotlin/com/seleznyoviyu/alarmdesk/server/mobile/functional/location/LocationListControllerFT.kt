package com.seleznyoviyu.alarmdesk.server.mobile.functional.location

import com.seleznyoviyu.alarmdesk.server.mobile.functional.AbstractFunctionalTestBase
import com.seleznyoviyu.alarmdesk.server.mobile.web.configuration.JwtTokenUtil
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.location.LocationJson
import com.seleznyoviyu.alarmdesk.server.mobile.web.json.region.RegionJson
import io.restassured.RestAssured
import io.restassured.http.ContentType
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import java.util.*
import kotlin.random.Random

class LocationListControllerFT : AbstractFunctionalTestBase() {

    @Test
    fun locationListTest() {
        val region = region()
        val count = Random.nextInt(3, 12)
        val list = mutableListOf<LocationJson>()
        for (i in 1..count) {
            list.add(location(region.id!!))
        }

        val accessToken = authenticate()

        val locationListResponse = RestAssured.given().contentType(ContentType.JSON)
            .header(JwtTokenUtil.ACCESS_TOKEN_HEADER, "Bearer $accessToken")
            //.header(USER_HEADER_NAME, TEST_ADMIN_USER)
            .`when`()
            .baseUri("http://localhost")
            .port(BACKEND_PORT)
            .basePath("/location")
            .get("/list-all")
            .prettyPeek()
            .then()
            .extract()
            .jsonPath()
        //println(response)
        val locationListResponseBody = locationListResponse.getList<MutableMap<String, Any>>("")

        Assertions.assertThat(locationListResponseBody).hasSizeGreaterThanOrEqualTo(list.size)
        Assertions.assertThat(locationListResponseBody).containsAll(
            list.map {
                mutableMapOf(
                    Pair("id", it.id!!.toInt()),
                    Pair("title", it.title),
                    Pair("region-id", region.id!!.toInt()),
                    Pair("address", it.address),
                    Pair("ukm-location", it.ukmLocation.toInt())
                )
            }
        )
    }

    private fun location(regionId: Long): LocationJson {
        val title = UUID.randomUUID().toString()
        val address = UUID.randomUUID().toString()
        val locationId = nextId("location_seq")
        update(
            """
INSERT INTO location (
    id, title, region_id, address, ukm_location
) VALUES (
    :id, :title, :region_id, :address, :id
)                
            """.trimIndent(),
            MapSqlParameterSource()
                .addValue("id", locationId)
                .addValue("title", title)
                .addValue("region_id", regionId)
                .addValue("address", address)
        )
        return LocationJson(
            id = locationId,
            title = title,
            regionId = regionId,
            address = address,
            ukmLocation = locationId
        )
    }

    private fun region(): RegionJson {
        val title = UUID.randomUUID().toString()
        val regionId = nextId("region_seq")
        update(
            """
INSERT INTO region (
    id, title
) VALUES (
    :id, :title
)                
            """.trimIndent(),
            MapSqlParameterSource()
                .addValue("id", regionId)
                .addValue("title", title)
        )
        return RegionJson(
            id = regionId,
            title = title
        )
    }
}